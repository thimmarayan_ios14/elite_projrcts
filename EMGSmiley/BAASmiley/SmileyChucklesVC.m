//
//  SmileyChucklesVC.m
//  BAASmiley
//
//  Created by Mac User on 26/03/15.
//
//

#import "SmileyChucklesVC.h"
#import "AFNetworking.h"
#import "FXReachability.h"
#import "SpeechBubbleVC.h"
#import "Define.h"
#import "UIImage+animatedGIF.h"

#define label_width 300

@interface SmileyChucklesVC ()

@end

@implementation SmileyChucklesVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    _smileyChucklesVW = [[UIView alloc]init];
    _smileyChuckleAnimationImgVw = [[UIImageView alloc]init];

    _speechBubbleVC   = [[SpeechBubbleVC alloc]init];

    heheheLbl1 = [UILabel new];
    heheheLbl2 = [UILabel new];
    heheheLbl3 = [UILabel new];
    heheheLbl4 = [UILabel new];
    thatTicklesLbl = [UILabel new];
    heheheLbl5 = [UILabel new];
    heheheLbl6 = [UILabel new];
    heheheLbl7 = [UILabel new];
    



    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    smileySecondsLeft = 4;


    /* CALLING SERVICE  */
    
    //[self getContentService];

    [_smileyChucklesVW setFrame:CGRectMake(0, 0, 1024, 768)];
    [_smileyChucklesVW setBackgroundColor:[UIColor colorWithRed:(255.0f/255) green:(144.0f/255) blue:(0.0f/255)  alpha:1.0f]];
    [self.view addSubview:_smileyChucklesVW];
    
    heheheLbl1.frame = CGRectMake(_smileyChucklesVW.frame.size.width - 270 , _smileyChucklesVW.frame.size.height - 450 , label_width- 100, 50);
    heheheLbl2.frame = CGRectMake(_smileyChucklesVW.frame.size.width - 200 , heheheLbl1.frame.size.height + heheheLbl1.frame.origin.y + 20 , label_width - 50 , 50);
    
    heheheLbl3.frame = CGRectMake(_smileyChucklesVW.frame.size.width - 370, heheheLbl2.frame.size.height + heheheLbl2.frame.origin.y - 20   , label_width, 70);
    
    heheheLbl4.frame = CGRectMake(heheheLbl3.frame.origin.x - 160, heheheLbl2.frame.size.height + heheheLbl2.frame.origin.y  , label_width, 50);
    
    thatTicklesLbl.frame = CGRectMake(heheheLbl4.frame.origin.x + heheheLbl4.frame.size.width - 210, heheheLbl4.frame.size.height + heheheLbl4.frame.origin.y  , label_width, 50 );
    
    heheheLbl5.frame = CGRectMake(thatTicklesLbl.frame.origin.x - 170, thatTicklesLbl.frame.size.height + thatTicklesLbl.frame.origin.y  , label_width, 50 );
    
    heheheLbl6.frame = CGRectMake(heheheLbl5.frame.origin.x + heheheLbl5.frame.size.width - 150, heheheLbl5.frame.size.height + heheheLbl5.frame.origin.y + 35, label_width - 100, 50 );
    
    heheheLbl7.frame = CGRectMake(heheheLbl5.frame.origin.x + 190 , heheheLbl5.frame.size.height + heheheLbl5.frame.origin.y - 30 , label_width - 100, 50 );
    
    
    CGFloat rotationLeft = MakeRotation(M_PI_4 / 18.0, 0.0, 5.0);
    heheheLbl1.transform = CGAffineTransformMakeRotation(rotationLeft);
    
    CGFloat rotationRight = MakeRotation(M_PI_2 / 30.0, 0.0, 5.0);
    heheheLbl2.transform = CGAffineTransformMakeRotation(-rotationRight);
    
    CGFloat rotationRight3 = MakeRotation(M_PI_2 / 28.0, 0.0, 5.0);
    heheheLbl3.transform = CGAffineTransformMakeRotation(-rotationRight3);
    
    CGFloat rotationLeft4 = MakeRotation(M_PI_4 / 18.0, 0.0, 5.0);
    heheheLbl4.transform = CGAffineTransformMakeRotation(rotationLeft4);
    
    CGFloat rotationLeft5 = MakeRotation(M_PI_4 / 22.0, 0.0, 5.0);
    heheheLbl5.transform = CGAffineTransformMakeRotation(rotationLeft5);
    
    CGFloat rotationRight6 = MakeRotation(M_PI_2 / 18.0, 0.0, 5.0);
    heheheLbl6.transform = CGAffineTransformMakeRotation(-rotationRight6);
    
    CGFloat rotationRight7 = MakeRotation(M_PI_4 / 18.0, 0.0, 5.0);
    heheheLbl7.transform = CGAffineTransformMakeRotation(rotationRight7);
    
    heheheLbl1.textColor = [UIColor whiteColor];
    heheheLbl2.textColor = [UIColor whiteColor];
    heheheLbl3.textColor = [UIColor whiteColor];
    heheheLbl4.textColor = [UIColor whiteColor];
    heheheLbl5.textColor = [UIColor whiteColor];
    heheheLbl6.textColor = [UIColor whiteColor];
    thatTicklesLbl.textColor = [UIColor whiteColor];
    heheheLbl7.textColor = [UIColor whiteColor];

    
    heheheLbl1.text = @"he he he";
    heheheLbl2.text = @"he he he";
    heheheLbl3.text = @"he he he";
    heheheLbl4.text = @"he he he";
    thatTicklesLbl.text = @"that tickles";
    heheheLbl5.text = @"he he he";
    heheheLbl6.text = @"he he he";
    heheheLbl7.text = @"he he he";

    
    [heheheLbl1 setAlpha:0.0f];
    [heheheLbl2 setAlpha:0.0f];
    [heheheLbl3 setAlpha:0.0f];
    [heheheLbl4 setAlpha:0.0f];
    [heheheLbl5 setAlpha:0.0f];
    [heheheLbl6 setAlpha:0.3f];
    [thatTicklesLbl setAlpha:0.0f];
    [heheheLbl7 setAlpha:0.0f];

    
    heheheLbl1.font = [UIFont fontWithName:HIPOPOTAMOUTLINE size:40];
    heheheLbl2.font = [UIFont fontWithName:HIPOPOTAMOUTLINE size:35];
    heheheLbl3.font = [UIFont fontWithName:HIPOPOTAMOUTLINE size:53];
    heheheLbl4.font = [UIFont fontWithName:HIPOPOTAMOUTLINE size:40];
    thatTicklesLbl.font = [UIFont fontWithName:HIPOPOTAM size:41];
    heheheLbl5.font = [UIFont fontWithName:HIPOPOTAMOUTLINE size:50];
    heheheLbl6.font = [UIFont fontWithName:HIPOPOTAMOUTLINE size:24];
    heheheLbl7.font = [UIFont fontWithName:HIPOPOTAM size:24];

    
   [self startAnimationBatch];
    [self performSelector:@selector(configureAudioPlayer) withObject:nil afterDelay:1];
   // [self configureAudioPlayer];

    
    /* EYES BLINK VIEW */
    
    [_smileyChucklesVW setFrame:CGRectMake(0, 0, 1024, 768)];
    [_smileyChucklesVW setBackgroundColor:[UIColor colorWithRed:(255.0f/255) green:(144.0f/255) blue:(0.0f/255)  alpha:1.0f]];
    [self.view addSubview:_smileyChucklesVW];
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"Mouth-open" withExtension:@"gif"];
    UIImage *testImage1 = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url1]];
    
    [_smileyChuckleAnimationImgVw setFrame:CGRectMake(700, 470, 300, 300)];
    _smileyChuckleAnimationImgVw.backgroundColor=[UIColor clearColor];
    _smileyChuckleAnimationImgVw.animationImages=testImage1.images;
    _smileyChuckleAnimationImgVw.animationDuration=2.2;
    _smileyChuckleAnimationImgVw.animationRepeatCount=0;
    _smileyChuckleAnimationImgVw.userInteractionEnabled = NO;
    [_smileyChucklesVW addSubview:_smileyChuckleAnimationImgVw];
    [_smileyChuckleAnimationImgVw startAnimating];
    
    [self smileyChucklesTimer];
    
    [self.view addSubview:heheheLbl1];
    [self.view addSubview:heheheLbl2];
    [self.view addSubview:heheheLbl3];
    [self.view addSubview:heheheLbl4];
    [self.view addSubview:thatTicklesLbl];
    [self.view addSubview:heheheLbl5];
    [self.view addSubview:heheheLbl6];
    [self.view addSubview:heheheLbl7];

    
}
-(void)smileyChucklesTimer
{
    if ([_smileyChucklesTimer isValid]) {
        
        [_smileyChucklesTimer invalidate];
    }
    _smileyChucklesTimer = [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(startSmileyChucklesTimer:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_smileyChucklesTimer forMode:NSRunLoopCommonModes];
    
}
-(void)startSmileyChucklesTimer: (NSTimer *)theTimer
{
    if (smileySecondsLeft > 0) {
        
        smileySecondsLeft --;
    }
    else
    {
        [_smileyChucklesTimer invalidate];
        [_smileyChuckleAnimationImgVw stopAnimating];
        [_smileyChuckleAnimationImgVw removeFromSuperview];
        [_audioPlayer stop];
        
        if(![self.navigationController.topViewController isKindOfClass:[_speechBubbleVC class]]) {
            [self.navigationController pushViewController:_speechBubbleVC animated:NO];
            heheheLbl1 = [UILabel new];
            heheheLbl2 = [UILabel new];
            heheheLbl3 = [UILabel new];
            heheheLbl4 = [UILabel new];
            thatTicklesLbl = [UILabel new];
            heheheLbl5 = [UILabel new];
            heheheLbl6 = [UILabel new];
            heheheLbl7 = [UILabel new];
            
            self.audioPlayer = nil;

        }
        

    }
}

-(void)getContentService
{
    
    if ([FXReachability isReachable]) {
        
         AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:GET_PUB_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"%@",responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error : %@", error);
        }];
     }
    else
    {
       UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERT_TITLE message:@"Seems your internet is offline." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [offlineAlert show];
    }
}

- (void)configureAudioPlayer {
    
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:@"AS 538246 - MUNCHKIN TITTER - GIGGLE (Artist - SAE)" withExtension:@"mp3"];
    NSError *error;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_audioPlayer setNumberOfLoops:1];
    [_audioPlayer setMeteringEnabled:YES];
    [self setAudioPlayer:_audioPlayer];
    [_audioPlayer play];
}

- (void)configureAudioSession {
    NSError *error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (error) {
        NSLog(@"Error setting category: %@", [error description]);
    }
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationLandscapeLeft;
}
-(void)startAnimationBatch
{
    [UIView animateKeyframesWithDuration:5.0 delay:0.05 options: UIViewKeyframeAnimationOptionRepeat  animations:^{
        [heheheLbl6 setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            heheheLbl6.frame = CGRectMake(heheheLbl6.frame.origin.x ,-50 , _smileyChucklesVW.frame.size.width, 50);
            
        }];
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [heheheLbl6 setAlpha:1.0f];
        
    }];
    
    [UIView animateKeyframesWithDuration:5.0 delay:0.00 options: UIViewKeyframeAnimationOptionRepeat  animations:^{
        [heheheLbl7 setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            heheheLbl7.frame = CGRectMake(heheheLbl7.frame.origin.x ,-50 , _smileyChucklesVW.frame.size.width, 50);
            
        }];
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [heheheLbl7 setAlpha:1.0f];
        
    }];
    
    [UIView animateKeyframesWithDuration:5.0 delay:0.5 options: UIViewKeyframeAnimationOptionRepeat animations:^{
        [heheheLbl5 setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            heheheLbl5.frame = CGRectMake(heheheLbl6.frame.origin.x ,-50 , _smileyChucklesVW.frame.size.width, 50);
            
        }];
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [heheheLbl5 setAlpha:1.0f];
        
    }];
    
    [UIView animateKeyframesWithDuration:5.0 delay:1.0 options: UIViewKeyframeAnimationOptionRepeat animations:^{
        [thatTicklesLbl setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            thatTicklesLbl.frame = CGRectMake(heheheLbl6.frame.origin.x ,-50 , _smileyChucklesVW.frame.size.width, 50);
            
        }];
        
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [thatTicklesLbl setAlpha:1.0f];
        
    }];
    
    [UIView animateKeyframesWithDuration:5.0 delay:1.5 options: UIViewKeyframeAnimationOptionRepeat animations:^{
        [heheheLbl4 setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            heheheLbl4.frame = CGRectMake(heheheLbl4.frame.origin.x,-50 , _smileyChucklesVW.frame.size.width, 50);
            
        }];
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [heheheLbl4 setAlpha:1.0f];
        
    }];
    [UIView animateKeyframesWithDuration:5.0 delay:2.0 options: UIViewKeyframeAnimationOptionRepeat animations:^{
        
        [heheheLbl3 setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            heheheLbl3.frame = CGRectMake(heheheLbl3.frame.origin.x,-50 , label_width, heheheLbl3.frame.size.height);
            
        }];
        
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [heheheLbl3 setAlpha:1.0f];
        
    }];
    [UIView animateKeyframesWithDuration:5.0 delay:2.5 options: UIViewKeyframeAnimationOptionRepeat animations:^{
        
        [heheheLbl2 setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            
            heheheLbl2.frame = CGRectMake(heheheLbl2.frame.origin.x,-50 , label_width, heheheLbl2.frame.size.height);
            
        }];
        
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [heheheLbl2 setAlpha:1.0f];
        
    }];
    [UIView animateKeyframesWithDuration:5.0 delay:3.0 options: UIViewKeyframeAnimationOptionRepeat animations:^{
        
        [heheheLbl1 setAlpha:1.0f];
        
        [UIView addKeyframeWithRelativeStartTime:0.0 relativeDuration:5.0 animations:^{
            
            
            heheheLbl1.frame = CGRectMake(heheheLbl1.frame.origin.x,- 50 , label_width, heheheLbl1.frame.size.height);
            
        }];
        
    } completion:^(BOOL finished) {
        
        //fade out
        
        [heheheLbl1 setAlpha:1.0f];
    }];
    
}
CGFloat MakeRotation(CGFloat start, CGFloat end, CGFloat progress)
{
   return start * (1.0 - progress) + end * progress;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
