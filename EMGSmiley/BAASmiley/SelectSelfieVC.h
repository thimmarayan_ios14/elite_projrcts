//
//  SelectSelfieVC.h
//  BAASmiley
//
//  Created by Mac User on 26/03/15.
//
//

#import <UIKit/UIKit.h>
#import "TakePhotoPC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface SelectSelfieVC : UIViewController<MPMediaPickerControllerDelegate>
{
    UIView                   *_selfieVW;
    TakePhotoPC              *_imagePickerControlr;
    UIImageView              *_messageBoxImgVW, *_eyesBlinkAnimationImgVw;
    UILabel                  *_selfieLabl;
    UIButton                 *_yesBtn , *_noBtn;
    BOOL                     _isPlaying;
}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end
