//
//  SpeechBubbleVC.h
//  BAASmiley
//
//  Created by Mac User on 26/03/15.
//
//

#import <UIKit/UIKit.h>
#import "SelectSelfieVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface SpeechBubbleVC : UIViewController<MPMediaPickerControllerDelegate>
{
    UIView            *_speechBubbleVW;
    NSTimer           *_speechBubbleTimer ;
    int               speechBubbleSecondsLeft;
    SelectSelfieVC    *_selectSelfieVC;
    UIImageView       *_messageBoxImgVW, *_eyesBlinkAnimationImgVw;
    UILabel           *_dontForgetLabl , *_rcaLabl, *_dayTimeLabl;
    BOOL              _isPlaying;
}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end
