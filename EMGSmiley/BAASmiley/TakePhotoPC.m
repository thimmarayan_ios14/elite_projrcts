//
//  TakePhotoPC.m
//  BAASmiley
//
//  Created by Apple on 25/03/15.
//
//

#import "TakePhotoPC.h"
#import "AppDelegate.h"
#import "Define.h"
#import "AFNetworking.h"
#import "FXReachability.h"

@interface TakePhotoPC ()

@end

@implementation TakePhotoPC

@synthesize secondsLeft,seconds,preVwSeconds;

- (void)viewDidLoad {

    [super viewDidLoad];
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
   // NSLog(@"%@",orientation);
    
    if ( UIInterfaceOrientationIsPortrait(orientation)) {
        NSLog(@"Portrait");
    }
    else if (UIInterfaceOrientationIsLandscape(orientation))
    {
        NSLog(@"Landscape");
    }
    else {
        NSLog(@"UNKnown");
    }
    self.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
    {
        self.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        
    }
    else
    {
        UIAlertView *userInfoAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"This Device don't have camera" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [userInfoAlert show];
        
    }
    
    NSLog(@"overlay rect %@",NSStringFromCGRect(self.cameraOverlayView.frame));
    self.showsCameraControls = NO;
    
  /*  NSString *path = [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"] pathForResource:@"Tock" ofType:@"aiff"];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundID);
    AudioServicesPlaySystemSound(soundID);
    AudioServicesDisposeSystemSoundID(soundID);
   */
    
    [self setNavigationBarHidden:YES];
    
    self.toolbarHidden = YES;
    self.modalPresentationCapturesStatusBarAppearance=YES;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.delegate = self;
    
  /*  NSURL *buttonURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"iPhone camera click" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)buttonURL, &SoundID);
*/
    /* OVERLAYVIEW */
    
    _overLayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    [self.view addSubview:_overLayView];
    
    /* FRAME IMAGEVIEW */

    _frameImgView=[[UIImageView alloc]init];
    [_frameImgView setFrame:_overLayView.frame];
    _frameImage = [UIImage imageNamed:@"smiley-frame"];
    [_frameImgView setImage:_frameImage];
    _frameImgView.userInteractionEnabled=YES;
    [_overLayView addSubview:_frameImgView];

    /* TIMER LABLE */
    
    _timerLabl = [[UILabel alloc]initWithFrame:CGRectMake(0, 768/1.35, 200, 200)];
    _timerLabl.textAlignment = NSTextAlignmentCenter;
    _timerLabl.font = [UIFont fontWithName:HIPOPOTAM size:90];
    _timerLabl.textColor = [UIColor whiteColor];
    [_timerLabl setBackgroundColor:[UIColor clearColor]];
    [_overLayView addSubview:_timerLabl];
    
    /* PREVIEW VIEW */
    
    _previewVW = [UIView new];
    _previewVW.frame = CGRectMake(0, 768, 1024, 768);
    _previewVW.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_previewVW];

    /* TAKEN PHOTO IMAGEVIEW */
    
    _photoImageVW = [UIImageView new];
    _photoImageVW.frame = CGRectMake(0,0, 1024, 768);
    [_previewVW addSubview:_photoImageVW];
 
}

- (void)configureAudioPlayer {
    
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:@"iPhone camera click" withExtension:@"mp3"];
    NSError *error;
    if (!_audioPlayer) {
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    }
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_audioPlayer setNumberOfLoops:1];
    [_audioPlayer setMeteringEnabled:YES];
    [_audioPlayer play];
}

-(void)countDownTimer
{
    if ([_countDownTimer isValid]) {
        
        [_countDownTimer invalidate];
    }
    _countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
}
-(void)preVWTimerStart
{
    [_audioPlayer stop];
    
    if ([_preVWTimer isValid]) {
        
        [_preVWTimer invalidate];
    }
    _preVWTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(preViewCountDownTimer:) userInfo:nil repeats:YES];
}
-(void)updateCounter:(NSTimer*)theTimer
{
    if(secondsLeft > 0 ){
        secondsLeft -- ;
        seconds = (secondsLeft %1800) % 60;
        _timerLabl.text = [NSString stringWithFormat:@"%d", seconds];
    }
    else{
        [self takePicture];
        [_countDownTimer invalidate];
        _countDownTimer = nil;
     }
}
-(void)preViewCountDownTimer:(NSTimer*)theTimer
{
    if (preVwSeconds>0) {
        preVwSeconds --;
    }
    else
    {
        
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self configureAudioPlayer];
    
    pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
/*  UIImage *imageToDisplay = [UIImage imageWithCGImage:[pickedImage CGImage]
                                                       scale:1.0
                                                 orientation: UIImageOrientationUp];*/
    
    orginalImage = [self mergeImages:pickedImage foreGround:nil];
    finalImage = [self mergeImages:pickedImage foreGround:_frameImage];
    
    _photoImageVW.image = finalImage;

    [self uploadDataToService];
    
    [UIView animateWithDuration:0.15 animations:^{

        _previewVW.frame = CGRectMake(0, 0, 1024, 768);

        [self preVWTimerStart];

    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}
-(void)closePreviewVw{
    
    _previewVW.frame = CGRectMake(0, 768, 1024, 768);
    _pickerDismissed = YES;
    _tohideImgVW     = YES;
    [self dismissViewControllerAnimated:NO completion:^{
        [((AppDelegate *)[[UIApplication sharedApplication] delegate]) logoutNav];
    }];
    
    [_preVWTimer invalidate];
    _preVWTimer    = nil;
    _timerLabl.text = nil;
}

- (UIImage *) mergeImages:(UIImage *)bgImageFileName foreGround:(UIImage *)fgImageFileName  {
    
    UIImage *bottomImage = bgImageFileName; //background image
    UIImage *image       = fgImageFileName; //foreground image
    
    CGSize newSize = CGSizeMake(950, 700);
    UIGraphicsBeginImageContext( newSize );
    
    // Use existing opacity as is
    [bottomImage drawInRect:CGRectMake(0,0,950,700)];
    
    // Apply supplied opacity if applicable
    // Change xPos, yPos if applicable
    [image drawInRect:CGRectMake(0,0,950,700) blendMode:kCGBlendModeNormal alpha:1.0];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return newImage;
}
-(void)uploadDataToService
{
    if ([FXReachability isReachable]) {
        
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        _currentTimeStr = [dateFormatter stringFromDate: currentTime];
        
        NSData *imagedata = UIImageJPEGRepresentation(orginalImage, 0.7); //UIImageJPEGRepresentation(finalImage, 0.7);
        
        NSString * imageString =  [self base64forData:imagedata];
 
        NSDictionary *parameters = @{@"ImgData":imageString,@"ImgDateTime" : _currentTimeStr};

        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [manager POST:[NSString stringWithFormat:@"%@",PUB_URL] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([[responseObject objectForKey:@"Returncode"] integerValue] == 1) {
                
                NSLog(@"sucess: %@", responseObject);
                
                [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(closePreviewVw) userInfo:nil repeats:NO];

            }
            
            else if ([[responseObject objectForKey:@"Returncode"] integerValue] == 2){
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"There is a problem connecting to the server." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            
        } failure:^(AFHTTPRequestOperation*operation, NSError*error) {
            
            NSLog(@"Error: %@", error);
            
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"There is a problem connectiong to the server." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [failureAlert show];
            
        }];
    }
    
    else
    {
        UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERT_TITLE message:@"Seems your internet is offline" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil,nil];
        [offlineAlert show];
    }
    
}

- (NSString*)base64forData:(NSData*)theData
{
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}
-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscape;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you wifgll often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
