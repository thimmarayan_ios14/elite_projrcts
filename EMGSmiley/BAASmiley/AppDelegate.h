//
//  AppDelegate.h
//  BAASmiley
//
//  Created by Apple on 25/03/15.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong,nonatomic)UINavigationController *navigationController;

@property (strong, nonatomic) NSString *imageString;

-(void)logoutNav;

@end

