//
//  SelectSelfieVC.m
//  BAASmiley
//
//  Created by Mac User on 26/03/15.
//
//

#import "SelectSelfieVC.h"
#import "UIImage+animatedGIF.h"
#import "Define.h"

@interface SelectSelfieVC ()

@end

@implementation SelectSelfieVC

- (void)viewDidLoad {
    [super viewDidLoad];
    

    _imagePickerControlr      = [[TakePhotoPC alloc]init];
    _selfieVW                 = [[UIView alloc]init];
    _messageBoxImgVW          = [[UIImageView alloc]init];
    _eyesBlinkAnimationImgVw  = [[UIImageView alloc]init];
    _selfieLabl               = [[UILabel alloc]init];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.view setBackgroundColor:[UIColor colorWithRed:(255.0f/255) green:(144.0f/255) blue:(0.0f/255)  alpha:1.0f]];


    if (_pickerDismissed == NO) {
        
       // [self configureAudioPlayer];
    }

    /* SELFIE VIEW */
    
    [_selfieVW setFrame:CGRectMake(self.view.frame.size.width/2 - (924/2), self.view.frame.size.height /2 - (565/2), 924, 565)];

    [_selfieVW setBackgroundColor:[UIColor clearColor]];
   // [self.view addSubview:_selfieVW];
    
    /* MESSAGE BOX IMAGEVIEW */
    
    UIImage *boxImage = [UIImage imageNamed:@"message-box"];
    [_messageBoxImgVW setFrame:CGRectMake(0,0,924,565)];
   // [_messageBoxImgVW setCenter:CGPointMake(_selfieVW.frame.size.width/2, 768/2)];
    [_messageBoxImgVW setImage:boxImage];
    _messageBoxImgVW.backgroundColor = [UIColor clearColor];
    [_selfieVW addSubview:_messageBoxImgVW];
    
    /* GIF ANIMATION */
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"Eye-Blink" withExtension:@"gif"];
    UIImage *testImage1 = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url1]];
    
    [_eyesBlinkAnimationImgVw setFrame:CGRectMake(820, 620, 150, 150)];
    _eyesBlinkAnimationImgVw.backgroundColor=[UIColor clearColor];
    _eyesBlinkAnimationImgVw.animationImages=testImage1.images;
    _eyesBlinkAnimationImgVw.animationDuration=2.5;
    _eyesBlinkAnimationImgVw.animationRepeatCount=0;
    _eyesBlinkAnimationImgVw.userInteractionEnabled = NO;
    [self.view addSubview:_eyesBlinkAnimationImgVw];
    [_eyesBlinkAnimationImgVw startAnimating];

    /* SELFIE LABEL */
    
    [_selfieLabl setFrame:CGRectMake(0,0, _selfieVW.frame.size.width, 565/3)];
    [_selfieLabl setCenter:CGPointMake(_selfieVW.frame.size.width/2, _selfieVW.frame.size.height/3.5)];
    _selfieLabl.text = @"WANNA TAKE A SELFIE?";
    _selfieLabl.textColor = [UIColor whiteColor];
    _selfieLabl.textAlignment = NSTextAlignmentCenter;
    _selfieLabl.font = [UIFont fontWithName:HIPOPOTAM size:60];
    _selfieLabl.backgroundColor =[UIColor clearColor];
    [_selfieVW addSubview:_selfieLabl];

    /* YES BTN */

    _yesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_yesBtn setFrame:CGRectMake(_selfieVW.frame.size.width /3.5, CGRectGetMaxY([_selfieLabl frame])+10, 150, 150)];
    [_yesBtn setTitle:@"YES" forState:UIControlStateNormal];
    [_yesBtn setBackgroundColor:[UIColor clearColor]];
    [_yesBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_yesBtn addTarget:self action:@selector(yesBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    _yesBtn.titleLabel.font = [UIFont fontWithName:HIPOPOTAM size:60];
    [_selfieVW addSubview:_yesBtn];
    
    /* NO BTN */
    
    _noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_noBtn setFrame:CGRectMake(CGRectGetMaxX([_yesBtn frame])+150, CGRectGetMinY([_yesBtn frame]), 150, 150)];
    [_noBtn setTitle:@"NO" forState:UIControlStateNormal];
    [_noBtn setBackgroundColor:[UIColor clearColor]];
    [_noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_noBtn addTarget:self action:@selector(noBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    _noBtn.titleLabel.font = [UIFont fontWithName:HIPOPOTAM size:60];
    [_selfieVW addSubview:_noBtn];
    
    if (!_tohideImgVW) {
        
        _selfieVW.hidden = NO;
    }
    else
    {
        _selfieVW.hidden = YES;
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self startAnimationForView];
}
-(void)yesBtnTapped
{
    [self presentViewController:_imagePickerControlr animated:NO completion:^{
      //  [_audioPlayer stop];
       // _audioPlayer = nil;
        _imagePickerControlr.secondsLeft  = 6;
        _imagePickerControlr.preVwSeconds = 2;
        [_imagePickerControlr countDownTimer];

    }];
    
}
-(void)noBtnTapped
{
   // [_audioPlayer stop];
   // _audioPlayer = nil;

    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)startAnimationForView
{
    _selfieVW.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [self.view addSubview:_selfieVW];
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _selfieVW.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _selfieVW.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                _selfieVW.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}

- (void)configureAudioPlayer {
    
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:@"Sound-MagicToggleButton-AudioJungle" withExtension:@"mp3"];
    NSError *error;
    if (!_audioPlayer) {
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    }
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_audioPlayer setNumberOfLoops:-1];
    [_audioPlayer setMeteringEnabled:YES];
    [_audioPlayer play];
}

/*
- (void)configureAudioSession {
    NSError *error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (error) {
        NSLog(@"Error setting category: %@", [error description]);
    }
}
 */

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationLandscapeLeft;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
