//
//  TakePhotoPC.h
//  BAASmiley
//
//  Created by Apple on 25/03/15.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@interface TakePhotoPC : UIImagePickerController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIView        *_overLayView, *_previewVW;
    UIImageView   *_frameImgView, *_photoImageVW;
    UILabel       *_timerLabl;
    NSTimer       *_countDownTimer, *_preVWTimer;
    UIImage       * pickedImage, * finalImage, *_frameImage, *orginalImage;
    NSString      *_currentTimeStr;
    SystemSoundID SoundID;
}
@property (nonatomic,assign)int secondsLeft,seconds,preVwSeconds ;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

-(void)countDownTimer;

@end
