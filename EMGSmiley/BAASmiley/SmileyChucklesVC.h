//
//  SmileyChucklesVC.h
//  BAASmiley
//
//  Created by Mac User on 26/03/15.
//
//

#import <UIKit/UIKit.h>
#import "SpeechBubbleVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface SmileyChucklesVC : UIViewController<MPMediaPickerControllerDelegate>
{
    UIView            *_smileyChucklesVW;
    UIImageView       *_smileyChuckleAnimationImgVw;
    NSTimer           *_smileyChucklesTimer ;
    int               smileySecondsLeft;
    SpeechBubbleVC    *_speechBubbleVC;
    BOOL              _isPlaying;
    UILabel           *heheheLbl1,*heheheLbl2,*heheheLbl3,*heheheLbl4,*thatTicklesLbl,*heheheLbl5,*heheheLbl6,*heheheLbl7;
    

}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@end
