//
//  SpeechBubbleVC.m
//  BAASmiley
//
//  Created by Mac User on 26/03/15.
//
//

#import "SpeechBubbleVC.h"
#import "UIImage+animatedGIF.h"
#import "Define.h"
#import "AppDelegate.h"

@interface SpeechBubbleVC (){
    
    int count;
    NSArray*imagesArray;
    NSMutableArray *imgCountArr;
}

@end

@implementation SpeechBubbleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:(255.0f/255) green:(144.0f/255) blue:(0.0f/255)  alpha:1.0f]];
    
    self.navigationController.navigationBarHidden = YES;
    
    _speechBubbleVW           = [[UIView alloc]init];
    _selectSelfieVC           = [[SelectSelfieVC alloc]init];
    _messageBoxImgVW          = [[UIImageView alloc]init];
    _eyesBlinkAnimationImgVw  = [[UIImageView alloc]init];
    _dontForgetLabl           = [[UILabel alloc]init];
    _rcaLabl                  = [[UILabel alloc]init];
    _dayTimeLabl              = [[UILabel alloc]init];


    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
     speechBubbleSecondsLeft = 6;
    imgCountArr = [[NSMutableArray alloc]init];
    
    count = 0;

    /* SPEECH BUBBLE VIEW */
    
    [_speechBubbleVW setFrame:CGRectMake(self.view.frame.size.width/2 - (924/2), self.view.frame.size.height /2 - (565/2), 924, 565)];
    [_speechBubbleVW setBackgroundColor:[UIColor clearColor]];
    
    /* MESSAGE BOX IMAGEVIEW */
    
    
   // UIImage *boxImage = [UIImage imageNamed:@"message-box"];
    [_messageBoxImgVW setFrame:CGRectMake(0,0,1024,768)];
   // [_messageBoxImgVW setCenter:CGPointMake(1024/2, 768/2)];
    //[_messageBoxImgVW setImage:boxImage];
   // _messageBoxImgVW.backgroundColor = [UIColor clearColor];
    //[self.view addSubview:_messageBoxImgVW];
    
    
    /* Random messages */
    
//    imagesArray = [NSArray arrayWithObjects:@"01",@"02",@"03", nil];
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if([appDel.imageString isEqualToString:@"01"]){
        appDel.imageString = @"02";
    }else if([appDel.imageString isEqualToString:@"02"]){
        appDel.imageString = @"03";
    }else{
        appDel.imageString = @"01";
    }
    
   // NSUInteger randomIndex = arc4random() % [imagesArray count];
    
//    if (count ==0)
//    {
//        [imgCountArr addObject:[NSNumber numberWithUnsignedInteger:randomIndex]];
//        
//        NSLog(@"ImgCountArr %@",imgCountArr);
//        
//    }
//    
//    if (!count==0)
//    {
//        if ([imgCountArr containsObject:[NSNumber numberWithUnsignedInteger:randomIndex]])
//        {
//            if (randomIndex == 0)
//            {
//                randomIndex = randomIndex + 1;
//                
//            }
//            else if (randomIndex ==1)
//            {
//                randomIndex = randomIndex + 1;
//            }
//            else if (randomIndex == 2)
//            {
//                randomIndex = randomIndex + 1;
//            }
//            
//            else if (randomIndex ==3)
//            {
//                randomIndex = randomIndex - 1;
//            }
//        }
//    }
//    
    _messageBoxImgVW.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:appDel.imageString]];
    count = 1;

    

    /* GIF ANIMATION */
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"Eye-Blink" withExtension:@"gif"];
    UIImage *testImage1 = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url1]];
    
    [_eyesBlinkAnimationImgVw setFrame:CGRectMake(820, 620, 150, 150)];
    _eyesBlinkAnimationImgVw.backgroundColor=[UIColor clearColor];
    _eyesBlinkAnimationImgVw.animationImages=testImage1.images;
    _eyesBlinkAnimationImgVw.animationDuration=2.5;
    _eyesBlinkAnimationImgVw.animationRepeatCount=0;
    _eyesBlinkAnimationImgVw.userInteractionEnabled = NO;
    [self.view addSubview:_eyesBlinkAnimationImgVw];
    [_eyesBlinkAnimationImgVw startAnimating];

 
    /* DONT FORGET LABEL */
    
  /*  [_dontForgetLabl setFrame:CGRectMake(0,0, _speechBubbleVW.frame.size.width, 565/4)];
    [_dontForgetLabl setCenter:CGPointMake(_speechBubbleVW.frame.size.width/2, _speechBubbleVW.frame.size.height/6)];
    _dontForgetLabl.text = @"DON'T FORGET";
    _dontForgetLabl.textColor = [UIColor whiteColor];
    _dontForgetLabl.textAlignment = NSTextAlignmentCenter;
    _dontForgetLabl.font = [UIFont fontWithName:HIPOPOTAM size:40];
    _dontForgetLabl.backgroundColor =[UIColor clearColor];
    [_speechBubbleVW addSubview:_dontForgetLabl];
    
  */  /* RCA LABEL */
    
  /*  [_rcaLabl setFrame:CGRectMake(CGRectGetMinX([_dontForgetLabl frame])+100,CGRectGetMaxY([_dontForgetLabl frame]), 924-150, 565/3)];
    _rcaLabl.text = @"RCA recording artist Elle King is performaing at the REGGIE AWARDS GALA";
    _rcaLabl.textColor = [UIColor whiteColor];
    _rcaLabl.font = [UIFont fontWithName:HIPOPOTAM size:40];
    _rcaLabl.textAlignment = NSTextAlignmentCenter;
    _rcaLabl.numberOfLines = 0;
    _rcaLabl.backgroundColor =[UIColor clearColor];
    [_speechBubbleVW addSubview:_rcaLabl];
*/
    /* DATE AND TIME LABEL */
    
 /*   [_dayTimeLabl setFrame:CGRectMake(CGRectGetMinX([_dontForgetLabl frame]),CGRectGetMaxY([_rcaLabl frame]), 924, 565/4.5)];
    _dayTimeLabl.text = @"Wednesday 15 @ 8pm";
    _dayTimeLabl.textColor = [UIColor whiteColor];
    _dayTimeLabl.textAlignment = NSTextAlignmentCenter;
    _dayTimeLabl.numberOfLines = 0;
    _dayTimeLabl.font = [UIFont fontWithName:HIPOPOTAM size:40];
    _dayTimeLabl.backgroundColor =[UIColor clearColor];
    [_speechBubbleVW addSubview:_dayTimeLabl];
  */
    [self speechBubblesTimer];
   // [self configureAudioPlayer];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self startAnimationForView];
   
}
-(void)speechBubblesTimer
{
    if ([_speechBubbleTimer isValid]) {
        
        [_speechBubbleTimer invalidate];
    }
    _speechBubbleTimer = [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(startSpeechBubbleTimer:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_speechBubbleTimer forMode:NSRunLoopCommonModes];
    
}
-(void)startSpeechBubbleTimer: (NSTimer *)theTimer
{
    if (speechBubbleSecondsLeft > 0) {
        
        speechBubbleSecondsLeft --;
    }
    else
    {
        [_eyesBlinkAnimationImgVw stopAnimating];
        [_eyesBlinkAnimationImgVw removeFromSuperview];
       // [_audioPlayer stop];
        
        if(![self.navigationController.topViewController isKindOfClass:[_selectSelfieVC class]]) {
            [self.navigationController pushViewController:_selectSelfieVC animated:NO];
          //  _audioPlayer = nil;

        }
        _tohideImgVW     = NO;
        _pickerDismissed = NO;
        [_speechBubbleTimer invalidate];
    }
}
- (void)configureAudioPlayer {
    
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:@"Sound-MagicToggleButton-AudioJungle" withExtension:@"mp3"];
    NSError *error;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_audioPlayer setNumberOfLoops:-1];
    [_audioPlayer setMeteringEnabled:YES];
    [self setAudioPlayer:_audioPlayer];
    [_audioPlayer play];
}

- (void)configureAudioSession {
    NSError *error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (error) {
        NSLog(@"Error setting category: %@", [error description]);
    }
}

-(void)startAnimationForView
{
    _messageBoxImgVW.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [self.view addSubview:_messageBoxImgVW];

    
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _messageBoxImgVW.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _messageBoxImgVW.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                _messageBoxImgVW.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationLandscapeLeft;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
