//
//  ViewController.h
//  BAASmiley
//
//  Created by Apple on 25/03/15.
//
//

#import <UIKit/UIKit.h>
#import "SmileyChucklesVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "TakePhotoPC.h"

@interface ViewController : UIViewController<MPMediaPickerControllerDelegate,AVAudioPlayerDelegate,AVAudioSessionDelegate>
{
    UIView                  *_eyesBlinkVW;
    UIImageView             *_eyesBlinkAnimationImgVw;
    SmileyChucklesVC        *_smileyChucklesVC;
    UITapGestureRecognizer  *_eyesBlinkGesture;
    BOOL                    _isPlaying;
    TakePhotoPC             *_imagePickerControlr;
    
    AVAudioPlayer *appSoundPlayer;
 
}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) AVAudioPlayer *appSoundPlayer;
@property (strong, nonatomic) NSURL *soundFileURL;
@end

