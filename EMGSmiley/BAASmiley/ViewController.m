//
//  ViewController.m
//  BAASmiley
//
//  Created by Apple on 25/03/15.
//
//

#import "ViewController.h"
#import "SmileyChucklesVC.h"

#import "TakePhotoPC.h"
#import "Define.h"
#import "UIImage+animatedGIF.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _smileyChucklesVC = [[SmileyChucklesVC alloc]init];
    _imagePickerControlr      = [[TakePhotoPC alloc]init];

    
    self.navigationController.navigationBarHidden = NO;
    
    _eyesBlinkVW = [[UIView alloc]init];
    _eyesBlinkAnimationImgVw = [[UIImageView alloc]init];

    
  
    

    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    
   // [self configureAudioPlayer];
    
    //[self playSoundFXnamed:@"Sound-MagicToggleButton-AudioJungle.mp3" Loop: NO];


    /* EYES BLINK VIEW */

    [_eyesBlinkVW setFrame:CGRectMake(0, 0, 1024, 768)];
    [_eyesBlinkVW setBackgroundColor:[UIColor colorWithRed:(255.0f/255) green:(144.0f/255) blue:(0.0f/255)  alpha:1.0f]];
    [self.view addSubview:_eyesBlinkVW];
    
    /* VIEW TAPGESTURE */
    
    _eyesBlinkGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(eyesVWTapped)];
    [_eyesBlinkVW addGestureRecognizer:_eyesBlinkGesture];
    
    /* GIF ANIMATION */
    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"Eye-Blink" withExtension:@"gif"];
    UIImage *testImage1 = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url1]];
    
    [_eyesBlinkAnimationImgVw setFrame:CGRectMake(700, 470, 300, 300)];
    _eyesBlinkAnimationImgVw.backgroundColor=[UIColor clearColor];
    _eyesBlinkAnimationImgVw.animationImages=testImage1.images;
    _eyesBlinkAnimationImgVw.animationDuration=2.5;
    _eyesBlinkAnimationImgVw.animationRepeatCount=0;
    _eyesBlinkAnimationImgVw.userInteractionEnabled = NO;
    [_eyesBlinkVW addSubview:_eyesBlinkAnimationImgVw];
     [_eyesBlinkAnimationImgVw startAnimating];
}

-(void)eyesVWTapped
{
    
        [_eyesBlinkAnimationImgVw stopAnimating];
        [_eyesBlinkAnimationImgVw removeFromSuperview];
    
    if(![self.navigationController.topViewController isKindOfClass:[_smileyChucklesVC class]]) {
        
        [_audioPlayer stop];
        _audioPlayer = nil;


        [self.navigationController pushViewController:_smileyChucklesVC animated:NO];
    }
    
}
/* AUDIO PLAYER */

- (void)configureAudioPlayer {
    
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:@"Sound-MagicToggleButton-AudioJungle" withExtension:@"mp3"];
    NSError *error;
    if(!self.audioPlayer)
    {
        self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    }
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    self.audioPlayer.delegate=self;
    [self.audioPlayer setNumberOfLoops:-1];
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer setMeteringEnabled:YES];
    [self setAudioPlayer:_audioPlayer];
    [self.audioPlayer play];
    
    
    
    
    
//    SystemSoundID soundID;
//    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Sound-MagicToggleButton-AudioJungle.mp3", [[NSBundle mainBundle] resourcePath]]];
//    
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundID);
//    AudioServicesPlaySystemSound (soundID);
    
    
}

-(BOOL) playSoundFXnamed: (NSString*) vSFXName Loop: (BOOL) vLoop
{
    NSError *error;
    
    NSBundle* bundle = [NSBundle mainBundle];
    
    NSString* bundleDirectory = (NSString*)[bundle bundlePath];
    
    NSURL *url = [NSURL fileURLWithPath:[bundleDirectory stringByAppendingPathComponent:vSFXName]];
    
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    if(vLoop)
        audioPlayer.numberOfLoops = -1;
    else
        audioPlayer.numberOfLoops = 0;
    
    BOOL success = YES;
    
    if (audioPlayer == nil)
    {
        success = NO;
    }
    else
    {
        success = [audioPlayer play];
    }
    return success;
}




- (void)configureAudioSession {
    NSError *error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (error) {
        NSLog(@"Error setting category: %@", [error description]);
    }
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationLandscapeLeft;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
