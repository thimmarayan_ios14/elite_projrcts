//
//  AudioPlayer.h
//  MemoryTiles
//
//  Created by Purush V on 28/01/13.
//  Copyright (c) 2013 Purush V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
@interface AudioPlayer : UIViewController<AVAudioPlayerDelegate>
{
    SystemSoundID systemSoundID;
    AVAudioPlayer *audioPlayer;
    UIButton *StartStopSound;
}
@property (nonatomic, retain)AVAudioPlayer *audioPlayer;
@property (nonatomic, retain) IBOutlet UIButton *StartStopSound;

-(void)playSoundWithFile:(NSString *)fileName;
@end
