//
//  AudioPlayer.m
//  MemoryTiles
//
//  Created by Purush V on 28/01/13.
//  Copyright (c) 2013 Purush V. All rights reserved.
//

#import "AudioPlayer.h"

@interface AudioPlayer ()

@end

@implementation AudioPlayer
@synthesize audioPlayer,StartStopSound;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
-(void)playSoundWithFile:(NSString *)fileName
{
    // Get the file path to the song to play.
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",fileName]
                                                         ofType:@"mp3"];
            NSError * error = NULL;
    // Convert the file path to a URL.
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    filePath = nil;
    //Initialize the AVAudioPlayer.
    audioPlayer = [[AVAudioPlayer alloc]
                   initWithContentsOfURL:fileURL error:&error];
    fileURL = nil;
    // Preloads the buffer and prepares the audio for playing.
    audioPlayer.delegate = self;
    if(audioPlayer == NULL)
    {
        NSLog( @"error in creating AVAudioPlayer - %@ %@", [error domain], [error localizedDescription] );
    }
    else{
        
        [audioPlayer prepareToPlay];
        audioPlayer.currentTime = 0;
        [audioPlayer play];
    }
}
-(void)viewDidUnload{
    
    [super viewDidUnload];

}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player1 successfully:(BOOL)flag {
    if (player1 == audioPlayer) {
        
        audioPlayer = nil;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark ORIENTATION


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
        
        return YES;
    
    return NO;
}


@end
