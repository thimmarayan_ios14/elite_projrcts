//
//  DataCaptureVC.m
//  AppsWorld
//
//  Created by Mac User on 29/04/15.
//
//

#import "DataCaptureVC.h"
#import "AFNetworking.h"
#import "Define.h"
#import "FXReachability.h"
#import "SelectionGameVC.h"
#import "MBProgressHUD.h"
#import <unistd.h>
#import "PKCustomKeyboard.h"

#define LABLE_WIDTH 200
#define LABEL_HEIGHT 50
#define OPTION_BUTTON_WIDTH 200
#define OPTION_BUTTON_HEIGHT 50
#define OPTION_BUTTON_GAP 50

@interface DataCaptureVC ()<MBProgressHUDDelegate,DataCaptureDelegate>
{
    MBProgressHUD   *HUD;
    
    UIButton *statusBtn;
    int count;
    NSTimer *dismissTimer;
    UIView *emailLeftpaddingView,*emailRightpaddingView,*mobileLeftPaddingView,*mobileRightPaddingView;
}
@end

@implementation DataCaptureVC
@synthesize backBtn;
#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    _dataCaptureView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 768)];
    _dataCaptureView.backgroundColor = RGBA(86, 161, 213, 1);
    [self.view addSubview:_dataCaptureView];
    
    _detailsVW       = [[UIView alloc]initWithFrame:CGRectMake(0, 200,1024, 450)];
    _detailsVW.backgroundColor = [UIColor clearColor];
    [_dataCaptureView addSubview:_detailsVW];
    
    [[self navigationController]setNavigationBarHidden:YES animated:YES];
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
-(void)setFrame
{
    
    [self.welcomeLabl setFrame:CGRectMake(10,38, 500, 80)];
    
    [self.separatorVW setFrame:CGRectMake(CGRectGetMaxX([_welcomeLabl frame])+15, CGRectGetMinY([_welcomeLabl frame])+2, 1, CGRectGetHeight([_welcomeLabl frame])-5)];
    
    [self.pleaseEnterLabl setFrame:CGRectMake(CGRectGetMaxX([_separatorVW frame])+15, CGRectGetMinY([_welcomeLabl frame]), CGRectGetWidth([_welcomeLabl frame])-50, CGRectGetHeight([_welcomeLabl frame])/2.9)];
    
    [self.followingLabl setFrame:CGRectMake(CGRectGetMinX([_pleaseEnterLabl frame]), CGRectGetMaxY([_pleaseEnterLabl frame])+8, CGRectGetWidth([_pleaseEnterLabl frame]), CGRectGetHeight([_pleaseEnterLabl frame])+20)];
    
    [self.emailLabl setFrame:CGRectMake(CGRectGetMinX([_welcomeLabl frame]), CGRectGetMaxY([_welcomeLabl frame])+82, 120, 60)];
    
    [self.emailTxtfld setFrame:CGRectMake(CGRectGetMaxX([_emailLabl frame])+10, CGRectGetMinY([_emailLabl frame])+5, 400, CGRectGetHeight([_emailLabl frame])-10)];
    
    [self.emailLineVW setFrame:CGRectMake(CGRectGetMinX([_emailTxtfld frame])+20, CGRectGetMinY([_emailTxtfld frame])+40, CGRectGetWidth([_emailTxtfld frame])-2*20, 1)];
    
    emailLeftpaddingView.frame=CGRectMake(0,CGRectGetMinY([_emailTxtfld frame]),20,50);
    emailRightpaddingView.frame=CGRectMake(CGRectGetWidth(self.emailTxtfld.frame)-10,CGRectGetMinY([_emailTxtfld frame]),10,50);
    
    [self.mobileNumLabl setFrame:CGRectMake(CGRectGetMinX([_emailLabl frame]), CGRectGetMaxY([_emailLabl frame])+10, 380, CGRectGetHeight([_emailLabl frame]))];
    
    [self.mobileNumTxtfld setFrame:CGRectMake(CGRectGetMaxX([_mobileNumLabl frame])-35, CGRectGetMinY([_mobileNumLabl frame])+8, 400, CGRectGetHeight([_emailTxtfld frame]))];
    
    [self.phoneLineVW setFrame:CGRectMake(CGRectGetMinX([_mobileNumTxtfld frame])+20, CGRectGetMinY([_mobileNumTxtfld frame])+40, CGRectGetWidth([_mobileNumTxtfld frame])-2*20, 1)];
    
    mobileLeftPaddingView.frame=CGRectMake(0,CGRectGetMinY([_mobileNumTxtfld frame]),20,50);
    mobileRightPaddingView.frame=CGRectMake(CGRectGetWidth(_mobileNumTxtfld.frame)-10,CGRectGetMinY([_mobileNumTxtfld frame]),10,50);
    
    [self.genderQuestionLabl setFrame:CGRectMake(CGRectGetMinX([_emailLabl frame]), CGRectGetMaxY([_mobileNumLabl frame])+20, 190, CGRectGetHeight([_emailLabl frame]))];
    
    count=0;
    
    statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusBtn.frame =  CGRectMake(0,0,CGRectGetWidth(self.view.frame),50);
    [statusBtn setBackgroundColor:[UIColor clearColor]];
    [statusBtn addTarget:self action:@selector(statusBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:statusBtn];
    
    _maleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _maleBtn.frame = CGRectMake(CGRectGetMaxX([_genderQuestionLabl frame])-30,CGRectGetMinY([_genderQuestionLabl frame])+5,200, OPTION_BUTTON_HEIGHT);
    maleBtnGradient = [CAGradientLayer layer];
    maleBtnGradient.frame = self.maleBtn.layer.bounds;
    maleBtnGradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    maleBtnGradient.cornerRadius = self.maleBtn.layer.cornerRadius;
    [self.maleBtn.layer addSublayer:maleBtnGradient];
    
    _maleBtn.layer.cornerRadius = 6;
    _maleBtn.clipsToBounds = YES;
    _maleBtn.backgroundColor = [UIColor whiteColor];
    [_maleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _maleBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    _maleBtn.exclusiveTouch = YES;
    [_maleBtn setTitle:@"APPLE USER" forState:UIControlStateNormal];
    _maleBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_maleBtn addTarget:self action:@selector(maleBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _femaleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _femaleBtn.frame = CGRectMake(CGRectGetMaxX([_maleBtn frame])+10,CGRectGetMinY([_maleBtn frame]), 220, OPTION_BUTTON_HEIGHT);
    
    femaleBtnGradient = [CAGradientLayer layer];
    femaleBtnGradient.frame = self.femaleBtn.layer.bounds;
    femaleBtnGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    femaleBtnGradient.cornerRadius = self.femaleBtn.layer.cornerRadius;
    [self.femaleBtn.layer addSublayer:femaleBtnGradient];
    
    [_femaleBtn setTitle:@"ANDROID USER" forState:UIControlStateNormal];
    _femaleBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _femaleBtn.backgroundColor = [UIColor whiteColor];
    _femaleBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_femaleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _femaleBtn.exclusiveTouch = YES;
    _femaleBtn.layer.cornerRadius = 6;
    _femaleBtn.clipsToBounds = YES;
    [_femaleBtn addTarget:self action:@selector(femaleBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self.roleQuestionLabl setFrame:CGRectMake(CGRectGetMinX([_emailLabl frame]), CGRectGetMaxY([_genderQuestionLabl frame])+10,400, CGRectGetHeight([_genderQuestionLabl frame]))];
    
    _developerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _developerBtn.frame = CGRectMake(CGRectGetMaxX([_roleQuestionLabl frame])-15,CGRectGetMinY([_roleQuestionLabl frame])+7, 100, OPTION_BUTTON_HEIGHT);
    developerGradient  = [CAGradientLayer layer];
    developerGradient.frame = self.developerBtn.layer.bounds;
    developerGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    developerGradient.cornerRadius = self.developerBtn.layer.cornerRadius;
    [self.developerBtn.layer addSublayer:developerGradient];
    
    [_developerBtn setTitle:@"CASH" forState:UIControlStateNormal];
    _developerBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _developerBtn.backgroundColor = [UIColor whiteColor];
    _developerBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_developerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _developerBtn.exclusiveTouch = YES;
    _developerBtn.layer.cornerRadius = 6;
    _developerBtn.clipsToBounds = YES;
    [_developerBtn addTarget:self action:@selector(developerBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _brandBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _brandBtn.frame = CGRectMake(CGRectGetMaxX([_developerBtn frame])+10,CGRectGetMinY([_developerBtn frame]),130, OPTION_BUTTON_HEIGHT);
    brandGradient  = [CAGradientLayer layer];
    brandGradient.frame = self.brandBtn.layer.bounds;
    brandGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    brandGradient.cornerRadius = self.brandBtn.layer.cornerRadius;
    [self.brandBtn.layer addSublayer:brandGradient];
    
    [_brandBtn setTitle:@"POINTS" forState:UIControlStateNormal];
    _brandBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _brandBtn.backgroundColor = [UIColor whiteColor];
    _brandBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_brandBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _brandBtn.exclusiveTouch = YES;
    _brandBtn.layer.cornerRadius = 6;
    _brandBtn.clipsToBounds = YES;
    [_brandBtn addTarget:self action:@selector(brandBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _marketerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _marketerBtn.frame = CGRectMake(CGRectGetMaxX([_brandBtn frame])+10,CGRectGetMinY([_developerBtn frame]),100,OPTION_BUTTON_HEIGHT);
    
    marketGradient  = [CAGradientLayer layer];
    marketGradient.frame = self.marketerBtn.layer.bounds;
    marketGradient.colors = [NSArray arrayWithObjects:
                             (id)RGBA(209, 210, 212, 1).CGColor,
                             (id)RGBA(250, 250, 250, 1).CGColor,nil];
    brandGradient.cornerRadius = self.marketerBtn.layer.cornerRadius;
    [self.marketerBtn.layer addSublayer:marketGradient];
    
    [_marketerBtn setTitle:@"MILES" forState:UIControlStateNormal];
    _marketerBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _marketerBtn.backgroundColor = [UIColor whiteColor];
    _marketerBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_marketerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _marketerBtn.exclusiveTouch = YES;
    _marketerBtn.layer.cornerRadius = 6;
    _marketerBtn.clipsToBounds = YES;
    [_marketerBtn addTarget:self action:@selector(marketerBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _otherBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _otherBtn.frame = CGRectMake(CGRectGetMaxX([_marketerBtn frame])+10,CGRectGetMinY([_developerBtn frame]),230, OPTION_BUTTON_HEIGHT);
    
    otherGradient  = [CAGradientLayer layer];
    otherGradient.frame = self.otherBtn.layer.bounds;
    otherGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    otherGradient.cornerRadius = self.otherBtn.layer.cornerRadius;
    [self.otherBtn.layer addSublayer:otherGradient];
    
    [_otherBtn setTitle:@"HUGS & KISSES" forState:UIControlStateNormal];
    _otherBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _otherBtn.backgroundColor = [UIColor whiteColor];
    _otherBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_otherBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _otherBtn.exclusiveTouch = YES;
    _otherBtn.layer.cornerRadius = 6;
    _otherBtn.clipsToBounds = YES;
    [_otherBtn addTarget:self action:@selector(otherBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self.locationQuestionLabl setFrame:CGRectMake(CGRectGetMinX([_emailLabl frame]), CGRectGetMaxY([_roleQuestionLabl frame])+20, 440, CGRectGetHeight([_emailLabl frame]))];
    
    _localBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _localBtn.frame = CGRectMake(CGRectGetMaxX([_locationQuestionLabl frame])+10,CGRectGetMinY([_locationQuestionLabl frame])+8,100, OPTION_BUTTON_HEIGHT);
    
    localGradient  = [CAGradientLayer layer];
    localGradient.frame = self.localBtn.layer.bounds;
    localGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    localGradient.cornerRadius = self.localBtn.layer.cornerRadius;
    [self.localBtn.layer addSublayer:localGradient];
    
    [_localBtn setTitle:@"CAR" forState:UIControlStateNormal];
    _localBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _localBtn.backgroundColor = [UIColor whiteColor];
    _localBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_localBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _localBtn.exclusiveTouch = YES;
    _localBtn.layer.cornerRadius = 6;
    _localBtn.clipsToBounds = YES;
    [_localBtn addTarget:self action:@selector(localBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _nationalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nationalBtn.frame = CGRectMake(CGRectGetMaxX([_localBtn frame])+10,CGRectGetMinY([_localBtn frame]),120, OPTION_BUTTON_HEIGHT);
    
    nationalGradient  = [CAGradientLayer layer];
    nationalGradient.frame = self.nationalBtn.layer.bounds;
    nationalGradient.colors = [NSArray arrayWithObjects:
                               (id)RGBA(209, 210, 212, 1).CGColor,
                               (id)RGBA(250, 250, 250, 1).CGColor,nil];
    nationalGradient.cornerRadius = self.nationalBtn.layer.cornerRadius;
    [self.nationalBtn.layer addSublayer:nationalGradient];
    
    [_nationalBtn setTitle:@"TRAIN" forState:UIControlStateNormal];
    _nationalBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _nationalBtn.backgroundColor = [UIColor whiteColor];
    _nationalBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_nationalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _nationalBtn.exclusiveTouch = YES;
    _nationalBtn.layer.cornerRadius = 6;
    _nationalBtn.clipsToBounds = YES;
    [_nationalBtn addTarget:self action:@selector(nationalBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _outOfStateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _outOfStateBtn.frame = CGRectMake(CGRectGetMaxX([_nationalBtn frame])+10,CGRectGetMinY([_localBtn frame]), 130, OPTION_BUTTON_HEIGHT);
    
    internationalGradient = [CAGradientLayer layer];
    internationalGradient.frame = self.outOfStateBtn.layer.bounds;
    internationalGradient.colors = [NSArray arrayWithObjects:
                                    (id)RGBA(209, 210, 212, 1).CGColor,
                                    (id)RGBA(250, 250, 250, 1).CGColor,nil];
    internationalGradient.cornerRadius = self.outOfStateBtn.layer.cornerRadius;
    [self.outOfStateBtn.layer addSublayer:internationalGradient];
    
    [_outOfStateBtn setTitle:@"PLANE" forState:UIControlStateNormal];
    _outOfStateBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _outOfStateBtn.backgroundColor = [UIColor whiteColor];
    _outOfStateBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _outOfStateBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_outOfStateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _outOfStateBtn.exclusiveTouch = YES;
    _outOfStateBtn.layer.cornerRadius = 6;
    _outOfStateBtn.clipsToBounds = YES;
    [_outOfStateBtn addTarget:self action:@selector(internationalBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self.questionLabl setFrame:CGRectMake(CGRectGetMinX([_emailLabl frame]), CGRectGetMaxY([_locationQuestionLabl frame]),210 , CGRectGetHeight([_emailLabl frame])+40)];
    
    _optionBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    _optionBtn1.frame =  CGRectMake(CGRectGetMaxX([_questionLabl frame])-35,CGRectGetMinY([_questionLabl frame])+35,210, OPTION_BUTTON_HEIGHT);
    
    option1Gradient  = [CAGradientLayer layer];
    option1Gradient.frame = self.optionBtn1.layer.bounds;
    option1Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    option1Gradient.cornerRadius = self.optionBtn1.layer.cornerRadius;
    [self.optionBtn1.layer addSublayer:option1Gradient];
    
    [_optionBtn1 setTitle:@"GOLD DIGGER IPA" forState:UIControlStateNormal];
    _optionBtn1.titleLabel.textAlignment = NSTextAlignmentCenter;
    _optionBtn1.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_optionBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _optionBtn1.exclusiveTouch = YES;
    _optionBtn1.layer.cornerRadius = 6;
    _optionBtn1.clipsToBounds = YES;
    [_optionBtn1 addTarget:self action:@selector(optionBtn1Tapped) forControlEvents:UIControlEventTouchUpInside];
    
    _optionBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    _optionBtn2.frame =  CGRectMake(CGRectGetMaxX([_optionBtn1 frame])+10,CGRectGetMinY([_optionBtn1 frame]),180, OPTION_BUTTON_HEIGHT);
    
    option2Gradient  = [CAGradientLayer layer];
    option2Gradient.frame = self.optionBtn2.layer.bounds;
    option2Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    option2Gradient.cornerRadius = self.optionBtn2.layer.cornerRadius;
    [self.optionBtn2.layer addSublayer:option2Gradient];
    
    [_optionBtn2 setTitle:@"OKTOBERFEST" forState:UIControlStateNormal];
    _optionBtn2.titleLabel.textAlignment = NSTextAlignmentCenter;
    _optionBtn2.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_optionBtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _optionBtn2.exclusiveTouch = YES;
    _optionBtn2.layer.cornerRadius = 6;
    _optionBtn2.clipsToBounds = YES;
    [_optionBtn2 addTarget:self action:@selector(optionBtn2Tapped) forControlEvents:UIControlEventTouchUpInside];
    
    _optionBtn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    _optionBtn3.frame =  CGRectMake(CGRectGetMaxX([_optionBtn2 frame])+10,CGRectGetMinY([_optionBtn1 frame]),130, OPTION_BUTTON_HEIGHT);
    
    option3Gradient  = [CAGradientLayer layer];
    option3Gradient.frame = self.optionBtn3.layer.bounds;
    option3Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    option3Gradient.cornerRadius = self.optionBtn3.layer.cornerRadius;
    [self.optionBtn3.layer addSublayer:option3Gradient];
    
    [_optionBtn3 setTitle:@"WITBERRY" forState:UIControlStateNormal];
    _optionBtn3.titleLabel.textAlignment = NSTextAlignmentCenter;
    _optionBtn3.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_optionBtn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _optionBtn3.exclusiveTouch = YES;
    _optionBtn3.layer.cornerRadius = 6;
    _optionBtn3.clipsToBounds = YES;
    [_optionBtn3 addTarget:self action:@selector(optionBtn3Tapped) forControlEvents:UIControlEventTouchUpInside];
    
    _optionBtn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    _optionBtn4.frame =  CGRectMake(CGRectGetMaxX([_optionBtn3 frame])+10,CGRectGetMinY([_optionBtn1 frame]),280, OPTION_BUTTON_HEIGHT);
    
    option4Gradient  = [CAGradientLayer layer];
    option4Gradient.frame = self.optionBtn4.layer.bounds;
    option4Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    option3Gradient.cornerRadius = self.optionBtn4.layer.cornerRadius;
    [self.optionBtn4.layer addSublayer:option4Gradient];
    
    [_optionBtn4 setTitle:@"IRON HILL LIGHT LAGER" forState:UIControlStateNormal];
    _optionBtn4.titleLabel.textAlignment = NSTextAlignmentCenter;
    _optionBtn4.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:25];
    [_optionBtn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _optionBtn4.exclusiveTouch = YES;
    _optionBtn4.layer.cornerRadius = 6;
    _optionBtn4.clipsToBounds = YES;
    [_optionBtn4 addTarget:self action:@selector(optionBtn4Tapped) forControlEvents:UIControlEventTouchUpInside];
    
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame =  CGRectMake(CGRectGetWidth(self.view.frame)-150,690, 130, 49);
    [_submitBtn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    _submitBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _submitBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:35];
    _submitBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_submitBtn setBackgroundColor:RGBA(241, 137, 3, 1)];
    _submitBtn.layer.cornerRadius = 6;
    _submitBtn.clipsToBounds = YES;
    _submitBtn.exclusiveTouch = YES;
    _submitBtn.userInteractionEnabled=YES;
    [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame =  CGRectMake(15,690, 130, 49);
    [backBtn setTitle:@"BACK" forState:UIControlStateNormal];
    backBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    backBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:35];
    backBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [backBtn setBackgroundColor:RGBA(15,55,100, 1)];
    backBtn.layer.cornerRadius = 6;
    backBtn.clipsToBounds = YES;
    backBtn.exclusiveTouch = YES;
    backBtn.userInteractionEnabled=YES;
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
}

-(void)backBtnTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setFrame];
    
    [_dataCaptureView addSubview:_welcomeLabl];
    
    [_dataCaptureView addSubview:_separatorVW];
    
    [_dataCaptureView addSubview:_pleaseEnterLabl];
    
    [_dataCaptureView addSubview:_followingLabl];
    
    [_dataCaptureView addSubview:_emailLabl];
    
    CAGradientLayer *emailGradient = [CAGradientLayer layer];
    emailGradient.frame = self.emailTxtfld.layer.bounds;
    emailGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    emailGradient.cornerRadius = self.emailTxtfld.layer.cornerRadius;
    [self.emailTxtfld.layer addSublayer:emailGradient];
    
    [_dataCaptureView addSubview:_emailTxtfld];
    
    [_dataCaptureView addSubview:_emailLineVW];
    
    [_dataCaptureView addSubview: _mobileNumLabl];
    
    CAGradientLayer *mobileNoGradient = [CAGradientLayer layer];
    mobileNoGradient.frame = self.mobileNumTxtfld.layer.bounds;
    mobileNoGradient.colors = [NSArray arrayWithObjects:
                               (id)RGBA(209, 210, 212, 1).CGColor,
                               (id)RGBA(250, 250, 250, 1).CGColor,nil];
    mobileNoGradient.cornerRadius = self.mobileNumTxtfld.layer.cornerRadius;
    [self.mobileNumTxtfld.layer addSublayer:mobileNoGradient];
    
    [_dataCaptureView addSubview: _mobileNumTxtfld];
    
    [_dataCaptureView addSubview:_phoneLineVW];
    
    [_dataCaptureView addSubview:_genderQuestionLabl];
    
    [_dataCaptureView addSubview:_maleBtn];
    
    [_dataCaptureView addSubview:_femaleBtn];
    
    [_dataCaptureView addSubview:_roleQuestionLabl];
    
    [_dataCaptureView addSubview:_developerBtn];
    
    [_dataCaptureView addSubview:_brandBtn];
    
    [_dataCaptureView addSubview:_marketerBtn];
    
    [_dataCaptureView addSubview:_otherBtn];
    
    [_dataCaptureView addSubview:_locationQuestionLabl];
    
    [_dataCaptureView addSubview:_localBtn];
    
    [_dataCaptureView addSubview:_nationalBtn];
    
    [_dataCaptureView addSubview:_outOfStateBtn];
    
    [_dataCaptureView addSubview:_questionLabl];
    
    [_dataCaptureView addSubview:_optionBtn1];
    
    [_dataCaptureView addSubview:_optionBtn2];
    
    [_dataCaptureView addSubview:_optionBtn3];
    
    [_dataCaptureView addSubview:_optionBtn4];
    
    [_dataCaptureView addSubview:_submitBtn];
    
     [_dataCaptureView addSubview:backBtn];
    
}
-(BOOL)prefersStatusBarHidden
{
    return  YES ;
}
#pragma mark - Custom Setter/Getter

-(UILabel *)welcomeLabl
{
    if (!_welcomeLabl) {
        
        _welcomeLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _welcomeLabl.text = @"WELCOME";
        _welcomeLabl.font = [UIFont fontWithName:HELVETICA_THIN size:100];
        _welcomeLabl.textAlignment = NSTextAlignmentCenter;
        _welcomeLabl.backgroundColor = [UIColor clearColor];
        _welcomeLabl.textColor = [UIColor whiteColor];
    }
    return _welcomeLabl;
}

-(UIView *)separatorVW
{
    if (!_separatorVW) {
        
        _separatorVW = [[UIView alloc]initWithFrame:CGRectZero];
        _separatorVW.backgroundColor = RGBA(239, 137, 34, 1);
        
    }
    return _separatorVW;
}

-(UILabel *)pleaseEnterLabl
{
    if (!_pleaseEnterLabl) {
        
        _pleaseEnterLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _pleaseEnterLabl.text = @"PLEASE ENTER THE";
        _pleaseEnterLabl.font = [UIFont fontWithName:HELVETICA_THIN size:35];
        _pleaseEnterLabl.numberOfLines = 2;
        _pleaseEnterLabl.textAlignment = NSTextAlignmentLeft;
        _pleaseEnterLabl.backgroundColor = [UIColor clearColor];
        _pleaseEnterLabl.textColor = [UIColor whiteColor];
    }
    return _pleaseEnterLabl;
}
-(UILabel *)followingLabl
{
    if (!_followingLabl) {
        
        _followingLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _followingLabl.text = @"FOLLOWING";
        _followingLabl.font = [UIFont fontWithName:HELVETICA_THIN size:55];
        _followingLabl.numberOfLines = 2;
        _followingLabl.textAlignment = NSTextAlignmentLeft;
        _followingLabl.backgroundColor = [UIColor clearColor];
        _followingLabl.textColor = [UIColor whiteColor];
    }
    return _followingLabl;
}
-(UILabel *)emailLabl
{
    if (!_emailLabl) {
        
        _emailLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _emailLabl.text = @" EMAIL :";
        _emailLabl.font = [UIFont fontWithName:HELVETICA_THIN size:30];
        _emailLabl.textAlignment = NSTextAlignmentLeft;
        _emailLabl.backgroundColor = [UIColor clearColor];
        _emailLabl.textColor = [UIColor whiteColor];
    }
    return _emailLabl;
}
-(UITextField *)emailTxtfld
{
    if (!_emailTxtfld) {
        
        _emailTxtfld = [[UITextField alloc] initWithFrame:CGRectZero];
        _emailTxtfld.delegate = self;
        _emailTxtfld.backgroundColor = [UIColor whiteColor];
        _emailTxtfld.layer.cornerRadius = 6;
        _emailTxtfld.font = [UIFont fontWithName:HELVETICA_THIN size:30];
        _emailTxtfld.textAlignment = NSTextAlignmentLeft;
        _emailTxtfld.textColor = [UIColor blackColor];
        _emailTxtfld.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _emailTxtfld.keyboardType = UIKeyboardTypeEmailAddress;
        emailLeftpaddingView = [[UIView alloc]init];
        emailRightpaddingView = [[UIView alloc]init];
        [_emailTxtfld addSubview:emailRightpaddingView];
        [_emailTxtfld addSubview:emailLeftpaddingView];
        [_emailTxtfld setLeftViewMode:UITextFieldViewModeAlways];
        [_emailTxtfld setLeftView:emailLeftpaddingView];
        [_emailTxtfld setRightView:emailRightpaddingView];
        [_emailTxtfld setRightViewMode:UITextFieldViewModeAlways];
        
    }
    return _emailTxtfld;
}
-(UIView *)emailLineVW
{
    if (!_emailLineVW) {
        
        _emailLineVW = [[UIView alloc]initWithFrame:CGRectZero];
        _emailLineVW.backgroundColor = [UIColor blackColor];
    }
    return _emailLineVW;
}

-(UILabel *)mobileNumLabl
{
    if (!_mobileNumLabl) {
        
        _mobileNumLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _mobileNumLabl.text = @" CELL PHONE NUMBER :";
        _mobileNumLabl.font = [UIFont fontWithName:HELVETICA_THIN size:30];
        _mobileNumLabl.textAlignment = NSTextAlignmentLeft;
        _mobileNumLabl.backgroundColor = [UIColor clearColor];
        _mobileNumLabl.textColor = [UIColor whiteColor];
    }
    return _mobileNumLabl;
}
-(UITextField *)mobileNumTxtfld
{
    if (!_mobileNumTxtfld) {
        
        _mobileNumTxtfld = [[UITextField alloc] initWithFrame:CGRectZero];
        _mobileNumTxtfld.delegate = self;
        _mobileNumTxtfld.backgroundColor = [UIColor whiteColor];
        _mobileNumTxtfld.layer.cornerRadius = 6;
        _mobileNumTxtfld.clearButtonMode = YES;
        _mobileNumTxtfld.font = [UIFont fontWithName:HELVETICA_THIN size:30];
        _mobileNumTxtfld.textAlignment = NSTextAlignmentLeft;
        _mobileNumTxtfld.textColor = [UIColor blackColor];
        _mobileNumTxtfld.keyboardType = UIKeyboardTypeNumberPad;
        mobileLeftPaddingView = [[UIView alloc]init];
        mobileRightPaddingView = [[UIView alloc]init];
        [_mobileNumTxtfld addSubview:mobileRightPaddingView];
        [_mobileNumTxtfld addSubview:mobileLeftPaddingView];
        [_mobileNumTxtfld setLeftViewMode:UITextFieldViewModeAlways];
        [_mobileNumTxtfld setLeftView:mobileLeftPaddingView];
        [_mobileNumTxtfld setRightView:mobileRightPaddingView];
        [_mobileNumTxtfld setRightViewMode:UITextFieldViewModeAlways];
    }
    return _mobileNumTxtfld;
}
-(UIView *)phoneLineVW
{
    if (!_phoneLineVW) {
        
        _phoneLineVW = [[UIView alloc]initWithFrame:CGRectZero];
        _phoneLineVW.backgroundColor = [UIColor blackColor];
        
    }
    return _phoneLineVW;
}

-(UILabel *)genderQuestionLabl
{
    if (!_genderQuestionLabl) {
        
        _genderQuestionLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _genderQuestionLabl.text = @" ARE YOU :";
        _genderQuestionLabl.font = [UIFont fontWithName:HELVETICA_THIN size:30];
        _genderQuestionLabl.textColor = [UIColor whiteColor];
        _genderQuestionLabl.backgroundColor = [UIColor clearColor];
        _genderQuestionLabl.textAlignment = NSTextAlignmentLeft;
    }
    return _genderQuestionLabl;
}
-(UILabel *)roleQuestionLabl
{
    if (!_roleQuestionLabl) {
        
        _roleQuestionLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _roleQuestionLabl.text = @" BEST LOYALTY REWARDS :";
        _roleQuestionLabl.font = [UIFont fontWithName:HELVETICA_THIN size:30];
        _roleQuestionLabl.textColor = [UIColor whiteColor];
        _roleQuestionLabl.backgroundColor = [UIColor clearColor];
        _roleQuestionLabl.textAlignment = NSTextAlignmentLeft;
    }
    return _roleQuestionLabl;
}
-(UILabel *)locationQuestionLabl
{
    if (!_locationQuestionLabl) {
        
        _locationQuestionLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _locationQuestionLabl.text = @" PREFERRED TRANSPORTATION :";
       // _locationQuestionLabl.adjustsFontSizeToFitWidth = YES;
        _locationQuestionLabl.font =[UIFont fontWithName:HELVETICA_THIN size:30];
        _locationQuestionLabl.textColor = [UIColor whiteColor];
        _locationQuestionLabl.backgroundColor = [UIColor clearColor];
        _locationQuestionLabl.textAlignment = NSTextAlignmentLeft;
    }
    return _locationQuestionLabl;
}
-(UILabel *)questionLabl
{
    if (!_questionLabl) {
        
        _questionLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _questionLabl.text = @" FAVORITE \n BEER :";
        _questionLabl.numberOfLines=0;
        _questionLabl.font =[UIFont fontWithName:HELVETICA_THIN size:30];
        _questionLabl.textColor = [UIColor whiteColor];
        _questionLabl.backgroundColor = [UIColor clearColor];
        _questionLabl.textAlignment = NSTextAlignmentLeft;
    }
    return _questionLabl;
}

#pragma mark - Textfield Delegation Method


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _customKeyboard = [[PKCustomKeyboard alloc] init];
    _customKeyboard.dataCaptureDelegate = self;
    _customKeyboard.dataCapture=@"DataCapture";
    [_customKeyboard setTextView:self.emailTxtfld];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTxtfld)
    {
        if (_emailTxtfld.text.length==0)
        {
            [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter the email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else
        {
            if ([self validateEmailWithString:textField.text]) {
                
                [_mobileNumTxtfld becomeFirstResponder];
                
                return YES;
            }
            else
            {
                [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
                return NO;
            }
        }
    }
    else if(textField == _mobileNumTxtfld){
        
        if (_mobileNumTxtfld.text.length==0) {
            
            [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please Enter the mobile number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else{
            if(([_mobileNumTxtfld.text length]==14 )){
                
                [textField resignFirstResponder];
                return YES;
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter a valid mobile number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                return NO;
            }
        }
    }
    
    return YES;
    
}
-(void)textfieldShouldReturn
{
    if (_emailTxtfld)
    {
        if (_emailTxtfld.text.length==0)
        {
            [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter the email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else
        {
            if ([self validateEmailWithString:_emailTxtfld.text]) {
                
                [_mobileNumTxtfld becomeFirstResponder];
                
            }
            else
            {
                [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
            }
        }
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==_mobileNumTxtfld)
    {
        //    NSUInteger newLength = [phoneNoTxtFld.text length] + [string length] - range.length;
        //    return (newLength > 10) ? NO : YES;
        
        NSString *validRegEx =@"^[0-9]*$"; //change this regular expression as your requirement
        NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
        if (myStringMatchesRegEx)
        {
            int length = [self getLength:_mobileNumTxtfld.text];
            
            if(length == 10)
            {
                if(range.length == 0)
                    return NO;
            }
            
            if(length == 3)
            {
                NSString *num = [self formatNumber:_mobileNumTxtfld.text];
                textField.text = [NSString stringWithFormat:@"(%@) ",num];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
            }
            else if(length == 6)
            {
                NSString *num = [self formatNumber:_mobileNumTxtfld.text];
                textField.text = [NSString stringWithFormat:@"(%@)-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
        }
        else
        {
            
            NSString *mess1 = [NSString stringWithFormat:@"%@ is not a number format!",string];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:mess1
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            alert.tag = 1;
        }
        
    }
    return YES;
}

#pragma mark - AlertView clickedButtonAtIndex method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        
        if ( [_mobileNumTxtfld.text length] > 0)
            _mobileNumTxtfld.text = [_mobileNumTxtfld.text substringToIndex:[_mobileNumTxtfld.text length] - 1];
        
    }
}


#pragma mark -
#pragma mark USA mobile no format checking

-(NSString*)formatNumber:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        
    }
    
    return mobileNumber;
}

-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
    
}

#pragma mark - Button Action Methods

-(void)maleBtnTapped
{
    [_mobileNumTxtfld resignFirstResponder];
    [_emailTxtfld resignFirstResponder];
    
    genderTapped = YES;
    
    _genderStr = @"M";
    
    maleBtnGradient.colors = nil;
    
    _maleBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_maleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    femaleBtnGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_femaleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
-(void)femaleBtnTapped
{
    [_mobileNumTxtfld resignFirstResponder];
    [_emailTxtfld resignFirstResponder];
    
    genderTapped = YES;
    
    _genderStr = @"F";
    
    femaleBtnGradient.colors = nil;
    
    _femaleBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_femaleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    maleBtnGradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_maleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
-(void)developerBtnTapped
{
    typeTapped = YES;
    
    _typeStr = @"D";
    
    developerGradient.colors = nil;
    
    _developerBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_developerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    brandGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_brandBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    marketGradient.colors = [NSArray arrayWithObjects:
                             (id)RGBA(209, 210, 212, 1).CGColor,
                             (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_marketerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    otherGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_otherBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
-(void)brandBtnTapped
{
    typeTapped = YES;
    
    _typeStr = @"B";
    
    brandGradient.colors = nil;
    
    _brandBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_brandBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    developerGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_developerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    marketGradient.colors = [NSArray arrayWithObjects:
                             (id)RGBA(209, 210, 212, 1).CGColor,
                             (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_marketerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    otherGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_otherBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}
-(void)marketerBtnTapped
{
    typeTapped = YES;
    
    _typeStr = @"M";
    
    marketGradient.colors = nil;
    
    _marketerBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_marketerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    developerGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_developerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    brandGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_brandBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    otherGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_otherBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}
-(void)otherBtnTapped
{
    typeTapped = YES;
    
    _typeStr = @"O";
    
    otherGradient.colors = nil;
    
    _otherBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_otherBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    developerGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_developerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    brandGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_brandBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    marketGradient.colors = [NSArray arrayWithObjects:
                             (id)RGBA(209, 210, 212, 1).CGColor,
                             (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_marketerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
-(void)localBtnTapped
{
    locationTapped = YES;
    
    _locationStr = @"L";
    
    localGradient.colors = nil;
    
    _localBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_localBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    nationalGradient.colors = [NSArray arrayWithObjects:
                               (id)RGBA(209, 210, 212, 1).CGColor,
                               (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_nationalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    internationalGradient.colors = [NSArray arrayWithObjects:
                                    (id)RGBA(209, 210, 212, 1).CGColor,
                                    (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_outOfStateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
-(void)nationalBtnTapped
{
    locationTapped = YES;
    
    _locationStr = @"N";
    
    nationalGradient.colors= nil;
    
    _nationalBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_nationalBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    localGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_localBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    internationalGradient.colors = [NSArray arrayWithObjects:
                                    (id)RGBA(209, 210, 212, 1).CGColor,
                                    (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_outOfStateBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
-(void)internationalBtnTapped
{
    locationTapped = YES;
    
    _locationStr = @"IN";
    
    internationalGradient.colors = nil;
    
    _outOfStateBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_outOfStateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    localGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_localBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    nationalGradient.colors = [NSArray arrayWithObjects:
                               (id)RGBA(209, 210, 212, 1).CGColor,
                               (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_nationalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}
-(void)optionBtn1Tapped
{
    questionTapped = YES;
    
    _optionStr = @"B";
    
    option1Gradient.colors = nil;
    
    _optionBtn1.backgroundColor = RGBA(0, 32, 91, 1);
    [_optionBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    option2Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    option3Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    option4Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
}
-(void)optionBtn2Tapped
{
    questionTapped = YES;
    
    _optionStr = @"W";
    
    option2Gradient.colors = nil;
    
    _optionBtn2.backgroundColor = RGBA(0, 32, 91, 1);
    [_optionBtn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    option1Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    option3Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    option4Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
-(void)optionBtn3Tapped
{
    questionTapped = YES;
    
    _optionStr = @"L";
    
    option3Gradient.colors = nil;
    
    _optionBtn3.backgroundColor = RGBA(0, 32, 91, 1);
    [_optionBtn3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    option1Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    option2Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    option4Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
}
-(void)optionBtn4Tapped
{
    questionTapped = YES;
    
    _optionStr = @"S";
    
    option4Gradient.colors = nil;
    
    _optionBtn4.backgroundColor = RGBA(0, 32, 91, 1);
    [_optionBtn4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    option1Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    option2Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    option3Gradient.colors = [NSArray arrayWithObjects:
                              (id)RGBA(209, 210, 212, 1).CGColor,
                              (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_optionBtn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}

-(void)submitBtnTapped
{
    if ([_emailTxtfld.text length]==0 || [_mobileNumTxtfld.text length]==0 || !genderTapped || !typeTapped || !locationTapped || !questionTapped) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please fill all the fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        if ([self validateEmailWithString:_emailTxtfld.text] && [_mobileNumTxtfld.text length]==14 ) {
            
            NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSRange range = [_emailTxtfld.text rangeOfCharacterFromSet:whitespace];
            if (range.location != NSNotFound) {
                // There is whitespace.
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else
            {
                _submitBtn.userInteractionEnabled=NO;
                [self playBtnTapped];
            }
           
        }
        else
        {
            if (![self validateEmailWithString:_emailTxtfld.text]) {
                
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
            }
            else if ([self validateEmailWithString:_emailTxtfld.text])
            {
                
                NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                NSRange range = [_emailTxtfld.text rangeOfCharacterFromSet:whitespace];
                if (range.location != NSNotFound) {
                    // There is whitespace.
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alertView show];
                    
                }

            }
            else if ([_mobileNumTxtfld.text length] != 14)
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter valid mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter valid email id and mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }
    }
}

#pragma mark - Registeration Service Method

-(void)playBtnTapped
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if([FXReachability isReachable]){
        
        NSDictionary *inputDict = @{@"email":self.emailTxtfld.text,@"phone":_mobileNumTxtfld.text,@"type":_typeStr,@"drink":_optionStr,@"location":_locationStr,@"gender":_genderStr,@"signup":@"yes",@"device":@"iPad"};
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *urlString=[NSString stringWithFormat:@"%@",LOGIN_SERVICE];
        [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFormData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"input"];
        }
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  NSDictionary *resultDict = (NSDictionary*)responseObject;
                  NSLog(@"%@",resultDict);
                  
                  [self hideHud];
                  
                  [[NSUserDefaults standardUserDefaults]setObject:[resultDict valueForKey:@"userId"] forKey:USERID];
                  
                  [[NSUserDefaults standardUserDefaults]setObject:[[[resultDict valueForKey:@"appDetail"]valueForKey:@"100ME"]stringValue] forKey:MEMORYGAMESTATUS];
                  [[NSUserDefaults standardUserDefaults]setObject:[[[resultDict valueForKey:@"appDetail"]valueForKey:@"100SC"]stringValue] forKey:SCRATCHGAMESSTATUS];
                  [[NSUserDefaults standardUserDefaults]setObject:[[[resultDict valueForKey:@"appDetail"]valueForKey:@"100SP"]stringValue] forKey:SPOTDIFFGAMESTATUS];
                  
                  if ([[[resultDict valueForKey:@"code"]stringValue] isEqualToString:@"1000"])
                  {
                      SelectionGameVC *selectionGameVC = [[SelectionGameVC alloc]init];
                      [self.navigationController pushViewController:selectionGameVC animated:YES];
                      genderTapped = NO;
                      typeTapped = NO;
                      locationTapped = NO;
                      questionTapped = NO;
                  }
                  else if([[[resultDict valueForKey:@"code"]stringValue] isEqualToString:@"3000"])
                  {
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Email id already exists. Please enter different email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                      [alertView show];
                      _submitBtn.userInteractionEnabled=YES;
                  }
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  NSLog(@"%@",error);
                  
                  
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"There is a problem connecting to the server. Please check your internet connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
                  _submitBtn.userInteractionEnabled=YES;
                  
              }];
    }
    else{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Seems your internet is offline" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [offlineAlert show];
        _submitBtn.userInteractionEnabled=YES;
    }
    
}
#pragma mark - Email Validation

- (BOOL)validateEmailWithString:(NSString*)email
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; //[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
   /* NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];*/
}

#pragma mark-
#pragma mark - Hide Loading View

- (void)myTask
{
    // Do something usefull in here instead of sleeping ...
}

-(void)hideHud
{
    [HUD hide:YES];
    
    // sleep(3);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark - Status button Action

-(void)statusBtnTap:(id)sender
{
    count++;
    
    if (count==3)
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    
    dismissTimer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                   target:self
                                                 selector:@selector(reduceCount)
                                                 userInfo:nil
                                                  repeats:NO];
}

-(void)reduceCount
{
    count=0;
}

@end
