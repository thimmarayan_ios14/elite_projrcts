//
//  DataCaptureVC.h
//  AppsWorld
//
//  Created by Mac User on 29/04/15.
//
//

#import <UIKit/UIKit.h>
#import "PKCustomKeyboard.h"

@class PKCustomKeyboard;

@interface DataCaptureVC : UIViewController <UITextFieldDelegate>
{
    BOOL            genderTapped;
    BOOL            typeTapped;
    BOOL            locationTapped;
    BOOL            questionTapped;
    CAGradientLayer *maleBtnGradient, *femaleBtnGradient,*developerGradient, *brandGradient, *marketGradient, *otherGradient,*localGradient,
    *nationalGradient, *internationalGradient, *option1Gradient, *option2Gradient, *option3Gradient,*option4Gradient;
    
}

@property (nonatomic, strong)PKCustomKeyboard *customKeyboard;

@property (nonatomic,strong) UIView       *dataCaptureView;

@property (nonatomic,strong) UIView       *detailsVW;

@property (nonatomic,strong) UILabel      *welcomeLabl;

@property (nonatomic,strong) UIView       *separatorVW ;

@property (nonatomic,strong) UILabel      *pleaseEnterLabl;

@property (nonatomic,strong) UILabel      *followingLabl;

@property (nonatomic,strong) UILabel      *emailLabl;

@property (nonatomic,strong) UITextField  *emailTxtfld;

@property (nonatomic,strong) UILabel      *mobileNumLabl;

@property (nonatomic,strong) UITextField  *mobileNumTxtfld;

@property (nonatomic,strong) UILabel      *genderQuestionLabl, *roleQuestionLabl, *locationQuestionLabl;

@property (nonatomic,strong) UIButton     *maleBtn,  *femaleBtn;

@property (nonatomic,strong) UIButton     *developerBtn, *brandBtn, *marketerBtn, *otherBtn;

@property (nonatomic,strong) UIButton     *localBtn, *outOfStateBtn , *nationalBtn;

@property (nonatomic,strong) UILabel      *questionLabl;

@property (nonatomic,strong) UIImage      *radioImg;

@property (nonatomic,strong) UIButton     *optionBtn1, *optionBtn2, *optionBtn3 ,*optionBtn4;

@property (nonatomic,strong) UIButton     *submitBtn;

@property (nonatomic,strong) NSString     *emailIdStr;

@property (nonatomic,strong) NSString     *genderStr;

@property (nonatomic,strong) NSString     *typeStr;

@property (nonatomic,strong) NSString     *locationStr;

@property (nonatomic,strong) NSString     *optionStr;

@property (nonatomic,strong) UIView       *emailLineVW;

@property (nonatomic,strong) UIView       *phoneLineVW;
@property (nonatomic,strong) UIButton       *backBtn;

-(NSString*)formatNumber:(NSString*)mobileNumber;

-(int)getLength:(NSString*)mobileNumber;


@end
