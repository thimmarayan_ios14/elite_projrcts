//
//  SelectionGameVC.m
//  AppsWorld
//
//  Created by Mac User on 28/04/15.
//
//

#import "SelectionGameVC.h"
#import "ScratcherVC.h"
#import "FXReachability.h"
#import "AFNetworking.h"
#import "MDScratchImageView.h"
#import "SpotTheDifferVC.h"
#import "Define.h"
#import "ScoreVC.h"
#import "MBProgressHUD.h"
#import <unistd.h>

#define NUMBER_LABEL_WIDTH 145
#define NUMBER_LABEL_HEIGHT 140

@interface SelectionGameVC ()<UIGestureRecognizerDelegate,MBProgressHUDDelegate>
{
    NSString *scoreCardStr;
    UILabel *chooseGameLbl,*winPrizeLbl,*sanFranMatchLbl,*spotDiffLbl,*scratchWinLbl,*matchLbl,*diffLbl,*WinLbl;
    UIView *scratchVw,*spotDiffVw,*matchVw;
    UIImageView *tickMarkImg1,*tickMarkImg2,*tickMarkImg3;
    MBProgressHUD *HUD;
    UIButton *statusBtn;
    int count;
    NSTimer *dismissTimer;
    UITapGestureRecognizer *matchVwTap,*scratchTap,*spotDiffTap;
}
@end

@implementation SelectionGameVC

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // [self matcherGameService];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    chooseGameLbl=[[UILabel alloc]init];
    winPrizeLbl=[[UILabel alloc]init];
    matchVw=[[UIView alloc]init];
    spotDiffVw=[[UIView alloc]init];
    scratchVw=[[UIView alloc]init];
    spotDiffVw=[[UIView alloc]init];
    matchVw=[[UIView alloc]init];
    sanFranMatchLbl=[[UILabel alloc]init];
    spotDiffLbl=[[UILabel alloc]init];
    scratchWinLbl=[[UILabel alloc]init];
    matchLbl=[[UILabel alloc]init];
    diffLbl=[[UILabel alloc]init];
    WinLbl=[[UILabel alloc]init];
    tickMarkImg1=[[UIImageView alloc]init];
    tickMarkImg2=[[UIImageView alloc]init];
    tickMarkImg3=[[UIImageView alloc]init];
    sound = [[AudioPlayer alloc]init];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    chooseGameLbl.font = [UIFont fontWithName:HELVETICA_THIN size:65];
    chooseGameLbl.textColor = [UIColor blackColor];
    chooseGameLbl.text=@"CHOOSE WHAT GAME TO PLAY";
    chooseGameLbl.backgroundColor=[UIColor clearColor];
    [self.view addSubview:chooseGameLbl];
    
    winPrizeLbl.font = [UIFont fontWithName:HELVETICA_THIN size:115];
    winPrizeLbl.textColor = [UIColor blackColor];
    winPrizeLbl.text=@"AND WIN A PRIZE!";
    winPrizeLbl.backgroundColor=[UIColor clearColor];
    [self.view addSubview:winPrizeLbl];
    
    matchVw.backgroundColor=RGBCOLOR(0,32,91);
    matchVw.layer.cornerRadius=20.0f;
    matchVw.userInteractionEnabled=YES;
    matchVw.tag=1;
    spotDiffTap = [[UITapGestureRecognizer alloc]
                   initWithTarget:self
                   action:@selector(memoryBtnTapped:)];
    spotDiffTap.delegate=self;
    //    spotDiffTap.view.tag=1;
    spotDiffTap.numberOfTapsRequired=1;
    [matchVw addGestureRecognizer:spotDiffTap];
    [self.view addSubview:matchVw];
    
    spotDiffVw.backgroundColor=RGBCOLOR(86,161,213);
    spotDiffVw.layer.cornerRadius=20.0f;
    spotDiffVw.userInteractionEnabled=YES;
    spotDiffVw.tag=2;
    spotDiffTap = [[UITapGestureRecognizer alloc]
                   initWithTarget:self
                   action:@selector(memoryBtnTapped:)];
    spotDiffTap.delegate=self;
    spotDiffTap.view.tag=2;
    spotDiffTap.numberOfTapsRequired=1;
    [spotDiffVw addGestureRecognizer:spotDiffTap];
    
    [self.view addSubview:spotDiffVw];
    
    scratchVw.backgroundColor=RGBCOLOR(241,137,3);
    scratchVw.layer.cornerRadius=20.0f;
    scratchVw.userInteractionEnabled=YES;
    scratchVw.tag=3;
    spotDiffTap = [[UITapGestureRecognizer alloc]
                   initWithTarget:self
                   action:@selector(memoryBtnTapped:)];
    spotDiffTap.delegate=self;
    spotDiffTap.numberOfTapsRequired=1;
    spotDiffTap.view.tag=3;
    [scratchVw addGestureRecognizer:spotDiffTap];
    [self.view addSubview:scratchVw];
    
    sanFranMatchLbl.font = [UIFont fontWithName:HELVETICA_THIN size:52];
    sanFranMatchLbl.textColor = [UIColor whiteColor];
    sanFranMatchLbl.text=@"DELAWARE";
    //sanFranMatchLbl.textAlignment=NSTextAlignmentCenter;
    sanFranMatchLbl.backgroundColor=[UIColor clearColor];
    [matchVw addSubview:sanFranMatchLbl];
    
    matchLbl.font = [UIFont fontWithName:HELVETICA_THIN size:70];
    matchLbl.textColor = [UIColor whiteColor];
    matchLbl.text=@"MATCH";
    matchLbl.backgroundColor=[UIColor clearColor];
    [matchVw addSubview:matchLbl];
    
    spotDiffLbl.font = [UIFont fontWithName:HELVETICA_THIN size:54];
    spotDiffLbl.textColor = [UIColor whiteColor];
    spotDiffLbl.text=@"SPOT THE";
    spotDiffLbl.backgroundColor=[UIColor clearColor];
    [spotDiffVw addSubview:spotDiffLbl];
    
    diffLbl.font = [UIFont fontWithName:HELVETICA_THIN size:43];
    diffLbl.textColor = [UIColor whiteColor];
    diffLbl.text=@" DIFFERENCE";
    diffLbl.backgroundColor=[UIColor clearColor];
    [spotDiffVw addSubview:diffLbl];
    
    scratchWinLbl.font = [UIFont fontWithName:HELVETICA_THIN size:56];
    scratchWinLbl.textColor = [UIColor whiteColor];
    scratchWinLbl.text=@"SCRATCH";
    scratchWinLbl.backgroundColor=[UIColor clearColor];
    [scratchVw addSubview:scratchWinLbl];
    
    WinLbl.font = [UIFont fontWithName:HELVETICA_THIN size:93];
    WinLbl.textColor = [UIColor whiteColor];
    WinLbl.text=@"& WIN";
    WinLbl.backgroundColor=[UIColor clearColor];
    [scratchVw addSubview:WinLbl];
    
    tickMarkImg1.image=[UIImage imageNamed:@"selected"];
    tickMarkImg2.image=[UIImage imageNamed:@"selected"];
    tickMarkImg3.image=[UIImage imageNamed:@"selected"];
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:MEMORYGAMESTATUS] isEqualToString:@"1"])
    {
//        matchVw.backgroundColor=RGBCOLOR(225,225,225);
//        //matchVw.backgroundColor=RGBCOLOR(0,32,91);
//        matchVw.userInteractionEnabled=NO;
//        tickMarkImg1.frame=CGRectMake(80,80,[UIImage imageNamed:@"selected"].size.width,[UIImage imageNamed:@"selected"].size.height);
//        [matchVw addSubview:tickMarkImg1];
        firstTimeUser = @"new";
    }
    else{
        firstTimeUser = @"no";
    }
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:SCRATCHGAMESSTATUS] isEqualToString:@"1"])
    {
        scratchVw.backgroundColor=RGBCOLOR(225,225,225);
         //scratchVw.backgroundColor=RGBCOLOR(241,137,3);
        
        scratchVw.userInteractionEnabled=NO;
        tickMarkImg2.frame=CGRectMake(80,80,[UIImage imageNamed:@"selected"].size.width,[UIImage imageNamed:@"selected"].size.height);
        [scratchVw addSubview:tickMarkImg2];
    }
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:SPOTDIFFGAMESTATUS] isEqualToString:@"1"])
    {
//        spotDiffVw.backgroundColor=RGBCOLOR(225,225,225);
//        //spotDiffVw.backgroundColor=RGBCOLOR(86,161,213);
//        
//        spotDiffVw.userInteractionEnabled=NO;
//        tickMarkImg3.frame=CGRectMake(80,80,[UIImage imageNamed:@"selected"].size.width,[UIImage imageNamed:@"selected"].size.height);
//        [spotDiffVw addSubview:tickMarkImg3];
        firstTimeUser = @"new";
    }
    else{
        firstTimeUser = @"no";
    }
    
    count=0;
    
    statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusBtn.frame =  CGRectMake(0,0,CGRectGetWidth(self.view.frame),50);
    [statusBtn setBackgroundColor:[UIColor clearColor]];
    [statusBtn addTarget:self action:@selector(statusBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:statusBtn];
    
    // FRAMES
    
    chooseGameLbl.frame=CGRectMake(50,135,CGRectGetWidth(self.view.frame)-100,52);
    winPrizeLbl.frame=CGRectMake(CGRectGetMinX(chooseGameLbl.frame),CGRectGetMaxY(chooseGameLbl.frame)+20,CGRectGetWidth(chooseGameLbl.frame),90);
    [matchVw setFrame:CGRectMake(CGRectGetMinX(winPrizeLbl.frame)+5, CGRectGetMaxY(winPrizeLbl.frame)+50, 270, 270)];
    spotDiffVw.frame=CGRectMake(CGRectGetMaxX(matchVw.frame)+50,CGRectGetMinY(matchVw.frame),CGRectGetWidth(matchVw.frame),CGRectGetHeight(matchVw.frame));;
    scratchVw.frame=CGRectMake(CGRectGetMaxX(spotDiffVw.frame)+46,CGRectGetMinY(matchVw.frame),CGRectGetWidth(matchVw.frame),CGRectGetHeight(matchVw.frame));;
    sanFranMatchLbl.frame=CGRectMake(8,80,CGRectGetWidth(matchVw.frame)-10,45);
    matchLbl.frame=CGRectMake(CGRectGetMinX(sanFranMatchLbl.frame),CGRectGetMaxY(sanFranMatchLbl.frame)-18,CGRectGetWidth(sanFranMatchLbl.frame),100);
    spotDiffLbl.frame=CGRectMake(12,CGRectGetMinY(sanFranMatchLbl.frame)+8,CGRectGetWidth(matchVw.frame)-25,50);
    diffLbl.frame=CGRectMake(CGRectGetMinX(spotDiffLbl.frame)-10,CGRectGetMaxY(spotDiffLbl.frame)-8,CGRectGetWidth(matchLbl.frame)+7,50);
    scratchWinLbl.frame=CGRectMake(12,CGRectGetMinY(sanFranMatchLbl.frame)-5,CGRectGetWidth(matchVw.frame)-25,45);
    WinLbl.frame=CGRectMake(CGRectGetMinX(scratchWinLbl.frame),CGRectGetMaxY(scratchWinLbl.frame)-10,CGRectGetWidth(scratchWinLbl.frame),100);
    
    
     //[[NSNotificationCenter defaultCenter] postNotificationName:@"Notification_2" object:self];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [_matchTimer invalidate];       // Stop Timer
    _matchTimer = nil;
}

-(void)scratcherBtnTapped
{
    spotDiffVw.userInteractionEnabled=NO;
    matchVw.userInteractionEnabled=NO;
    
    UIStoryboard *board;
    if (!self.storyboard) {
        board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    } else {
        board = self.storyboard;
    }
    ScratcherVC *scratchVC = [board instantiateViewControllerWithIdentifier:@"Scratch"];
    [self.navigationController pushViewController:scratchVC animated:YES];
    
    /* _scratcherVW = [[ScratcherVW alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
     _scratcherVW.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.7];
     [self.view addSubview:_scratcherVW];*/
}

-(void)memoryBtnTapped:(UITapGestureRecognizer *)gesture
{
    if (gesture.view.tag==1)
    {
        spotDiffVw.userInteractionEnabled=NO;
        scratchVw.userInteractionEnabled=NO;
        
        if (matchVw.userInteractionEnabled == YES)
        {
            [self performSelector:@selector(callshuffleTilesmethod) withObject:nil afterDelay:0.6];
            
            /* GAME VIEW */
            
            gameVw = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 768)];
            gameVw.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:gameVw];
            
            count=0;
            
            statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            statusBtn.frame =  CGRectMake(0,0,CGRectGetWidth(self.view.frame),50);
            [statusBtn setBackgroundColor:[UIColor clearColor]];
            [statusBtn addTarget:self action:@selector(statusBtnTap:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:statusBtn];
            
            /* SAN FRAN LABLE */
            
            [self.sanfransLabl setFrame:CGRectMake(0,15, gameVw.frame.size.width, 95)];
            [gameVw addSubview:_sanfransLabl];
            
            /*  TILES VIEW */
            
            _tdTileVW = [[TDTileView alloc]initWithFrame:CGRectMake(25,CGRectGetMaxY([_sanfransLabl frame])+15,1024-2*(25),768-2*(145))];
            _tdTileVW.backgroundColor = [UIColor clearColor];
            _tdTileVW.numberOfColumns = 6;
            _tdTileVW.numberOfRows = 3;
            _tdTileVW.spaceBetweenTiles = 20;
            _tdTileVW.tileWidth = 145; // (_tdTileVW.frame.size.width / 6) - (3 * _tdTileVW.spaceBetweenTiles);
            _tdTileVW.tileHeight =  145;//(_tdTileVW.frame.size.height / 6) - (3 * _tdTileVW.spaceBetweenTiles);
            _tdTileVW.delegate = self;
            [_tdTileVW drawTDTileView];
            [gameVw addSubview:_tdTileVW];
            
            /* TIME LABEL */
            
            [self.timeLabl setFrame:CGRectMake(25, CGRectGetMaxY([_tdTileVW frame])+15, NUMBER_LABEL_WIDTH, NUMBER_LABEL_HEIGHT)];
            [gameVw addSubview:_timeLabl];
            
            
            /* TIME HEADER LABEL */
            
            [self.timeHeaderLabl setFrame:CGRectMake(CGRectGetMaxX([_timeLabl frame])+10, CGRectGetMinY([_timeLabl frame]), 310, NUMBER_LABEL_HEIGHT)];
            [gameVw addSubview:_timeHeaderLabl];
            
            /* SEPARATOR VIEW */
            
            [self.separatorVW setFrame:CGRectMake(CGRectGetMaxX([_timeHeaderLabl frame])+15, CGRectGetMinY([_timeLabl frame])+12, 2, NUMBER_LABEL_HEIGHT-20)];
            [gameVw addSubview:_separatorVW];
            
            /* MATCHES HEADER LABEL */
            
            [self.correctHeaderLabl setFrame:CGRectMake(CGRectGetMaxX([_separatorVW frame])+15, CGRectGetMinY([_timeLabl frame]), 315, 70)];
            [gameVw addSubview:_correctHeaderLabl];
            
            /* MATCHES HEADER LABEL */
            
            [self.matchesHeaderLabl setFrame:CGRectMake(CGRectGetMaxX([_separatorVW frame])+15, CGRectGetMaxY([_correctHeaderLabl frame]), 315, 70)];
            [gameVw addSubview:_matchesHeaderLabl];
            
            /* MATCHES LABEL */
            
            [self.matchesLabl setFrame:CGRectMake(CGRectGetMaxX([_matchesHeaderLabl frame])+15, CGRectGetMinY([_timeLabl frame]), NUMBER_LABEL_WIDTH, NUMBER_LABEL_HEIGHT)];
            [gameVw addSubview:_matchesLabl];
            
            [self timerStart];
        }
    }
    else if (gesture.view.tag==2)
    {
        matchVw.userInteractionEnabled=NO;
        scratchVw.userInteractionEnabled=NO;
        
        if (spotDiffVw.userInteractionEnabled == YES)
        {
            SpotTheDifferVC *spotDifferVC = [[SpotTheDifferVC alloc]init];
            spotDifferVC.isNewUserString = firstTimeUser;
            [self.navigationController pushViewController:spotDifferVC animated:YES];
        }
    }
    else if (gesture.view.tag==3)
    {
        matchVw.userInteractionEnabled=NO;
        spotDiffVw.userInteractionEnabled=NO;
        
        if (scratchVw.userInteractionEnabled == YES)
        {
            UIStoryboard *board;
            if (!self.storyboard) {
                board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            } else {
                board = self.storyboard;
            }
            ScratcherVC *scratchVC = [board instantiateViewControllerWithIdentifier:@"Scratch"];
            [self.navigationController pushViewController:scratchVC animated:YES];
        }
    }
    
}
-(UILabel *)sanfransLabl
{
    if (!_sanfransLabl) {
        
        _sanfransLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _sanfransLabl.text = @"DELAWARE MATCH";
        _sanfransLabl.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:120];
        _sanfransLabl.adjustsFontSizeToFitWidth = YES;
        _sanfransLabl.textAlignment = NSTextAlignmentCenter;
        _sanfransLabl.backgroundColor = [UIColor clearColor];
        _sanfransLabl.textColor = [UIColor orangeColor];
    }
    return _sanfransLabl;
}
-(UILabel *)timeLabl
{
    if (!_timeLabl) {
        
        _timeLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _timeLabl.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:100];
        _timeLabl.layer.cornerRadius = 10;
        _timeLabl.clipsToBounds = YES;
        _timeLabl.backgroundColor = RGBA(0, 32, 91, 1);
        _timeLabl.textAlignment = NSTextAlignmentCenter;
        _timeLabl.textColor = [UIColor whiteColor];
    }
    return _timeLabl;
}

-(UILabel *)timeHeaderLabl
{
    if (!_timeHeaderLabl) {
        
        _timeHeaderLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _timeHeaderLabl.text = @"TIME";
        _timeHeaderLabl.textColor = [UIColor blackColor];
        _timeHeaderLabl.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:155];
        _timeHeaderLabl.textAlignment = NSTextAlignmentCenter;
        _timeHeaderLabl.backgroundColor = [UIColor clearColor];
    }
    return _timeHeaderLabl;
}
-(UILabel *)correctHeaderLabl
{
    if (!_correctHeaderLabl) {
        
        _correctHeaderLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _correctHeaderLabl.text = @"CORRECT";
        _correctHeaderLabl.textColor = [UIColor blackColor];
        _correctHeaderLabl.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:70];
        _correctHeaderLabl.textAlignment = NSTextAlignmentCenter;
        _correctHeaderLabl.backgroundColor = [UIColor clearColor];
    }
    return _correctHeaderLabl;
}

-(UILabel *)matchesHeaderLabl
{
    if (!_matchesHeaderLabl) {
        
        _matchesHeaderLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _matchesHeaderLabl.text = @"MATCHES";
        _matchesHeaderLabl.textColor = [UIColor blackColor];
        _matchesHeaderLabl.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:70];
        _matchesHeaderLabl.textAlignment = NSTextAlignmentCenter;
        _matchesHeaderLabl.backgroundColor = [UIColor clearColor];
    }
    return _matchesHeaderLabl;
}

-(UILabel *)matchesLabl
{
    if (!_matchesLabl) {
        
        _matchesLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _matchesLabl.text = @"0";
        _matchesLabl.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:100];
        _matchesLabl.layer.cornerRadius= 10;
        _matchesLabl.clipsToBounds = YES;
        _matchesLabl.textAlignment = NSTextAlignmentCenter;
        _matchesLabl.backgroundColor = RGBA(86, 161, 213, 1);
        _matchesLabl.textColor = [UIColor whiteColor];
    }
    return _matchesLabl;
}
-(UIView *)separatorVW
{
    if (!_separatorVW) {
        
        _separatorVW = [[UIView alloc]initWithFrame:CGRectZero];
        _separatorVW.backgroundColor = [UIColor orangeColor];
    }
    return _separatorVW;
}
-(void)matchingBtnTapped
{
    scratchVw.userInteractionEnabled=NO;
    matchVw.userInteractionEnabled=NO;
    
    SpotTheDifferVC *spotDifferVC = [[SpotTheDifferVC alloc]init];
    [self.navigationController pushViewController:spotDifferVC animated:YES];
}
#pragma mark - TILE VIEW DELEGATE METHODS

-(void)tileTouched:(NSUInteger)totoalTouches
{
    
    //One point will be decrement when user plays one move
    
}
-(void)numberOfTilesPaired:(NSUInteger)score
{
    NSLog(@"%lu",(unsigned long)score);
    _matchesLabl.text =[NSString stringWithFormat:@"%lu",(unsigned long)score];
    
    if ([_matchesLabl.text isEqualToString:@"9"])
    {
//        [sound playSoundWithFile:@"crowdapplause"];
        
        if ([_matchTimer isValid]) {
            
            [_matchTimer invalidate];       // Stop Timer
            _matchTimer = nil;
            [self matcherGameService];
        }
    }
    
}

-(void)callshuffleTilesmethod
{
    [_tdTileVW shuffleTiles];
}

#pragma mark - Timer

- (void)timerStart{
    
    totalSeconds = 91;
    
    if ([_matchTimer isValid]) {
        
        [_matchTimer invalidate];
        _matchTimer = nil;
    }
    _matchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    [_matchTimer fire];
    
}

- (void)updateTimer
{
    totalSeconds--;
    if (totalSeconds>=0)
    {
        _timeLabl.text = [NSString stringWithFormat:@"%.2d",totalSeconds];
    }
    else
    {
        [_matchTimer invalidate];
        
        [self matcherGameService];
        
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"TimeUp!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //        [alert show];
    }
}

#pragma mark - AlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [gameVw removeFromSuperview];
        
    }
}

#pragma mark - Matching Game Method

-(void)matcherGameService
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if([FXReachability isReachable]){
        
        int scoreCard=[_matchesLabl.text intValue]*100 + [_timeLabl.text intValue]*10;
        scoreCardStr=[NSString stringWithFormat:@"%d",scoreCard];
        
        int timeTaken=90-[_timeLabl.text intValue];
        NSString *totalTimeTaken=[NSString stringWithFormat:@"%.2d",timeTaken];
        
        NSDictionary *inputDict = @{@"appId":@"100ME",@"time":_timeLabl.text,@"score":scoreCardStr,@"matches":_matchesLabl.text,@"userId":[[NSUserDefaults standardUserDefaults]valueForKey:USERID],@"device":@"web",@"flag":firstTimeUser};
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *urlString=[NSString stringWithFormat:@"%@",GAME_SERVICE];
        [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFormData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"input"];
        }
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  NSDictionary *resultDict = (NSDictionary*)responseObject;
                  NSLog(@"Matcher Game : %@",resultDict);
                  
                  NSString *score;
                  if ([scoreCardStr isEqualToString:@"0"])
                  {
                      score=@"00";
                  }
                  else
                      
                  {
                      score=scoreCardStr;
                  }
                  
                  if ([[[resultDict valueForKey:@"code"] stringValue] isEqualToString:@"1000"])
                  {
                      ScoreVC *scoreVC = [[ScoreVC alloc]init];
                      scoreVC.totalSecondsStr=totalTimeTaken;
                      scoreVC.totalScoreStr=score;
                      scoreVC.gameTypeStr=@"MemoryGame";
                      scoreVC.uniqueCodeStr=[[resultDict valueForKey:@"u_code"]stringValue];
                      [self.navigationController pushViewController:scoreVC animated:YES];
                  }
                  else if ([[[resultDict valueForKey:@"code"] stringValue] isEqualToString:@"2000"])
                  {
                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter the Valid Details" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                      //                      [alertView show];
                  }
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"There is a problem connecting to the server. Please check your internet connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
                  
              }];
    }
    else{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Seems your internet is offline" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [offlineAlert show];
    }
    
}

#pragma mark-
#pragma mark - Hide Loading View

- (void)myTask
{
    // Do something usefull in here instead of sleeping ...
}

//-(void)showLoadingView
//{
//    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(360,320,250,200)];
//    [self.navigationController.view addSubview:HUD];
//    HUD.color = [UIColor blackColor];
//    HUD.delegate = self;
//    HUD.labelText = @"Loading...";
//    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
//    [self.view addSubview:HUD];
//}

-(void)hideLoadingView
{
    [HUD hide:YES];
}

#pragma mark Cube Animation

-(void)cubeAnimation{
    
    CATransition *transition = [CATransition animation];
    transition.delegate = self;
    transition.duration = 0.8;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = @"alignedCube";
    transition.subtype = kCATransitionFromRight;
    
    [_tdTileVW.layer addAnimation:transition forKey:nil];
    _tdTileVW.hidden = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return  YES ;
}

#pragma mark-
#pragma mark - Status button Action

-(void)statusBtnTap:(id)sender
{
    count++;
    
    if (count==3)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    dismissTimer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                   target:self
                                                 selector:@selector(reduceCount)
                                                 userInfo:nil
                                                  repeats:NO];
}

-(void)reduceCount
{
    count=0;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
