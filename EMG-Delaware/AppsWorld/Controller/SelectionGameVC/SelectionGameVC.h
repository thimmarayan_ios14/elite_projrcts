//
//  SelectionGameVC.h
//  AppsWorld
//
//  Created by Mac User on 28/04/15.
//
//

#import <UIKit/UIKit.h>
#import "TDTileView.h"

@interface SelectionGameVC : UIViewController<TDTileViewDelegate,UIAlertViewDelegate>
{
    UIButton    *_scratcherGameBtn, *_memoryGameBtn, *_matchingGameBtn;
    UIView      * gameVw;
    TDTileView  *_tdTileVW;
    int         totalSeconds;
    AudioPlayer * sound;
    NSString *firstTimeUser;
}

@property(nonatomic,strong) UILabel     *sanfransLabl;

@property(nonatomic,strong) UILabel     *timeLabl, *matchesLabl;

@property(nonatomic,strong) UILabel     *timeHeaderLabl, *correctHeaderLabl,*matchesHeaderLabl;

@property(nonatomic,strong) UIView      *separatorVW;

@property(nonatomic,strong) NSTimer     *matchTimer;

@end
