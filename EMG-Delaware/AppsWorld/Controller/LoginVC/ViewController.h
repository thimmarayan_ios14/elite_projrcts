//
//  ViewController.h
//  AppsWorld
//
//  Created by Mac User on 28/04/15.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic,strong) UILabel         *areYouLabl;

@property (nonatomic,strong) UIButton        *submitBtn;

@property (nonatomic,strong) UILabel         *welcomeLabl;

@property (nonatomic,strong) UIView          *separatorVW ;

@property (nonatomic,strong) UILabel         *pleaseEnterLabl;

@property (nonatomic,strong) UILabel         *followingLabl;

@property (nonatomic,strong) UIButton        *freshUserBtn , *existingUserBtn;

@property (nonatomic,strong) CAGradientLayer *freshGradient, *existingGradient;

@end
