//
//  ViewController.m
//  AppsWorld
//
//  Created by Mac User on 28/04/15.
//
//



#import "ViewController.h"
#import "ExistingUserVC.h"
#import "DataCaptureVC.h"
#import "Define.h"

@interface ViewController ()
{
    BOOL newUserTapped , existingUserTapped;
    UIButton *statusBtn;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:RGBA(86, 161, 213, 1)];
    
    [[self navigationController]setNavigationBarHidden:YES animated:YES];
    
    // Do any additional setup after loading the view.
    
//    UILocalNotification* local = [[UILocalNotification alloc]init];
//    if (local)
//    {
//        local.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
//        local.alertBody = @"Hey this is my first local notification!!!";
//        local.timeZone = [NSTimeZone defaultTimeZone];
//        [[UIApplication sharedApplication] scheduleLocalNotification:local];
//        
//    }
    
    
}
-(void)setFrames
{
    [self.welcomeLabl setFrame:CGRectMake(10,38, 500, 80)];
    
    [self.separatorVW setFrame:CGRectMake(CGRectGetMaxX([_welcomeLabl frame])+15, CGRectGetMinY([_welcomeLabl frame])+2, 1, CGRectGetHeight([_welcomeLabl frame])-5)];
    
    [self.pleaseEnterLabl setFrame:CGRectMake(CGRectGetMaxX([_separatorVW frame])+15, CGRectGetMinY([_welcomeLabl frame]), CGRectGetWidth([_welcomeLabl frame])-50, CGRectGetHeight([_welcomeLabl frame])/2.9)];
    
    [self.followingLabl setFrame:CGRectMake(CGRectGetMinX([_pleaseEnterLabl frame]), CGRectGetMaxY([_pleaseEnterLabl frame])+8, CGRectGetWidth([_pleaseEnterLabl frame]), CGRectGetHeight([_pleaseEnterLabl frame])+20)];
    
    [self.areYouLabl setFrame:CGRectMake(CGRectGetMinX([_welcomeLabl frame])+10, CGRectGetMaxY([_welcomeLabl frame])+240, 175, 60)];
    
    if (_freshUserBtn) {

        [_freshUserBtn removeFromSuperview];
    }
    _freshUserBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _freshUserBtn.frame =  CGRectMake(CGRectGetMaxX([_areYouLabl frame])+10,CGRectGetMinY([_areYouLabl frame])+7,180, 49);
    _freshGradient  = [CAGradientLayer layer];
    _freshGradient.frame = self.freshUserBtn.layer.bounds;
    _freshGradient.colors = [NSArray arrayWithObjects:
                             (id)RGBA(209, 210, 212, 1).CGColor,
                             (id)RGBA(250, 250, 250, 1).CGColor,nil];
    _freshGradient.cornerRadius = self.freshUserBtn.layer.cornerRadius;
    [self.freshUserBtn.layer addSublayer:_freshGradient];
    
    [_freshUserBtn setTitle:@"NEW USER" forState:UIControlStateNormal];
    _freshUserBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    _freshUserBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:33];
    [_freshUserBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _freshUserBtn.exclusiveTouch = YES;
    _freshUserBtn.layer.cornerRadius = 6;
    _freshUserBtn.clipsToBounds = YES;
    [_freshUserBtn addTarget:self action:@selector(newUserBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    if (_existingUserBtn) {
        [_existingUserBtn removeFromSuperview];
    }
    _existingUserBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _existingUserBtn.frame =  CGRectMake(CGRectGetMaxX([_freshUserBtn frame])+20,CGRectGetMinY([_freshUserBtn frame]),245, 49);
    _existingGradient  = [CAGradientLayer layer];
    _existingGradient.frame = self.existingUserBtn.layer.bounds;
    _existingGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    _existingGradient.cornerRadius = self.existingUserBtn.layer.cornerRadius;
    [self.existingUserBtn.layer addSublayer:_existingGradient];
    
    [_existingUserBtn setTitle:@"EXISTING USER" forState:UIControlStateNormal];
    _existingUserBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:33];
    [_existingUserBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _existingUserBtn.exclusiveTouch = YES;
    _existingUserBtn.layer.cornerRadius = 6;
    _existingUserBtn.clipsToBounds = YES;
    [_existingUserBtn addTarget:self action:@selector(existingUserBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
  /*  _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame =  CGRectMake(CGRectGetWidth(self.view.frame)-150,690, 130, 49);
    [_submitBtn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_ULTRALIGHT size:35];
    _submitBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_submitBtn setBackgroundColor:RGBA(241, 137, 3, 1)];
    _submitBtn.layer.cornerRadius = 6;
    _submitBtn.clipsToBounds = YES;
    _submitBtn.exclusiveTouch = YES;
    _submitBtn.userInteractionEnabled=YES;
    [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitBtnTapped) forControlEvents:UIControlEventTouchUpInside];*/

}
-(void)viewWillAppear:(BOOL)animated
{
    [self setFrames];
    
    [self.view addSubview:_welcomeLabl];
    
    [self.view addSubview:_separatorVW];
    
    [self.view addSubview:_pleaseEnterLabl];
    
    [self.view addSubview:_followingLabl];
    
    [self.view addSubview:_areYouLabl];
    
    [self.view addSubview:_freshUserBtn];
    
    [self.view addSubview:_existingUserBtn];
    
    [self.view addSubview:_submitBtn];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _freshUserBtn.tag=0;
    _existingUserBtn.tag=0;
}

-(BOOL)prefersStatusBarHidden
{
    return  YES ;
}


-(void)newUserBtnTapped
{
    newUserTapped = YES;
    _freshUserBtn.tag=1;

    _freshGradient.colors = nil;
    
    _freshUserBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_freshUserBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _existingGradient.colors = [NSArray arrayWithObjects:
                                (id)RGBA(209, 210, 212, 1).CGColor,
                                (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_existingUserBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    DataCaptureVC *dataCaptureVC = [[DataCaptureVC alloc]init];
    [self.navigationController pushViewController:dataCaptureVC animated:YES];

}
-(void)existingUserBtnTapped
{
    newUserTapped = NO;
    _existingUserBtn.tag=2;

    _existingGradient.colors = nil;
    
    _existingUserBtn.backgroundColor = RGBA(0, 32, 91, 1);
    [_existingUserBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _freshGradient.colors = [NSArray arrayWithObjects:
                             (id)RGBA(209, 210, 212, 1).CGColor,
                             (id)RGBA(250, 250, 250, 1).CGColor,nil];
    [_freshUserBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    ExistingUserVC *existingUserVC = [[ExistingUserVC alloc]init];
    [self.navigationController pushViewController:existingUserVC animated:YES];
    
}
/*-(void)submitBtnTapped
{
    _submitBtn.userInteractionEnabled=NO;
    
    if (_freshUserBtn.tag==1 || _existingUserBtn.tag==2)
    {
    if (newUserTapped==YES)
    {
        DataCaptureVC *dataCaptureVC = [[DataCaptureVC alloc]init];
        [self.navigationController pushViewController:dataCaptureVC animated:NO];
    }
    else if(newUserTapped==NO)
    {
        ExistingUserVC *existingUserVC = [[ExistingUserVC alloc]init];
        [self.navigationController pushViewController:existingUserVC animated:NO];
    }
    }
    else
    {
        _submitBtn.userInteractionEnabled=YES;
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please select any one of the user" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}*/
#pragma mark - Custom Setter/Getter Method

-(UILabel *)welcomeLabl
{
    if (!_welcomeLabl) {
        
        _welcomeLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _welcomeLabl.text = @"WELCOME";
        _welcomeLabl.font = [UIFont fontWithName:HELVETICA_THIN size:100];
        _welcomeLabl.textAlignment = NSTextAlignmentCenter;
        _welcomeLabl.backgroundColor = [UIColor clearColor];
        _welcomeLabl.textColor = [UIColor whiteColor];
    }
    return _welcomeLabl;
}

-(UIView *)separatorVW
{
    if (!_separatorVW) {
        
        _separatorVW = [[UIView alloc]initWithFrame:CGRectZero];
        _separatorVW.backgroundColor = RGBA(239, 137, 34, 1);
        
    }
    return _separatorVW;
}

-(UILabel *)pleaseEnterLabl
{
    if (!_pleaseEnterLabl) {
        
        _pleaseEnterLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _pleaseEnterLabl.text = @"PLEASE ENTER THE";
        _pleaseEnterLabl.font = [UIFont fontWithName:HELVETICA_THIN size:34];
        _pleaseEnterLabl.numberOfLines = 2;
        _pleaseEnterLabl.textAlignment = NSTextAlignmentLeft;
        _pleaseEnterLabl.backgroundColor = [UIColor clearColor];
        _pleaseEnterLabl.textColor = [UIColor whiteColor];
    }
    return _pleaseEnterLabl;
}
-(UILabel *)followingLabl
{
    if (!_followingLabl) {
        
        _followingLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _followingLabl.text = @"FOLLOWING";
        _followingLabl.font = [UIFont fontWithName:HELVETICA_THIN size:56];
        _followingLabl.numberOfLines = 2;
        _followingLabl.textAlignment = NSTextAlignmentLeft;
        _followingLabl.backgroundColor = [UIColor clearColor];
        _followingLabl.textColor = [UIColor whiteColor];
    }
    return _followingLabl;
}

-(UILabel *)areYouLabl
{
    if (!_areYouLabl) {
        
        _areYouLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _areYouLabl.text = @"ARE YOU A :";
        _areYouLabl.font = [UIFont fontWithName:HELVETICA_THIN size:32];
        _areYouLabl.textAlignment = NSTextAlignmentLeft;
        _areYouLabl.backgroundColor = [UIColor clearColor];
        _areYouLabl.textColor = [UIColor whiteColor];
    }
    return _areYouLabl;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

