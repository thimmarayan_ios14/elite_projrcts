//
//  ScoreVC.m
//  AppsWorld
//
//  Created by Apple on 01/05/15.
//
//

#import "ScoreVC.h"
#import "Define.h"
#import "ViewController.h"

@interface ScoreVC ()<UIGestureRecognizerDelegate>
{
    UILabel *congratulationLbl,*timeTakenLbl,*totalTimeLbl,*scoreLbl,*totalScoreLbl,*prizeLbl,*totalPrizeLbl,*codeLbl,*informLbl,*mandatoryLbl,*secsLbl, *secondaryLabel;
    UIButton *finishBtn;
    UIView *totalTimeVw,*totalScoreVw,*prizeCodeVw;
    NSTimer *dismissTimer;
    
        UIButton *statusBtn;
    int count;
    NSTimer *timer;
}
@end

@implementation ScoreVC
@synthesize totalScoreStr,totalSecondsStr,uniqueCodeStr,gameTypeStr,scratcher_Images;

- (void)viewDidLoad
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden=YES;

    congratulationLbl=[[UILabel alloc]init];
    totalTimeVw=[[UIView alloc]init];
    totalScoreVw=[[UIView alloc]init];
    prizeCodeVw=[[UIView alloc]init];
    
    timeTakenLbl=[[UILabel alloc]init];
    totalTimeLbl=[[UILabel alloc]init];
    scoreLbl=[[UILabel alloc]init];
    totalScoreLbl=[[UILabel alloc]init];
    prizeLbl=[[UILabel alloc]init];
    codeLbl=[[UILabel alloc]init];
    totalPrizeLbl=[[UILabel alloc]init];
    informLbl=[[UILabel alloc]init];
    mandatoryLbl=[[UILabel alloc]init];
    secsLbl=[[UILabel alloc]init];
    secondaryLabel = [[UILabel alloc]init];
    
    scratcher_Images=[UIImageView new];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self ScoreUI];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [dismissTimer invalidate];
    dismissTimer = nil;
}

-(void)ScoreUI
{
    count=0;
    
    statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusBtn.frame =  CGRectMake(0,0,CGRectGetWidth(self.view.frame),50);
    [statusBtn setBackgroundColor:[UIColor clearColor]];
    [statusBtn addTarget:self action:@selector(statusBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:statusBtn];

   
    congratulationLbl.backgroundColor=[UIColor clearColor];
    [congratulationLbl setTextAlignment:NSTextAlignmentCenter];
//    congratulationLbl.numberOfLines = 2;
    [self.view addSubview:congratulationLbl];
    
    //secondaryLabel.textColor = [UIColor blackColor];
    secondaryLabel.backgroundColor=[UIColor clearColor];
    secondaryLabel.font = [UIFont fontWithName:HELVETICA_THIN size:80];
    [secondaryLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:secondaryLabel];
    
    scratcher_Images.backgroundColor=[UIColor clearColor];
    [self.view addSubview:scratcher_Images];
    
    totalTimeVw.backgroundColor=RGBCOLOR(0,32,91);
    totalTimeVw.layer.cornerRadius=20.0f;
    totalTimeVw.userInteractionEnabled=YES;
    UITapGestureRecognizer *timeTap = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(timeTapped)];
    timeTap.delegate=self;
    [totalTimeVw addGestureRecognizer:timeTap];
    [self.view addSubview:totalTimeVw];
    
    totalScoreVw.backgroundColor=RGBCOLOR(86,161,213);
    totalScoreVw.layer.cornerRadius=20.0f;
    totalScoreVw.userInteractionEnabled=YES;
    UITapGestureRecognizer *scoreTap = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(scoreTapped)];
    scoreTap.delegate=self;
    //[totalScoreVw addGestureRecognizer:scoreTap];
    [self.view addSubview:totalScoreVw];
    
    prizeCodeVw.backgroundColor=RGBCOLOR(241,137,3);
    prizeCodeVw.layer.cornerRadius=20.0f;
    prizeCodeVw.userInteractionEnabled=YES;
    UITapGestureRecognizer *prizeCodeTap = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(prizecodeTapped)];
    prizeCodeTap.delegate=self;
   // [prizeCodeVw addGestureRecognizer:prizeCodeTap];
   // [self.view addSubview:prizeCodeVw];
    
    timeTakenLbl.font = [UIFont fontWithName:HELVETICA_THIN size:45];
    timeTakenLbl.textColor = [UIColor whiteColor];
    timeTakenLbl.text=@"TIME TAKEN";
    timeTakenLbl.backgroundColor=[UIColor clearColor];
    [totalTimeVw addSubview:timeTakenLbl];
   
    totalTimeLbl.font = [UIFont fontWithName:HELVETICA_THIN size:180];
    totalTimeLbl.textColor = [UIColor whiteColor];
    totalTimeLbl.text=totalSecondsStr;  // totalsecondsstr
    totalTimeLbl.backgroundColor=[UIColor clearColor];
    [totalTimeVw addSubview:totalTimeLbl];
    
    secsLbl.font = [UIFont fontWithName:HELVETICA_THIN size:35];
    secsLbl.textColor = [UIColor whiteColor];
    secsLbl.text=@"secs";  // totalsecondsstr
    secsLbl.backgroundColor=[UIColor clearColor];
    [totalTimeVw addSubview:secsLbl];
    
    scoreLbl.font = [UIFont fontWithName:HELVETICA_THIN size:73];
    scoreLbl.textColor = [UIColor whiteColor];
    scoreLbl.text=@"SCORE";
    scoreLbl.backgroundColor=[UIColor clearColor];
    [totalScoreVw addSubview:scoreLbl];
    
    totalScoreLbl.textColor = [UIColor whiteColor];
    totalScoreLbl.text=totalScoreStr; //totalScoreStr
    totalScoreLbl.backgroundColor=[UIColor clearColor];
    [totalScoreVw addSubview:totalScoreLbl];
    
    prizeLbl.font = [UIFont fontWithName:HELVETICA_THIN size:95];
    prizeLbl.textColor = [UIColor whiteColor];
    prizeLbl.text=@"PRIZE";
    prizeLbl.backgroundColor=[UIColor clearColor];
    [prizeCodeVw addSubview:prizeLbl];
    
    codeLbl.font = [UIFont fontWithName:HELVETICA_THIN size:88];
    codeLbl.textColor = [UIColor whiteColor];
    codeLbl.text=@"CODE";
    codeLbl.backgroundColor=[UIColor clearColor];
    [prizeCodeVw addSubview:codeLbl];
    
    totalPrizeLbl.font = [UIFont fontWithName:HELVETICA_REGULAR size:85];
    totalPrizeLbl.textColor = [UIColor whiteColor];
    totalPrizeLbl.text=uniqueCodeStr;
    totalPrizeLbl.backgroundColor=[UIColor clearColor];
    [prizeCodeVw addSubview:totalPrizeLbl];
    
    informLbl.font = [UIFont fontWithName:HELVETICA_THIN size:44];
    informLbl.textColor = [UIColor blackColor];
    informLbl.backgroundColor=[UIColor clearColor];
    informLbl.text=@"PLEASE ENTER YOUR UNIQUE PRIZE CODE VIA\n THE LINK SENT TO YOUR CELL PHONE FOR A\n       CHANCE TO WIN A PREMIUM PRIZE!*";
    informLbl.numberOfLines=3;
    //[self.view addSubview:informLbl];
    
    mandatoryLbl.font = [UIFont fontWithName:HELVETICA_THIN size:22];
    mandatoryLbl.textColor = [UIColor blackColor];
    mandatoryLbl.text=@"*MESSAGE AND DATA RATES MAY APPLY";
    mandatoryLbl.backgroundColor=[UIColor clearColor];
    //[self.view addSubview:mandatoryLbl];
    
    
    
    // Attributed String
    
    NSMutableAttributedString *UniqueCodeText =[[NSMutableAttributedString alloc]initWithAttributedString: informLbl.attributedText];
    UIFont *uniqueCodeFont = [UIFont fontWithName:HELVETICA_LIGHT size:46];
    [UniqueCodeText addAttribute:NSFontAttributeName value:uniqueCodeFont range:NSMakeRange(102,15)];
    [informLbl setAttributedText: UniqueCodeText];
    
    // Tap Gesture View
    
    UITapGestureRecognizer *dismissTap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(dismissTapped)];
    dismissTap.delegate=self;
 //   [self.view addGestureRecognizer:dismissTap];
    
    // Dismiss after 10 sec
    
   dismissTimer= [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(dismissAutomatically) userInfo:nil repeats:NO];
    
    //******************************************************ScratcherVC******************************************************
   
    congratulationLbl.frame = CGRectMake(0,90,self.view.frame.size.width,80);
     congratulationLbl.font = [UIFont fontWithName:HELVETICA_THIN size:90];
    [secondaryLabel setFrame:CGRectMake(0, CGRectGetMaxY(congratulationLbl.frame), self.view.frame.size.width, 270)];
    secondaryLabel.font = [UIFont fontWithName:HELVETICA_THIN size:85];
    secondaryLabel.textColor = RGBA(137,137,137, 1);

    scratcher_Images.contentMode = UIViewContentModeCenter;
    scratcher_Images.frame = CGRectMake((CGRectGetWidth(self.view.frame)-[UIImage imageNamed:@"happy_smiley"].size.width)/2, CGRectGetMaxY(secondaryLabel.frame)-20, [UIImage imageNamed:@"happy_smiley"].size.width, [UIImage imageNamed:@"happy_smiley"].size.height);
    
    [totalTimeVw setFrame:CGRectMake(CGRectGetMinX(congratulationLbl.frame)+220, CGRectGetMaxY(congratulationLbl.frame)+85, 270, 270)];

    if ([totalScoreLbl.text isEqualToString:@"0"])
    {
        congratulationLbl.text=@"THANKS FOR PLAYING";
        secondaryLabel.text = @"Sorry, You did not win.";
        
        if(self.isScratcher){
            congratulationLbl.textColor = RGBA(15,55,100, 1);
            scratcher_Images.image=[UIImage imageNamed:@"sad_smiley"];
        }
    }
    else
    {
            congratulationLbl.text=@"CONGRATULATIONS !!";
         secondaryLabel.text = @"You have won a gift card.";
        
        if(self.isScratcher)
        {
            congratulationLbl.textColor = RGBA(241, 137, 3, 1);
            scratcher_Images.image=[UIImage imageNamed:@"happy_smiley"];
        }
       
    }
    
    
   
    // Frames
    
   
    
    totalScoreVw.frame=CGRectMake(CGRectGetMaxX(totalTimeVw.frame)+50,CGRectGetMinY(totalTimeVw.frame),CGRectGetWidth(totalTimeVw.frame),CGRectGetHeight(totalTimeVw.frame));
    prizeCodeVw.frame=CGRectMake(CGRectGetMaxX(totalScoreVw.frame)+50,CGRectGetMinY(totalTimeVw.frame),CGRectGetWidth(totalTimeVw.frame),CGRectGetHeight(totalTimeVw.frame));
    timeTakenLbl.frame=CGRectMake(15,30,CGRectGetWidth(totalTimeVw.frame)-30,40);
    if ([totalTimeLbl.text isEqualToString:@"--"])
    {
        totalTimeLbl.frame=CGRectMake(CGRectGetMinX(timeTakenLbl.frame)+55,CGRectGetMaxY(timeTakenLbl.frame),CGRectGetWidth(totalTimeVw.frame)-2*CGRectGetMinX(timeTakenLbl.frame)+10,150);
        secsLbl.hidden=YES;
    }
    else
    {
        totalTimeLbl.frame=CGRectMake(CGRectGetMinX(timeTakenLbl.frame)+20,CGRectGetMaxY(timeTakenLbl.frame)+12,CGRectGetWidth(totalTimeVw.frame)-2*CGRectGetMinX(timeTakenLbl.frame)+10,150);
        secsLbl.hidden=NO;
    }
    secsLbl.frame=CGRectMake(CGRectGetMinX(totalTimeLbl.frame)+68,CGRectGetMaxY(totalTimeLbl.frame)-14,80,50);
    scoreLbl.frame=CGRectMake(15,20,CGRectGetWidth(totalScoreVw.frame)-30,60);
    
    
    if ([totalScoreLbl.text isEqualToString:@"00"])
    {
        totalScoreLbl.font = [UIFont fontWithName:HELVETICA_THIN size:175];
        totalScoreLbl.frame=CGRectMake(CGRectGetMinX(scoreLbl.frame)+20,CGRectGetMaxY(scoreLbl.frame)+7,CGRectGetWidth(totalScoreVw.frame)-2*CGRectGetMinX(scoreLbl.frame)+10,150);
  
    }
    else if (totalScoreLbl.text.length==3)
    {
        totalScoreLbl.font = [UIFont fontWithName:HELVETICA_REGULAR size:130];
        totalScoreLbl.frame=CGRectMake(CGRectGetMinX(scoreLbl.frame)+10,CGRectGetMaxY(scoreLbl.frame)+7,CGRectGetWidth(totalScoreVw.frame)-2*CGRectGetMinX(scoreLbl.frame)+10,150);
    }
    else if(totalScoreLbl.text.length==4)
    {
        totalScoreLbl.font = [UIFont fontWithName:HELVETICA_REGULAR size:110];
        totalScoreLbl.frame=CGRectMake(CGRectGetMinX(scoreLbl.frame)-2,CGRectGetMaxY(scoreLbl.frame)+7,CGRectGetWidth(totalScoreVw.frame)-2*CGRectGetMinX(scoreLbl.frame)+10,150);
  
    }
    prizeLbl.frame=CGRectMake(15,20,CGRectGetWidth(prizeCodeVw.frame)-30,70);
    codeLbl.frame=CGRectMake(15,CGRectGetMaxY(prizeLbl.frame)+5,CGRectGetWidth(prizeCodeVw.frame)-30,80);
    totalPrizeLbl.frame=CGRectMake(40,CGRectGetMaxY(codeLbl.frame)+3,CGRectGetWidth(prizeCodeVw.frame)-80,80);
    informLbl.frame=CGRectMake(CGRectGetMinX(totalTimeVw.frame)+10,CGRectGetMaxY(totalTimeVw.frame)+58,CGRectGetWidth(self.view.frame)-2*CGRectGetMinX(totalTimeVw.frame),159);
    mandatoryLbl.frame=CGRectMake(CGRectGetWidth(self.view.frame)/2-180,CGRectGetMaxY(informLbl.frame)+10,500,30);
    
    if(self.isScratcher){
        totalTimeVw.hidden = YES;
        totalScoreVw.hidden = YES;

    }else{
        secondaryLabel.hidden = YES;
        scratcher_Images.hidden=YES;
    }
}

#pragma mark -
#pragma mark - Tap Gesture Method

-(void)timeTapped
{
    if ([gameTypeStr isEqualToString:@"scratcher"])
    {
        ViewController *viewController=[[ViewController alloc]init];
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

-(void)scoreTapped
{
    if ([gameTypeStr isEqualToString:@"scratcher"])
    {
        ViewController *viewController=[[ViewController alloc]init];
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }

}

-(void)prizecodeTapped
{
    if ([gameTypeStr isEqualToString:@"scratcher"])
    {
        ViewController *viewController=[[ViewController alloc]init];
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
 
}

-(void)dismissTapped
{
    if ([gameTypeStr isEqualToString:@"scratcher"])
    {
        ViewController *viewController=[[ViewController alloc]init];
        [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    
}

-(void)dismissAutomatically
{
    if ([gameTypeStr isEqualToString:@"scratcher"])
    {
    ViewController *viewController=[[ViewController alloc]init];
    [self.navigationController pushViewController:viewController animated:NO];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

#pragma mark -
#pragma mark - Finish button Action

-(void)finishBtnTapped:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(BOOL)prefersStatusBarHidden
{
    return  YES ;
}
#pragma mark-
#pragma mark - Status button Action

-(void)statusBtnTap:(id)sender
{
    count++;
    
    if (count==3)
    {
        if ([gameTypeStr isEqualToString:@"scratcher"])
        {
            ViewController *viewController=[[ViewController alloc]init];
            [self.navigationController pushViewController:viewController animated:NO];
        }
        else
        {
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
    }
    
    timer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                   target:self
                                                 selector:@selector(reduceCount)
                                                 userInfo:nil
                                                  repeats:NO];
}

-(void)reduceCount
{
    count=0;
}

@end
