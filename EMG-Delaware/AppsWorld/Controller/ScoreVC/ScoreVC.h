//
//  ScoreVC.h
//  AppsWorld
//
//  Created by Apple on 01/05/15.
//
//

#import <UIKit/UIKit.h>

@interface ScoreVC : UIViewController
{
    UIImageView *scratcher_Images;
}
@property(nonatomic,retain)NSString *totalSecondsStr;
@property(nonatomic,retain)NSString *totalScoreStr;
@property(nonatomic,retain)NSString *uniqueCodeStr;
@property(nonatomic,retain)NSString *gameTypeStr;
@property(strong, nonatomic)UIImageView *scratcher_Images;
@property(nonatomic) BOOL isScratcher;

@end
