//
//  SpotTheDifferVC.m
//  AppsWorld
//
//  Created by Mac User on 30/04/15.
//
//

#import "SpotTheDifferVC.h"
#import "Define.h"
#import "AFNetworking.h"
#import "FXReachability.h"
#import "ScoreVC.h"
#import "MBProgressHUD.h"
#import <unistd.h>

@interface SpotTheDifferVC ()<MBProgressHUDDelegate>
{
    NSArray *staticData;
    UIImageView *firstPhoto,*secondPhoto;
    NSMutableArray *buttonAry;
    UIButton *selectedBtn;
    BOOL checkAryCount;
    int count,index;
    
    // Bottom view
    
    int secondsForGame,calculateMatchesCount;
    NSTimer *timerForGame;
    UILabel *myCounterLabel,*timeLbl,*findLbl,*diffLbl,*matchesLbl,*secondsLbl,*matchesCountLbl;
    UIView *timerView,*firstHorLine,*secondHorLine,*matchesView;
    BOOL firstCheck,secondCheck,thirdCheck,fourthCheck,fifthCheck,sixCheck;
    
    UIAlertView *alert;
    UIButton *btn;
    NSString *scoreCardStr;
    
    MBProgressHUD *HUD;
    UIButton *statusBtn;
    int tapCount;
    NSTimer *dismissTimer;
}
@end

@implementation SpotTheDifferVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    buttonAry=[[NSMutableArray alloc]init];
    
    timerForGame=[[NSTimer alloc]init];
    timerView=[[UIView alloc]init];
    secondsLbl=[[UILabel alloc]init];
    timeLbl=[[UILabel alloc]init];
    firstHorLine=[[UIView alloc]init];
    findLbl=[[UILabel alloc]init];
    diffLbl=[[UILabel alloc]init];
    secondHorLine=[[UIView alloc]init];
    matchesLbl=[[UILabel alloc]init];
    matchesView=[[UIView alloc]init];
    matchesCountLbl=[[UILabel alloc]init];
    
    [self loadStaticData];
    
    [self timerStart];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Notification_3" object:self];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self findMatchUI];
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [timerForGame invalidate];       // Stop Timer
    timerForGame = nil;
}

-(void)findMatchUI
{
    count=0;
    calculateMatchesCount=1;
    
    firstCheck=NO;
    secondCheck=NO;
    thirdCheck=NO;
    fourthCheck=NO;
    fifthCheck=NO;
    sixCheck=NO;
    
    tapCount=0;
    
    statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusBtn.frame =  CGRectMake(0,0,CGRectGetWidth(self.view.frame),50);
    [statusBtn setBackgroundColor:[UIColor clearColor]];
    [statusBtn addTarget:self action:@selector(statusBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:statusBtn];
    
    firstPhoto = [UIImageView new];
    firstPhoto.backgroundColor=[UIColor clearColor];
    [self.view addSubview:firstPhoto];
    
    secondPhoto = [UIImageView new];
    secondPhoto.backgroundColor=[UIColor clearColor];
    [self.view addSubview:secondPhoto];
    
    if([staticData count]!=0)
    {
        for (NSDictionary *StaticDataDic in staticData)
        {
            if (count==0)
            {
                firstPhoto.image=[UIImage imageNamed:[StaticDataDic valueForKey:@"ImageName"]];
            }
            else
            {
                secondPhoto.image=[UIImage imageNamed:[StaticDataDic valueForKey:@"ImageName"]];
            }
            
            NSArray *buttonRectAry=[StaticDataDic valueForKey:@"ButtonRect"];
            
            for(int i=0;i<buttonRectAry.count;i++)
            {
                
                count++;
                
                CGFloat x=  (CGFloat)[[[buttonRectAry objectAtIndex:i]valueForKey:@"x"]floatValue];
                CGFloat y=  (CGFloat)[[[buttonRectAry objectAtIndex:i]valueForKey:@"y"]floatValue];
                CGFloat width=  (CGFloat)[[[buttonRectAry objectAtIndex:i]valueForKey:@"Width"]floatValue];
                CGFloat height=  (CGFloat)[[[buttonRectAry objectAtIndex:i]valueForKey:@"Height"]floatValue];
                
                selectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                selectedBtn.frame=CGRectMake(x,y,width,height);
                [selectedBtn setBackgroundColor:[UIColor clearColor]];
                selectedBtn.tag=count;
                [selectedBtn addTarget:self action:@selector(selectedBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:selectedBtn];
                
                [buttonAry addObject:selectedBtn];
            }
        }
    }
    
    // Bottom view
    
    timerView.backgroundColor=RGBCOLOR(241,137,3);
    timerView.layer.cornerRadius=5.0f;
    timerView.userInteractionEnabled=YES;
    [self.view addSubview:timerView];
    
    secondsLbl.font = [UIFont fontWithName:HELVETICA_THIN size:50];
    secondsLbl.textColor = [UIColor whiteColor];
    [secondsLbl adjustsFontSizeToFitWidth];
    [self.view addSubview:secondsLbl];
    
    timeLbl.font = [UIFont fontWithName:HELVETICA_THIN size:85];
    timeLbl.textColor = [UIColor blackColor];
    timeLbl.text=@"TIME";
    [timeLbl adjustsFontSizeToFitWidth];
    [self.view addSubview:timeLbl];
    
    firstHorLine.backgroundColor=RGBCOLOR(241,137,3);
    firstHorLine.userInteractionEnabled=YES;
    [self.view addSubview:firstHorLine];
    
    findLbl.font = [UIFont fontWithName:HELVETICA_THIN size:35];
    findLbl.textColor = RGBCOLOR(0,32,91);
    findLbl.text=@"FIND THE 6";
    [self.view addSubview:findLbl];
    
    diffLbl.font = [UIFont fontWithName:HELVETICA_THIN size:31.5];
    diffLbl.textColor = RGBCOLOR(0,32,91);
    diffLbl.text=@"DIFFERENCES";
    [self.view addSubview:diffLbl];
    
    secondHorLine.backgroundColor=RGBCOLOR(86,161,213);
    secondHorLine.userInteractionEnabled=YES;
    [self.view addSubview:secondHorLine];
    
    matchesLbl.font = [UIFont fontWithName:HELVETICA_THIN size:80];
    matchesLbl.textColor = [UIColor blackColor];
    matchesLbl.text=@"MATCHES";
    [matchesLbl adjustsFontSizeToFitWidth];
    [self.view addSubview:matchesLbl];
    
    matchesView.backgroundColor=RGBCOLOR(86,161,213);
    matchesView.layer.cornerRadius=5.0f;
    matchesView.userInteractionEnabled=YES;
    [self.view addSubview:matchesView];
    
    matchesCountLbl.font = [UIFont fontWithName:HELVETICA_THIN size:55];
    matchesCountLbl.textColor = [UIColor whiteColor];
    [matchesCountLbl adjustsFontSizeToFitWidth];
    matchesCountLbl.text = @"0";
    [self.view addSubview:matchesCountLbl];
    
    // Frames
    
    firstPhoto.frame=CGRectMake(65,30,[UIImage imageNamed:@"SpotTheDiff_1"].size.width,[UIImage imageNamed:@"SpotTheDiff_1"].size.height);
    secondPhoto.frame=CGRectMake(CGRectGetMaxX(firstPhoto.frame)+35,CGRectGetMinY(firstPhoto.frame),[UIImage imageNamed:@"SpotTheDiff_2"].size.width,[UIImage imageNamed:@"SpotTheDiff_2"].size.height);
    timerView.frame=CGRectMake(CGRectGetMinX(firstPhoto.frame)-40,CGRectGetMaxY(firstPhoto.frame)+10,80,75);
    secondsLbl.frame = CGRectMake(CGRectGetMinX(timerView.frame)+12,CGRectGetMinY(timerView.frame),70,70);
    timeLbl.frame = CGRectMake(CGRectGetMaxX(timerView.frame)+10,CGRectGetMinY(timerView.frame),180,70);
    firstHorLine.frame=CGRectMake(CGRectGetMaxX(timeLbl.frame)+5,CGRectGetMinY(timeLbl.frame)+5,1,63);
    findLbl.frame = CGRectMake(CGRectGetMaxX(firstHorLine.frame)+10,CGRectGetMinY(firstHorLine.frame),195,30);
    diffLbl.frame = CGRectMake(CGRectGetMinX(findLbl.frame),CGRectGetMaxY(findLbl.frame)+5,195,30);
    secondHorLine.frame=CGRectMake(CGRectGetMaxX(findLbl.frame)+8,CGRectGetMinY(firstHorLine.frame),1,63);
    matchesLbl.frame = CGRectMake(CGRectGetMaxX(secondHorLine.frame)+5,CGRectGetMinY(timeLbl.frame),385,70);
    matchesView.frame=CGRectMake(CGRectGetMaxX(matchesLbl.frame)+5,CGRectGetMinY(timerView.frame),80,75);
    matchesCountLbl.frame = CGRectMake(CGRectGetMinX(matchesView.frame)+25,CGRectGetMinY(timerView.frame),70,70);
}

#pragma mark -
#pragma mark - Load Static Data

-(void)loadStaticData
{
    staticData = @[
                   @{@"ImageName" : @"SpotTheDiff_1",
                     @"ButtonRect":@[
                             @{@"x":@"310", @"y":@"460",@"Width":@"70",@"Height":@"70"},
                             @{@"x":@"130", @"y":@"50",@"Width":@"245",@"Height":@"150"},
                             @{@"x":@"285", @"y":@"525",@"Width":@"66",@"Height":@"66"},
                             @{@"x":@"395", @"y":@"50",@"Width":@"100",@"Height":@"100"},
                             @{@"x":@"345", @"y":@"215",@"Width":@"80",@"Height":@"90"},
                             @{@"x":@"155", @"y":@"315",@"Width":@"100",@"Height":@"100"},
                             ],
                     },
                   @{@"ImageName" : @"SpotTheDiff_2",
                     @"ButtonRect":@[
                             @{@"x":@"770", @"y":@"460",@"Width":@"70",@"Height":@"70"},
                             @{@"x":@"600", @"y":@"50",@"Width":@"245",@"Height":@"150"},
                             @{@"x":@"750", @"y":@"525",@"Width":@"66",@"Height":@"66"},
                             @{@"x":@"860", @"y":@"50",@"Width":@"100",@"Height":@"100"},
                             @{@"x":@"810", @"y":@"210",@"Width":@"80",@"Height":@"90"},
                             @{@"x":@"615.0000", @"y":@"315",@"Width":@"100",@"Height":@"100"},
                             ],
                     }
                   ];
}

#pragma mark -
#pragma mark - Timer Method

- (void)timerStart
{
    secondsForGame = 91;
    
    secondsLbl.text = [NSString stringWithFormat:@"%.2d",secondsForGame];
    
    timerForGame = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    
    [timerForGame fire];
    
}

- (void)updateTimer
{
    secondsForGame--;
    secondsLbl.text = [NSString stringWithFormat:@"%.2d",secondsForGame];
    
    if (secondsForGame==0)
    {
        [self spotTheDifferService];
        
        if ([timerForGame isValid]) {
            
            [timerForGame invalidate];       // Stop Timer
            
            timerForGame = nil;
        }
    }
}

#pragma mark -
#pragma mark -Selected Button Action

-(void)selectedBtnTapped:(id)sender
{
    btn=(UIButton *)sender;
    index = btn.tag;
    
    if (btn.tag<=6)
    {
        if (index==1)
        {
            if (firstCheck==NO)
            {
                firstCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:7];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-medium"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-medium"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%d",calculateMatchesCount++];
            }
            else if ((firstCheck=YES))
            {
                
            }
        }
        else if (index==2)
        {
            if (secondCheck==NO)
            {
                secondCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:8];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"sspot-circle"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"sspot-circle"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((secondCheck=YES))
            {
                
            }
        }
        else if (index==3)
        {
            if (thirdCheck==NO)
            {
                thirdCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:9];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((thirdCheck=YES))
            {
                
            }
        }
        else if (index==4)
        {
            if (fourthCheck==NO)
            {
                fourthCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:10];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((fourthCheck=YES))
            {
                
            }
        }
        else if (index==5)
        {
            if (fifthCheck==NO)
            {
                fifthCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:11];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((fifthCheck=YES))
            {
                
            }
        }
        else if (index==6)
        {
            if (sixCheck==NO)
            {
                sixCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:12];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-small"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-small"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((sixCheck=YES))
            {
                
            }
        }
    }
    else if (btn.tag>6)
    {
        if (index==7)
        {
            if (firstCheck==NO)
            {
                firstCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:1];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-medium"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-medium"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((firstCheck=YES))
            {
                
            }
        }
        else if (index==8)
        {
            if (secondCheck==NO)
            {
                secondCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:2];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"sspot-circle"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"sspot-circle"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((secondCheck=YES))
            {
                
            }
        }
        else if (index==9)
        {
            if (thirdCheck==NO)
            {
                thirdCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:3];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((thirdCheck=YES))
            {
                
            }
        }
        else if (index==10)
        {
            if (fourthCheck==NO)
            {
                fourthCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:4];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((fourthCheck=YES))
            {
                
            }
        }
        else if (index==11)
        {
            if (fifthCheck==NO)
            {
                fifthCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:5];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-big"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((fifthCheck=YES))
            {
                
            }
        }
        else if (index==12)
        {
            if (sixCheck==NO)
            {
                sixCheck=YES;
                selectedBtn = (UIButton *)[self.view viewWithTag:6];
                [selectedBtn setBackgroundImage:[UIImage imageNamed:@"spot-circle-small"] forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"spot-circle-small"] forState:UIControlStateNormal];
                selectedBtn.userInteractionEnabled=NO;
                btn.userInteractionEnabled=NO;
                
                matchesCountLbl.text = [NSString stringWithFormat:@"%i",calculateMatchesCount++];
            }
            else if ((sixCheck=YES))
            {
                
            }
        }
    }
    
    if ([matchesCountLbl.text isEqualToString:@"6"])
    {
        
        if ([timerForGame isValid]) {
            
            [timerForGame invalidate];       // Stop Timer
            timerForGame = nil;
            
//            alert = [[UIAlertView alloc] initWithTitle:@"Congratulations!" message:@"You have successfully completed the spot the difference game" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Reset",nil];
//            [alert show];
//            
            [self spotTheDifferService];
        }
    }
}

#pragma mark -
#pragma mark AlertViewDelegate Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(buttonIndex==1)
    {
        [self timerStart];
        
        matchesCountLbl.text=@"0";
        calculateMatchesCount=1;
        
        for (int i =1;i<=count;i++)
        {
            selectedBtn = (UIButton *)[self.view viewWithTag:i];
            [selectedBtn setBackgroundImage:nil forState:UIControlStateNormal];
        }
        
        firstCheck=NO;
        secondCheck=NO;
        thirdCheck=NO;
        fourthCheck=NO;
        fifthCheck=NO;
        sixCheck=NO;
    }
}

#pragma mark - Matching Game Method

-(void)spotTheDifferService
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if([FXReachability isReachable]){
        
//        NSDictionary *inputDict = @{@"appId":@"100SP",@"time":@"12",@"score":@"10",@"matches":@"5",@"userId":@"5",@"device":@"web"};

        int timeTaken=90-[secondsLbl.text intValue];
        NSString *totalTimeTaken=[NSString stringWithFormat:@"%.2d",timeTaken];
        
        int scoreCard=[matchesCountLbl.text intValue]*100 + [secondsLbl.text intValue]*10;
        scoreCardStr=[NSString stringWithFormat:@"%d",scoreCard];
        
        NSDictionary *inputDict = @{@"appId":@"100SP",@"time":secondsLbl.text,@"score":scoreCardStr,@"matches":matchesCountLbl.text,@"userId":[[NSUserDefaults standardUserDefaults]valueForKey:USERID],@"device":@"web",@"flag":self.isNewUserString
                                    };
        NSLog(@"Spot the Difference input : %@",inputDict);

        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *urlString=[NSString stringWithFormat:@"%@",GAME_SERVICE];
        [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
        {
            [formData appendPartWithFormData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"input"];
        }
              success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  NSDictionary *resultDict = (NSDictionary*)responseObject;
                  NSLog(@"Spot the Difference : %@",resultDict);
                  
                  NSString *score;
                  if ([scoreCardStr isEqualToString:@"0"])
                  {
                      score=@"00";
                  }
                  else
                      
                  {
                      score=scoreCardStr;
                  }
                  
                  if ([[[resultDict valueForKey:@"code"] stringValue] isEqualToString:@"1000"])
                  {
                      ScoreVC *scoreVC = [[ScoreVC alloc]init];
                      scoreVC.totalSecondsStr=totalTimeTaken;
                      scoreVC.totalScoreStr=score;
                      scoreVC.uniqueCodeStr=[[resultDict valueForKey:@"u_code"]stringValue];
                      scoreVC.gameTypeStr=@"MatchGame";
                      [self.navigationController pushViewController:scoreVC animated:YES];
                  }
                  else if ([[[resultDict valueForKey:@"code"] stringValue] isEqualToString:@"2000"])
                  {
                      [MBProgressHUD hideHUDForView:self.view animated:YES];

//                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter the Valid Details" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                      [alertView show];
                  }

              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"There is a problem connecting to the server. Please check your internet connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
              }];
    }
    else{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Seems your internet is offline" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [offlineAlert show];
    }
    
}

#pragma mark-
#pragma mark - Hide Loading View

- (void)myTask
{
    // Do something usefull in here instead of sleeping ...
}

//-(void)showLoadingView
//{
//    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(360,320,250,200)];
//    [self.navigationController.view addSubview:HUD];
//    HUD.color = [UIColor blackColor];
//    HUD.delegate = self;
//    HUD.labelText = @"Loading...";
//    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
//    [self.view addSubview:HUD];
//}

-(void)hideLoadingView
{
    [HUD hide:YES];
}

-(BOOL)prefersStatusBarHidden
{
    return  YES ;
}

#pragma mark-
#pragma mark - Status button Action

-(void)statusBtnTap:(id)sender
{
    tapCount++;
    
    if (tapCount==3)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    dismissTimer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                   target:self
                                                 selector:@selector(reduceCount)
                                                 userInfo:nil
                                                  repeats:NO];
}

-(void)reduceCount
{
    tapCount=0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
