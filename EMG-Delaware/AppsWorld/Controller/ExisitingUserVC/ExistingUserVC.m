 //
//  UsersVC.m
//  AppsWorld
//
//  Created by Mac User on 04/05/15.
//
//
#import  "ExistingUserVC.h"
#import  "AFNetworking.h"
#import  "Define.h"
#import  "DataCaptureVC.h"
#import  "FXReachability.h"
#import "SelectionGameVC.h"
#import "MBProgressHUD.h"
#import <unistd.h>
#import "DataCaptureVC.h"
#import "PKCustomKeyboard.h"

#define OPTION_BUTTON_WIDTH 200
#define OPTION_BUTTON_HEIGHT 50

@interface ExistingUserVC ()<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    UIButton *statusBtn;
    int count;
    NSTimer *dismissTimer;
    UIView *paddingView,*paddingView1;
    BOOL removeAlert;
    UIAlertView *alert;
    UIButton *backBtn;
    
}
@end

@implementation ExistingUserVC

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    _loginView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 768)];
    _loginView.backgroundColor = RGBA(86, 161, 213, 1);
    [self.view addSubview:_loginView];
    
    [[self navigationController]setNavigationBarHidden:YES animated:YES];
    
    _dataCaptureVC = [[DataCaptureVC alloc]init];
    _dataCaptureVC.emailIdStr = @"";
    
    [[self navigationController]setNavigationBarHidden:YES animated:YES];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)setFrames
{
    removeAlert=NO;
    
    [self.welcomeLabl setFrame:CGRectMake(10, 0, 500, 150)];
    [self.welcomeLabl setFrame:CGRectMake(10,38, 500, 80)];
    
    [self.separatorVW setFrame:CGRectMake(CGRectGetMaxX([_welcomeLabl frame])+15, CGRectGetMinY([_welcomeLabl frame])+2, 1, CGRectGetHeight([_welcomeLabl frame])-5)];
    
    [self.pleaseEnterLabl setFrame:CGRectMake(CGRectGetMaxX([_separatorVW frame])+15, CGRectGetMinY([_welcomeLabl frame]), CGRectGetWidth([_welcomeLabl frame])-50, CGRectGetHeight([_welcomeLabl frame])/2.9)];
    
    [self.followingLabl setFrame:CGRectMake(CGRectGetMinX([_pleaseEnterLabl frame]), CGRectGetMaxY([_pleaseEnterLabl frame])+8, CGRectGetWidth([_pleaseEnterLabl frame]), CGRectGetHeight([_pleaseEnterLabl frame])+20)];
    
    [self.emailLabl setFrame:CGRectMake(CGRectGetMinX([_welcomeLabl frame])+10, CGRectGetMaxY([_welcomeLabl frame])+240, 207, 60)];

    [self.emailTxtfld setFrame:CGRectMake(CGRectGetMaxX([_emailLabl frame])+10, CGRectGetMinY([_emailLabl frame])+5, 400, CGRectGetHeight([_emailLabl frame])-10)];
    
    paddingView.frame=CGRectMake(0,CGRectGetMinY([_emailTxtfld frame]),20,50);
    paddingView1.frame=CGRectMake(CGRectGetWidth(self.emailTxtfld.frame)-10,CGRectGetMinY([_emailTxtfld frame]),10,50);

    [self.emailLineVW setFrame:CGRectMake(CGRectGetMinX([_emailTxtfld frame])+20, CGRectGetMinY([_emailTxtfld frame])+40, CGRectGetWidth([_emailTxtfld frame])-2*20, 1)];
    
    count=0;
    
    statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusBtn.frame =  CGRectMake(0,0,CGRectGetWidth(self.view.frame),50);
    [statusBtn setBackgroundColor:[UIColor clearColor]];
    [statusBtn addTarget:self action:@selector(statusBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:statusBtn];
    
    /* SUBMIT / PLAY BTN */
    
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame =  CGRectMake(CGRectGetWidth(self.view.frame)-150,690, 130, 49);
    [_submitBtn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    _submitBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:35];
    _submitBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_submitBtn setBackgroundColor:RGBA(241, 137, 3, 1)];
    _submitBtn.layer.cornerRadius = 6;
    _submitBtn.clipsToBounds = YES;
    _submitBtn.exclusiveTouch = YES;
    _submitBtn.userInteractionEnabled=YES;
    [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame =  CGRectMake(15,690, 130, 49);
    [backBtn setTitle:@"BACK" forState:UIControlStateNormal];
    backBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    backBtn.titleLabel.font = [UIFont fontWithName:HELVETICA_THIN size:35];
    backBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [backBtn setBackgroundColor:RGBA(15,55,100, 1)];
    backBtn.layer.cornerRadius = 6;
    backBtn.clipsToBounds = YES;
    backBtn.exclusiveTouch = YES;
    backBtn.userInteractionEnabled=YES;
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
}

-(void)backBtnTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self setFrames];
    
    [_loginView addSubview:_welcomeLabl];
    
    [_loginView addSubview:_separatorVW];
    
    [_loginView addSubview:_pleaseEnterLabl];
    
    [_loginView addSubview:_followingLabl];
    
    [_loginView addSubview:_emailLabl];
    
    CAGradientLayer *emailGradient = [CAGradientLayer layer];
    emailGradient.frame = self.emailTxtfld.layer.bounds;
    emailGradient.colors = [NSArray arrayWithObjects:
                            (id)RGBA(209, 210, 212, 1).CGColor,
                            (id)RGBA(250, 250, 250, 1).CGColor,nil];
    emailGradient.cornerRadius = self.emailTxtfld.layer.cornerRadius;
    [self.emailTxtfld.layer addSublayer:emailGradient];
    
    [_loginView addSubview:_emailTxtfld];
    
    [_loginView addSubview:_emailLineVW];
    
    [_loginView addSubview:_submitBtn];
    
    [_loginView addSubview:backBtn];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [alert removeFromSuperview];
    alert=nil;
}

-(BOOL)prefersStatusBarHidden
{
    return  YES ;
}
#pragma mark - Custom Setter/Getter

-(UILabel *)emailLabl
{
    if (!_emailLabl) {
        
        _emailLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _emailLabl.text = @"ENTER EMAIL :";
        _emailLabl.font = [UIFont fontWithName:HELVETICA_THIN size:32];
        _emailLabl.textAlignment = NSTextAlignmentLeft;
        _emailLabl.backgroundColor = [UIColor clearColor];
        _emailLabl.textColor = [UIColor whiteColor];
        
    }
    return _emailLabl;
}

-(UITextField *)emailTxtfld
{
    if (!_emailTxtfld) {
        
        _emailTxtfld = [[UITextField alloc] initWithFrame:CGRectZero];
        _emailTxtfld.delegate = self;
        _emailTxtfld.backgroundColor = [UIColor whiteColor];
        _emailTxtfld.layer.cornerRadius = 6;
        _emailTxtfld.userInteractionEnabled = YES;
        _emailTxtfld.font = [UIFont fontWithName:HELVETICA_THIN size:30];
        _emailTxtfld.textAlignment = NSTextAlignmentLeft;
        _emailTxtfld.textColor = [UIColor blackColor];
        _emailTxtfld.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _emailTxtfld.keyboardType = UIKeyboardTypeURL;
        paddingView = [[UIView alloc]init];
        paddingView1 = [[UIView alloc]init];
        [_emailTxtfld addSubview:paddingView1];
        [_emailTxtfld addSubview:paddingView];
        [_emailTxtfld setLeftViewMode:UITextFieldViewModeAlways];
        [_emailTxtfld setLeftView:paddingView];
        [_emailTxtfld setRightView:paddingView1];
        [_emailTxtfld setRightViewMode:UITextFieldViewModeAlways];
        [_emailTxtfld setReturnKeyType:UIReturnKeyGo];
    }
    return _emailTxtfld;
}
-(UIView *)emailLineVW
{
    if (!_emailLineVW) {
        
        _emailLineVW = [[UIView alloc]initWithFrame:CGRectZero];
        _emailLineVW.backgroundColor = [UIColor blackColor];
    }
    return _emailLineVW;
}
-(UILabel *)welcomeLabl
{
    if (!_welcomeLabl) {
        
        _welcomeLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _welcomeLabl.text = @"WELCOME";
        _welcomeLabl.font = [UIFont fontWithName:HELVETICA_THIN size:100];
        _welcomeLabl.textAlignment = NSTextAlignmentCenter;
        _welcomeLabl.backgroundColor = [UIColor clearColor];
        _welcomeLabl.textColor = [UIColor whiteColor];
    }
    return _welcomeLabl;
}

-(UIView *)separatorVW
{
    if (!_separatorVW) {
        
        _separatorVW = [[UIView alloc]initWithFrame:CGRectZero];
        _separatorVW.backgroundColor = RGBA(239, 137, 34, 1);
        
    }
    return _separatorVW;
}

-(UILabel *)pleaseEnterLabl
{
    if (!_pleaseEnterLabl) {
        
        _pleaseEnterLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _pleaseEnterLabl.text = @"PLEASE ENTER THE";
        _pleaseEnterLabl.font = [UIFont fontWithName:HELVETICA_THIN size:34];
        _pleaseEnterLabl.numberOfLines = 2;
        _pleaseEnterLabl.textAlignment = NSTextAlignmentLeft;
        _pleaseEnterLabl.backgroundColor = [UIColor clearColor];
        _pleaseEnterLabl.textColor = [UIColor whiteColor];
    }
    return _pleaseEnterLabl;
}
-(UILabel *)followingLabl
{
    if (!_followingLabl) {
        
        _followingLabl = [[UILabel alloc]initWithFrame:CGRectZero];
        _followingLabl.text = @"FOLLOWING";
        _followingLabl.font = [UIFont fontWithName:HELVETICA_THIN size:55];
        _followingLabl.numberOfLines = 2;
        _followingLabl.textAlignment = NSTextAlignmentLeft;
        _followingLabl.backgroundColor = [UIColor clearColor];
        _followingLabl.textColor = [UIColor whiteColor];
    }
    return _followingLabl;
}

#pragma mark - SUBMIT BUTTON ACTION

-(void)submitBtnTapped
{
    _submitBtn.userInteractionEnabled=NO;

    if ([_emailTxtfld.text length] == 0) {
        
        [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter the email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        _submitBtn.userInteractionEnabled=YES;
    }
    else if ([self validateEmailWithString:_emailTxtfld.text])
    {
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSRange range = [_emailTxtfld.text rangeOfCharacterFromSet:whitespace];
        if (range.location != NSNotFound) {
            // There is whitespace.
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            _submitBtn.userInteractionEnabled=YES;
        }
        else
        {
            [self loginService];
        }

    }
    else
    {
       alert= [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        _submitBtn.userInteractionEnabled=YES;
    }
}
#pragma mark - Email Validation

- (BOOL)validateEmailWithString:(NSString*)email
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark-
#pragma mark - TextField Delegate Method

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.emailLabl setFrame:CGRectMake(CGRectGetMinX([_welcomeLabl frame])+10, CGRectGetMaxY([_welcomeLabl frame])+220, 207, 60)];
    
    [self.emailTxtfld setFrame:CGRectMake(CGRectGetMaxX([_emailLabl frame])+10, CGRectGetMinY([_emailLabl frame])+5, 400, CGRectGetHeight([_emailLabl frame])-10)];
    
    [self.emailLineVW setFrame:CGRectMake(CGRectGetMinX([_emailTxtfld frame])+20, CGRectGetMinY([_emailTxtfld frame])+40, CGRectGetWidth([_emailTxtfld frame])-2*20, 1)];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _customKeyboard = [[PKCustomKeyboard alloc] init];
    _customKeyboard.dataCaptureDelegate = self;
    [_customKeyboard setTextView:self.emailTxtfld];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self.emailLabl setFrame:CGRectMake(CGRectGetMinX([_welcomeLabl frame])+10, CGRectGetMaxY([_welcomeLabl frame])+240, 207, 60)];
    
    [self.emailTxtfld setFrame:CGRectMake(CGRectGetMaxX([_emailLabl frame])+10, CGRectGetMinY([_emailLabl frame])+5, 400, CGRectGetHeight([_emailLabl frame])-10)];
    
    paddingView.frame=CGRectMake(0,CGRectGetMinY([_emailTxtfld frame]),20,50);
    paddingView1.frame=CGRectMake(CGRectGetWidth(self.emailTxtfld.frame)-10,CGRectGetMinY([_emailTxtfld frame]),10,50);
    
    [self.emailLineVW setFrame:CGRectMake(CGRectGetMinX([_emailTxtfld frame])+20, CGRectGetMinY([_emailTxtfld frame])+40, CGRectGetWidth([_emailTxtfld frame])-2*20, 1)];
    
    
  /*  if ([self validateEmailWithString:textField.text])
    {
        
        if ([_emailTxtfld.text length] == 0)
        {
            
            [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter the email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else if ([self validateEmailWithString:_emailTxtfld.text])
        {
            [self loginService];
        }

        return YES;
    }
    else
    {
        if (removeAlert==NO)
        {
        alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        }
        return YES;
    }*/
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([self validateEmailWithString:textField.text]||[textField.text length]==0) {
        
        [textField resignFirstResponder];
        
        return YES;
    }
    else
    {
        alert=[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}
-(void)textfieldShouldReturn
{
    if (_emailTxtfld)
    {
        if ([_emailTxtfld.text length] == 0) {
            
            [[[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter the email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
        }
        else if ([self validateEmailWithString:_emailTxtfld.text])
        {
            NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSRange range = [_emailTxtfld.text rangeOfCharacterFromSet:whitespace];
            if (range.location != NSNotFound) {
                // There is whitespace.
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else
            {
                [self loginService];
            }
            
        }
        else
        {
            alert= [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Please enter valid email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }

    }
}


#pragma mark - Email Login Service Method

-(void)loginService
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    if([FXReachability isReachable])
    {
        NSDictionary *inputDict = @{@"email":_emailTxtfld.text,@"device":@"iPad",@"signup":@"no"};
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *urlString=[NSString stringWithFormat:@"%@",LOGIN_SERVICE];
        [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFormData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"input"];
        }
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  NSDictionary *resultDict = (NSDictionary*)responseObject;
                  NSLog(@"%@",resultDict);
                  
                  [[NSUserDefaults standardUserDefaults]setObject:[resultDict valueForKey:@"userId"] forKey:USERID];
                  
                  [[NSUserDefaults standardUserDefaults]setObject:[[[resultDict valueForKey:@"appDetail"]valueForKey:@"100ME"]stringValue] forKey:MEMORYGAMESTATUS];
                  [[NSUserDefaults standardUserDefaults]setObject:[[[resultDict valueForKey:@"appDetail"]valueForKey:@"100SC"]stringValue] forKey:SCRATCHGAMESSTATUS];
                  [[NSUserDefaults standardUserDefaults]setObject:[[[resultDict valueForKey:@"appDetail"]valueForKey:@"100SP"]stringValue] forKey:SPOTDIFFGAMESTATUS];
                  
                  if ([[[resultDict valueForKey:@"code"]stringValue] isEqualToString:@"1000"])
                  {
                      
//                      if ([[[NSUserDefaults standardUserDefaults]valueForKey:MEMORYGAMESTATUS] isEqualToString:@"1"] &&[[[NSUserDefaults standardUserDefaults]valueForKey:SCRATCHGAMESSTATUS] isEqualToString:@"1"]&&[[[NSUserDefaults standardUserDefaults]valueForKey:SPOTDIFFGAMESTATUS] isEqualToString:@"1"])
//                      {
//                          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Thanks for playing all our interactive games.We hope you enjoyed playing our games." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
//                          alertView.tag=1;
//                          [alertView show];
//                      }
//                      else
//                      {
//                          SelectionGameVC *selectionGameVC = [[SelectionGameVC alloc]init];
//                          [self.navigationController pushViewController:selectionGameVC animated:YES];
//                      }
                      
                      
                      SelectionGameVC *selectionGameVC = [[SelectionGameVC alloc]init];
                      [self.navigationController pushViewController:selectionGameVC animated:YES];
                      
       
                  }
                  else if([[[resultDict valueForKey:@"code"]stringValue] isEqualToString:@"2000"])
                  {
                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"You are not a registered user. Please take a moment to register with us." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
                      alertView.tag=2;
                      [alertView show];
                  }
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"There is a problem connecting to the server. Please check your internet connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
                  
              }];
    }
    else{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Seems your internet is offline" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [offlineAlert show];
    }
}

#pragma mark-
#pragma mark - Hide Loading View

- (void)myTask
{
    // Do something usefull in here instead of sleeping ...
}

-(void)hideHud
{
    [HUD hide:YES];
    
    // sleep(3);
}

#pragma mark-
#pragma mark - Status button Action

-(void)statusBtnTap:(id)sender
{
    count++;
    
    if (count==3)
    {
        [self.navigationController popViewControllerAnimated:NO];
        
        removeAlert=YES;
    }
    
    dismissTimer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                   target:self
                                                 selector:@selector(reduceCount)
                                                 userInfo:nil
                                                  repeats:NO];
    
}

-(void)reduceCount
{
    count=0;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
    else if (alertView.tag==2)
    {
        if (buttonIndex == 0)
        {
            DataCaptureVC *dataCapture=[[DataCaptureVC alloc]init];
            [self.navigationController pushViewController:dataCapture animated:NO];
        }
        else if(buttonIndex==1)
        {
            
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
