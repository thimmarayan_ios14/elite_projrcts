//
//  UsersVC.h
//  AppsWorld
//
//  Created by Mac User on 04/05/15.
//
//
#import <UIKit/UIKit.h>
#import "PKCustomKeyboard.h"
#import "DataCaptureVC.h"

@interface  ExistingUserVC : UIViewController <UITextFieldDelegate,DataCaptureDelegate>

@property (nonatomic, strong) PKCustomKeyboard *customKeyboard;

@property (nonatomic,strong) UITextField    *emailTxtfld;

@property (nonatomic,strong) UILabel        *emailLabl;

@property (nonatomic,strong) UIButton       *submitBtn;

@property (nonatomic,strong) DataCaptureVC  *dataCaptureVC;

@property (nonatomic,strong) UIView         *loginView;

@property (nonatomic,strong) UILabel        *welcomeLabl;

@property (nonatomic,strong) UIView         *separatorVW ;

@property (nonatomic,strong) UILabel        *pleaseEnterLabl;

@property (nonatomic,strong) UILabel         *followingLabl;

@property (nonatomic,strong) UIView         *emailLineVW;

@end


