//
//  ScratcherVC.h
//  AppsWorld
//
//  Created by Mac User on 30/04/15.
//
//

#import <UIKit/UIKit.h>
#import "MDScratchImageView.h"

@interface ScratcherVC : UIViewController<MDScratchImageViewDelegate>
{
    IBOutlet MDScratchImageView *yellowScratchView;
    IBOutlet MDScratchImageView *blueScratchView;
    IBOutlet MDScratchImageView *orangeScratchView;
    
    IBOutlet UIImageView *bkgYellowImageView;
    IBOutlet UIImageView *bkgBlueImageView;
    IBOutlet UIImageView *bkgOrangeImageView;

}
@end
