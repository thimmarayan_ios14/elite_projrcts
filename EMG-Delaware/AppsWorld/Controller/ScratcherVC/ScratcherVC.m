//
//  ScratcherVC.m
//  AppsWorld
//
//  Created by Mac User on 30/04/15.
//
//

#import "ScratcherVC.h"
#import "AFNetworking.h"
#import "FXReachability.h"
#import "Define.h"
#import "ScoreVC.h"
#import "MBProgressHUD.h"
#import <unistd.h>

@interface ScratcherVC ()<MBProgressHUDDelegate>
{
    CGFloat yellowScratcherProgressed, blueScratcherProgressed, orangeScratcherProgressed;
    int firstImage;
    int secondImage;
    int thirdImage;
    NSString *resultStr;
    MBProgressHUD *HUD;
    BOOL checkService;
    NSString *scratcherWinner;
    UIButton *statusBtn;
    int count;
    NSTimer *dismissTimer;
}

@end

@implementation ScratcherVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    yellowScratchView.layer.cornerRadius = 0;
    blueScratchView.layer.cornerRadius = 0;
    orangeScratchView.layer.cornerRadius = 0;
    
    [self scratcherGetService];
    
    [yellowScratchView setImage:[UIImage imageNamed:@"SQUARE-YELLOW"] radius:20];
    yellowScratchView.delegate = self;
    [blueScratchView setImage:[UIImage imageNamed:@"SQUARE-BLUE"] radius:20];
    blueScratchView.delegate = self;
    [orangeScratchView setImage:[UIImage imageNamed:@"SQUARE-ORANGE"] radius:20];
    orangeScratchView.delegate = self;
    
//    if ([scratcherWinner isEqualToString:@"100"])
//    {
//        
//    }
//    else if ([scratcherWinner isEqualToString:@"0"])
//    {
//        [self setImagesRandom];
//    }
    
//    [self setRandomNos];
    
    
//    int timeTaken=90-[secondsLbl.text intValue];
//    NSString *totalTimeTaken=[NSString stringWithFormat:@"%.2d",timeTaken];
    

   
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     //[[NSNotificationCenter defaultCenter] postNotificationName:@"Notification_1" object:self];
    
    count=0;
    
    statusBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusBtn.frame =  CGRectMake(0,0,CGRectGetWidth(self.view.frame),50);
    [statusBtn setBackgroundColor:[UIColor clearColor]];
    [statusBtn addTarget:self action:@selector(statusBtnTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:statusBtn];
    checkService=NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    checkService=NO;
    
    [dismissTimer invalidate];       // Stop Timer
    dismissTimer = nil;
}

- (void)setRandomNos{
    
    firstImage = arc4random()%3;
    secondImage = arc4random()%3;
    thirdImage = arc4random()%3;
}

- (void)setImagesBasedOnRandomNos{
    [bkgYellowImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%i",firstImage]]];
    [bkgBlueImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%i",secondImage]]];
    [bkgOrangeImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%i",thirdImage]]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mdScratchImageView:(MDScratchImageView *)scratchImageView didChangeMaskingProgress:(CGFloat)maskingProgress {
    
    switch (scratchImageView.tag) {
        case 1:{
            yellowScratcherProgressed = maskingProgress;
        }
            break;
        case 2:{
            blueScratcherProgressed = maskingProgress;
        }
            break;
        case 3:{
            orangeScratcherProgressed = maskingProgress;
        }
            break;
        default:
            break;
    }
    
    CGFloat average = (yellowScratcherProgressed + blueScratcherProgressed + orangeScratcherProgressed)/3;
    if(average > 0.45){
        if(yellowScratcherProgressed > 0.55 && blueScratcherProgressed > 0.55  && orangeScratcherProgressed > 0.55){
            
            if (checkService==NO)
            {
            [self showFinishPage];
            checkService=YES;
            }
            else if (checkService==YES)
            {
                
            }
        }
    }
    //    }else{
    //        if(!((firstImage == secondImage)&& (secondImage == thirdImage) && (firstImage == thirdImage))){
    //            if(yellowScratcherProgressed > 0.5 || blueScratcherProgressed > 0.5){
    //                [self showFinishPage];
    //            }else if(blueScratcherProgressed > 0.5 || orangeScratcherProgressed > 0.5){
    //                [self showFinishPage];
    //            }else if(yellowScratcherProgressed > 0.5 || orangeScratcherProgressed > 0.5){
    //                [self showFinishPage];
    //            }
    //        }
    //    }
    
}

#pragma mark - Scratcher Method

-(void)scratcherService
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    if([FXReachability isReachable]){
        
        NSDictionary *inputDict = @{@"appId":@"100SC",@"result":resultStr,@"userId":[[NSUserDefaults standardUserDefaults]valueForKey:USERID],@"device":@"iPad"};
        NSLog(@"Scratcher input : %@",inputDict);

        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *urlString=[NSString stringWithFormat:@"%@",GAME_SERVICE];
        [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFormData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                        name:@"input"];
        }
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];

                  NSString *statusStr;
                  
                  NSDictionary *resultDict = (NSDictionary*)responseObject;
                  NSLog(@"Scratcher Game : %@",resultDict);
                  
                  if ([[[resultDict valueForKey:@"code"] stringValue] isEqualToString:@"1000"])
                  {
                      ScoreVC *scoreVC = [[ScoreVC alloc]init];
                      scoreVC.totalSecondsStr=@"--";
                      scoreVC.isScratcher = YES;
                      if ([resultStr isEqualToString:@"0"])
                      {
                          statusStr=@"0";
                      }
                      else
                      {
                          statusStr=@"100";
                      }
                      //scoreVC.totalSecondsStr
                      scoreVC.totalScoreStr=statusStr;
                      scoreVC.gameTypeStr=@"Scratcher";
                      scoreVC.uniqueCodeStr=[[resultDict valueForKey:@"u_code"]stringValue];
                      [self.navigationController pushViewController:scoreVC animated:YES];
                  }
                  else if ([[[resultDict valueForKey:@"code"] stringValue] isEqualToString:@"2000"])
                  {
                      [MBProgressHUD hideHUDForView:self.view animated:YES];

                      //                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Please enter the Valid Details" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                      //                      [alertView show];
                  }
                  
                  
                  
              }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
                  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"There is a problem connecting to the server. Please check your internet connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [alertView show];
              }];
    }
    else{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Seems your internet is offline" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [offlineAlert show];
    }
}

-(void)scratcherGetService
{
    
    if([FXReachability isReachable]){
        
//        [self showLoadingView];

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *urlString=[NSString stringWithFormat:@"%@",SCRATCHER_GETSERVICE];
        
        [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
//            [self hideLoadingView];

            NSDictionary *resultDic = (NSDictionary*)responseObject;
            NSLog(@"%@eventDic",resultDic);
            
            if ([[[resultDic valueForKey:@"scratcherWinner"] stringValue] isEqualToString:@"100"])
            {
                int shouldSelectWinner = [[resultDic valueForKey:@"scratcherWinner"] intValue];
                
                if(shouldSelectWinner == 100){
                    firstImage = arc4random()%3;
                    secondImage = firstImage;
                    thirdImage = firstImage;
                    
                    [self setImagesBasedOnRandomNos];
                    
                }else{
                    do {
                        [self setRandomNos];
                    } while ([self checkIfImagesAreSame]);
                    
                    [self setImagesBasedOnRandomNos];
                }
            }
            else
            {
                [self setRandomNos];
                [self setImagesBasedOnRandomNos];
            }
            
            
            
        }
failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [self hideLoadingView];
                 NSLog(@"Error scratcherGetService  : %@", error);
             }];
    }
    
    else{
        
//        [self hideLoadingView];
        UIAlertView *offlineAlert = [[UIAlertView alloc]initWithTitle:ALERTTITLE message:@"Seems your internet is offline." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [offlineAlert show];
    }
}

- (BOOL)checkIfImagesAreSame{
    
    if((firstImage == secondImage)&& (secondImage == thirdImage) && (firstImage == thirdImage)){
        return YES;
    }
    return NO;
}

- (void)showFinishPage{
    
    if([self checkIfImagesAreSame]){
        resultStr=@"100";
        [self scratcherService];
    }else{
        resultStr=@"0";
        [self scratcherService];
    }
}

#pragma mark-
#pragma mark - Hide Loading View

- (void)myTask
{
    // Do something usefull in here instead of sleeping ...
}

//-(void)showLoadingView
//{
//    HUD = [[MBProgressHUD alloc] initWithFrame:CGRectMake(360,320,250,200)];
//    [self.navigationController.view addSubview:HUD];
//    HUD.color = [UIColor blackColor];
//    HUD.delegate = self;
//    HUD.labelText = @"Loading...";
//    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
//    [self.view addSubview:HUD];
//}

-(void)hideLoadingView
{
    [HUD hide:YES];
}

-(BOOL)prefersStatusBarHidden
{
    return  YES ;
}

#pragma mark-
#pragma mark - Status button Action

-(void)statusBtnTap:(id)sender
{
    count++;
    
    if (count==3)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    dismissTimer= [NSTimer scheduledTimerWithTimeInterval:5.0
                                                   target:self
                                                 selector:@selector(reduceCount)
                                                 userInfo:nil
                                                  repeats:NO];
    
}

-(void)reduceCount
{
    count=0;
}

@end
