//
//  TDTileView.h
//  MemoryTiles
//
//  Created by Purush V on 25/01/13.
//  Copyright (c) 2013 Purush V. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioPlayer.h"

@protocol TDTileViewDelegate;

@protocol TDTileViewDelegate <NSObject>

- (void)tileTouched:(NSUInteger)totoalTouches;
- (void)numberOfTilesPaired:(NSUInteger)score;

@end

@interface TDTileView : UIView {
    
    // Properties declaration
    
    NSUInteger      numberOfColumns;
    NSUInteger      numberOfRows;
    NSUInteger      tileWidth;
    NSUInteger      tileHeight;
    NSUInteger      spaceBetweenTiles;
    
    // SubViews container arrays
    
    NSMutableArray  *tilesSubViewsAry;
    NSMutableArray  *tilesSubViewsDuplicateAry;
    NSMutableArray  *openedTilesAry;
    NSMutableArray  *tappedTilesAry;
    NSArray         *tilesIconImgAry;
    
    // Timer variable
    
    NSTimer         *tiesOpenTimer;
    NSTimer         *tiesCloseTimer;
    
    // Audio player
    
    AudioPlayer     *sound;
    
    // Delegate Method properties
    
    int             numberOfTouches;
    int             numberOfPairedTiles;
    
    BOOL            last4Enabled;
    NSMutableArray  *pairedTilesHistory;
    
    BOOL            pauseTimer;
    
@private
    
    NSDictionary    *employeeDict;
    NSMutableArray  *noRepetationArray;
    NSString        *employee1;
    NSString        *employee2;
    NSString        *employee3;
    NSString        *employee4;
    NSString        *employee5;
    NSString        *employee6;
    NSString        *employee7;
    NSString        *employee8;
    NSString        *employee9;
    NSString      *employee10;
    
}
@property(nonatomic, assign) NSUInteger numberOfColumns;
@property(nonatomic, assign) NSUInteger numberOfRows;
@property(nonatomic, assign) NSUInteger tileWidth;
@property(nonatomic, assign) NSUInteger tileHeight;
@property(nonatomic, assign) NSUInteger spaceBetweenTiles;
@property(nonatomic, strong) id<TDTileViewDelegate> delegate;
@property(nonatomic, strong) NSMutableArray *tilesSubViewsAry;

// Setter properties

- (void)setNumberOfColumns:(NSUInteger)numbers;
- (void)setNumberOfRows:(NSUInteger)numbers;
- (void)setTileHeight:(NSUInteger)height;
- (void)setTileWidth:(NSUInteger)width;
- (void)setSpaceBetweenTiles:(NSUInteger)space;

// drawing tiles according to the columns and rows

- (void)drawTDTileView;
- (void)shuffleTiles;
- (void)resetImage;

// Flipping Methods

- (void)flip:(UIImageView *)view isCalledByTapping:(BOOL)isCalledByTapping;

@end
