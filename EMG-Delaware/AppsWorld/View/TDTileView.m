//
//  TDTileView.m
//  MemoryTiles
//
//  Created by Purush V on 25/01/13.
//  Copyright (c) 2013 Purush V. All rights reserved.
//

#import "TDTileView.h"

#define NUMBEROFTILESCANOPEN 4
#define GETUSER @"http://emgcgs.smartletmanager.com/getuser"

@interface TDTileView ()

@end

@implementation TDTileView
@synthesize numberOfRows, numberOfColumns, tileWidth, tileHeight,
spaceBetweenTiles, delegate, tilesSubViewsAry;

#pragma mark - Return Random Employee Details

- (void)getEmployeesFromServerWithBlock:(void (^)(BOOL flag))completionHandler {
    
    NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] init];
    [mutableRequest setURL:[NSURL URLWithString:GETUSER]];
    [mutableRequest setHTTPMethod:@"GET"];
    // [mutableRequest setHTTPBody:postData];
    
    [NSURLConnection
     sendAsynchronousRequest:mutableRequest
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response, NSData *resData,
                         NSError *error) {
         
         employeeDict = (NSMutableDictionary *)
         [NSJSONSerialization JSONObjectWithData:resData
                                         options:0
                                           error:nil];
         NSLog(@"Response 1 ============================== %@",
               employeeDict);
         if (employeeDict != nil || resData != nil) {
             
             if ([[employeeDict objectForKey:@"userlist"] count] == 8) {
                 // [self stopAnimation];
                 /*NSLog(@"Response ==============================
                  %@",responseData);
                  
                  for (NSDictionary *dict in responseData) {
                  NSString *str=[dict valueForKey:@"userlist"];
                  NSLog(@"str ============================== %@",str);
                  }*/
                 
                 //                 NSString *  userIdStr=[responseData
                 //                 objectForKey:@"userlist"];
                 BOOL flag = YES;
                 completionHandler(flag);
                 
                 //  NSLog(@"userIdStr ============================== %@",dic);
                 
                 //  signUpView.frame = CGRectMake(1024, 0, 1024, 748);
                 
                 //                 [etActivity stopAnimating];
                 //                 [self loginView];
                 //
             }
         }
     }];
}

/*-(NSString *)getEmployeeDetailsForArray : (NSArray *)array {
 
 
 //    NSDictionary * dataCaptureDict = @{ @"userid" :[DefaultsManager
 readUserID],
 //                                        @"tmoves":
 result_moves_val.text,@"score" :result_match_val.text,
 //                                        @"timetaken":result_time_val.text};
 
 //NSLog(@"data capture dict %@",dataCaptureDict);
 
 //    NSData *jsonData; NSError *error;
 //
 //    jsonData = [NSJSONSerialization dataWithJSONObject:Nil
 options:NSJSONWritingPrettyPrinted error:&error];
 //
 //    NSString *jsonString=[[NSString alloc]initWithData:jsonData
 encoding:NSUTF8StringEncoding];
 //    NSString *postString;
 //    postString = [NSString stringWithFormat:@"input=%@",jsonString];
 //    NSLog(@"Post String : %@",postString);
 //
 //    NSMutableData *postData = [NSMutableData dataWithData:[postString
 dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
 
 //    int index = arc4random() % array.count;
 //    [noRepetationArray removeObjectAtIndex:index];
 }*/

- (void)setup {
    
    //  [self getEmployeesFromServerWithBlock:^(BOOL flag) {
    
    // if (flag) {
//    NSLog(@"employeeDict %@", employeeDict);
    
    employee1 = [NSString stringWithFormat:@"%@\n%@",@"MICHAEL RILEY",@"VISA"];
    
    
    employee2 = [NSString stringWithFormat:@"%@\n%@",@"BALA GURU",@"EMG"];
    
    //  employee2 = [NSString stringWithFormat:@"%@\n%@", employeeDict[@"userlist"][1][@"user_name"],employeeDict[@"userlist"][1][@"companyname"]];
    
    employee3 = [NSString stringWithFormat:@"%@\n%@",@"ANTHONY TAVERAS",@"ELITE MARKETING GROUP"];
    
    //  employee3 = [NSString stringWithFormat:@"%@\n%@",employeeDict[@"userlist"][2][@"user_name"],employeeDict[@"userlist"][2][@"companyname"]];
    
    
    employee4 = [NSString stringWithFormat:@"%@\n%@",@"ISAAC CHARLES",@"TEST"];
    
    //  employee4 = [NSString stringWithFormat:@"%@\n%@",employeeDict[@"userlist"][3][@"user_name"],employeeDict[@"userlist"][3][@"companyname"]];
    
    
    employee5 = [NSString stringWithFormat:@"%@\n%@",@"KAREN BAKER",@"SYNAPSE GROUP,INC"];
    
    //employee5 = [NSString stringWithFormat:@"%@\n%@",employeeDict[@"userlist"][4][@"user_name"],employeeDict[@"userlist"][4][@"companyname"]];
    
    
    employee6 = [NSString stringWithFormat:@"%@\n%@",@"MICHAEL ZIEG",@"MASTERCARD"];
    
    //
    // employee6 = [NSString stringWithFormat:@"%@\n%@",employeeDict[@"userlist"][5][@"user_name"],employeeDict[@"userlist"][5][@"companyname"]];
    
    employee7 = [NSString stringWithFormat:@"%@\n%@",@"WW EE",@"DDDD"];
    
    //employee7 = [NSString stringWithFormat:@"%@\n%@",employeeDict[@"userlist"][6][@"user_name"],employeeDict[@"userlist"][6][@"companyname"]];
    
    employee8 = [NSString stringWithFormat:@"%@\n%@",@"DAVID T",@"MOBILITY MINDS"];
    
    //employee8 = [NSStrin stringWithFormat:@"%@\n%@",employeeDict[@"userlist"][7][@"user_name"],employeeDict[@"userlist"][7][@"companyname"]];
    
    employee9 = [NSString stringWithFormat:@"%@\n%@",@"TEST",@"MM"];
    
    
    employee10 = [NSString stringWithFormat:@"%@\n%@",@"EMP10",@"MM"];
    
    
    // }
    // }];
    
    /* NSString *file = [[NSBundle mainBundle] pathForResource:@"Employee"
     ofType:@"plist"];
     noRepetationArray = [NSMutableArray arrayWithContentsOfFile:file];
     employee1 = [self getEmployeeDetailsForArray:noRepetationArray];
     employee2 = [self getEmployeeDetailsForArray:noRepetationArray];
     employee3 = [self getEmployeeDetailsForArray:noRepetationArray];
     employee4 = [self getEmployeeDetailsForArray:noRepetationArray];
     employee5 = [self getEmployeeDetailsForArray:noRepetationArray];
     employee6 = [self getEmployeeDetailsForArray:noRepetationArray];
     employee7 = [self getEmployeeDetailsForArray:noRepetationArray];
     employee8 = [self getEmployeeDetailsForArray:noRepetationArray]; */
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setup];
        tappedTilesAry = [[NSMutableArray alloc] init];
        sound = [[AudioPlayer alloc] init];
    }
    
    return self;
}
#pragma mark -
#pragma mark Setter methodsk
// Setters
- (void)setNumberOfColumns:(NSUInteger)numbers {
    numberOfColumns = numbers;
}
- (void)setNumberOfRows:(NSUInteger)numbers;
{ numberOfRows = numbers; }
- (void)setTileHeight:(NSUInteger)height {
    tileHeight = height;
}
- (void)setTileWidth:(NSUInteger)width {
    tileWidth = width;
}
- (void)setSpaceBetweenTiles:(NSUInteger)space {
    spaceBetweenTiles = space;
}
- (NSMutableArray *)tilesSubViewsAry {
    return tilesSubViewsAry;
}
#pragma mark -
#pragma mark Drawing tiles
// Tiles one time creation
- (void)drawTDTileView {
    
    int tileTag = 0;
    float tileY = 0;
    tilesSubViewsAry = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < numberOfRows; i++) {
        
        float tileX = 0;
        
        for (int j = 0; j < numberOfColumns; j++) {
            
            UIImageView *tileImageVw = [[UIImageView alloc]
                                        initWithFrame:CGRectMake(tileX, tileY, tileWidth, tileHeight)];
            tileImageVw.backgroundColor = [UIColor whiteColor];
            tileImageVw.layer.cornerRadius = 10;
            tileImageVw.image = [UIImage imageNamed:@"blank"];
            //imageNamed:[NSString stringWithFormat:@"boxx%d", tileTag + 1]];
            ;
            tileImageVw.tag = tileTag;
            [self addSubview:tileImageVw];
            tileImageVw.userInteractionEnabled = NO;
            tileX = tileImageVw.frame.origin.x + tileWidth + spaceBetweenTiles;
            
            UITapGestureRecognizer *singleTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(handleTap:)];
            [singleTap setNumberOfTapsRequired:1];
            [singleTap setNumberOfTouchesRequired:1];
            [tileImageVw addGestureRecognizer:singleTap];
            [tilesSubViewsAry addObject:tileImageVw];
            tileTag++;
        }
        
        tileY = tileY + tileHeight + spaceBetweenTiles;
    }
}
// After completion of game if user wish to play again means we can call this
// method to re set
- (void)resetImage {
    
    for (UIImageView *tiles in tilesSubViewsAry) {
        if (tiles.tag >= 100) {
            
            tiles.userInteractionEnabled = YES;
            [self flip:tiles isCalledByTapping:NO];
        }
    }
}
// This method will be called when there is shuffling of views need
- (void)shuffleTiles {
    
    for (int i = 0; i < (numberOfColumns * numberOfRows); i++) {
        
        UIImageView *tileVw = [tilesSubViewsAry objectAtIndex:i];
        tileVw.userInteractionEnabled = YES;
        UIImageView *shuffleVw = [tilesSubViewsAry objectAtIndex:arc4random() % 16];
        CGRect tempframe = tileVw.frame;
        UIImage *tempimg = tileVw.image;
        
        // Shuffling animation
        // [UIView beginAnimations:@"MoveView" context:nil];
        //  [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        // [UIView setAnimationDuration:0.7f];
        tileVw.frame = shuffleVw.frame;
        tileVw.image = shuffleVw.image;
        // [UIView commitAnimations];
        
        // [UIView beginAnimations:@"MoveView" context:nil];
        //  [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        // [UIView setAnimationDuration:0.7f];
        shuffleVw.frame = tempframe;
        shuffleVw.image = tempimg;
        //  [UIView commitAnimations];
    }
    numberOfTouches = 0;
    numberOfPairedTiles = 0;
    // tilesSubViewsDuplicateAry will maintain random flipping objects
    // tilesSubViewsAry will contain all view objects
    
    tilesSubViewsDuplicateAry = [tilesSubViewsAry mutableCopy];
    openedTilesAry = [[NSMutableArray alloc] init];
    pairedTilesHistory = [[NSMutableArray alloc] init];
    //    [self performSelector:@selector(tilesOpenStart) withObject:nil
    //    afterDelay:1.5];
}
- (void)tilesOpenStart {
    
    // Start up random flipping
    if ([tilesSubViewsDuplicateAry count] > 0) {
        
        if ([tiesOpenTimer isValid]) {
            [tiesOpenTimer invalidate];
            tiesOpenTimer = nil;
        }
        tiesOpenTimer =
        [NSTimer scheduledTimerWithTimeInterval:1
                                         target:self
                                       selector:@selector(tilesOpenRandomly)
                                       userInfo:nil
                                        repeats:YES];
        [tiesOpenTimer fire];
    }
}
- (void)tilesOpenRandomly {
    
    // below loop is used to check whether any paired object is in closed state
    for (UIImageView *view in pairedTilesHistory) {
        
        // Below loop will execute if pair objects available in random flipping
        // array
        // It must needed some time there is some paired objects dublicate may
        // available in random flipping array
        if ([tilesSubViewsDuplicateAry containsObject:view]) {
            
            [tilesSubViewsDuplicateAry removeObject:view];
            
        } else if ([openedTilesAry containsObject:view]) {
            [openedTilesAry removeObject:view];
        }
        
        if (view.tag <= (numberOfColumns * numberOfRows)) {
            
            // If it is in closed state then it will flip into open state
            [self flip:view isCalledByTapping:YES];
        }
    }
    
    // If pauseTimer == NO means then random flip will not execute
    if (!pauseTimer) {
        
        // We need to check whether last4Enabled or not when
        // [tilesSubViewsDuplicateAry count] == 0 then obviously there is only 4
        // obejcts
        if ([tilesSubViewsDuplicateAry count] > 0) {
            
            // random no generation
            int randomIndex = arc4random() % [tilesSubViewsDuplicateAry count];
            [self flip:[tilesSubViewsDuplicateAry objectAtIndex:randomIndex]
     isCalledByTapping:NO];
            // After fliping that view will be added into openedTilesAry
            [openedTilesAry
             addObject:[tilesSubViewsDuplicateAry objectAtIndex:randomIndex]];
            // Once one number generated then it will remove from
            // tilesSubViewsDuplicateAry to avoid repetation
            [tilesSubViewsDuplicateAry removeObjectAtIndex:randomIndex];
            // below loop will execute when openedTilesAry count]>NUMBEROFTILESCANOPEN
            if ([openedTilesAry count] > NUMBEROFTILESCANOPEN) {
                
                // when there is [openedTilesAry count] more than NUMBEROFTILESCANOPEN
                // means we can close the view
                [self flip:[openedTilesAry objectAtIndex:0] isCalledByTapping:NO];
                // After fliping that view will be added into tilesSubViewsDuplicateAry
                // for conitues flipping
                [tilesSubViewsDuplicateAry addObject:[openedTilesAry objectAtIndex:0]];
                [openedTilesAry removeObjectAtIndex:0];
            }
            
        } else {
            
            // this will execute when there is only 4 objects available
            last4Enabled = YES;
            
            if ([tiesCloseTimer isValid]) {
                [tiesCloseTimer invalidate];
                tiesCloseTimer = nil;
            }
            // tiesCloseTimer starting
            tiesCloseTimer =
            [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(tilesCloseRandomly)
                                           userInfo:nil
                                            repeats:YES];
            [tiesCloseTimer fire];
        }
    }
}
- (void)tilesCloseRandomly {
    
    // When there is 0 objects in openedTilesAry means all the objects are paired
    // so we can stop the timer
    if ([openedTilesAry count] > 0) {
        
        // below code is used to avoid the repetation of random number
        srand(time(NULL));
        int s[[openedTilesAry count]];
        BOOL fl = 1;
        for (int i = 0; i < [openedTilesAry count]; i++) {
            while (fl) {
                s[i] = rand() % [openedTilesAry count];
                [self flip:[openedTilesAry objectAtIndex:s[i]] isCalledByTapping:NO];
                
                fl = 0;
                for (int j = 0; j < i; j++) {
                    if (s[j] == s[i]) {
                        fl = 1;
                        j = i + 1;
                    }
                }
            }
        }
        
    } else {
        
        [tiesCloseTimer invalidate];
        tiesCloseTimer = nil;
    }
}

#pragma mark Flipping Methods

- (void)flip:(UIImageView *)view isCalledByTapping:(BOOL)isCalledByTapping {
    // If view.backgroundColor == [UIColor whiteColor] means view is in closed
    // state
    if (view.backgroundColor == [UIColor whiteColor]) {
        
        view.backgroundColor = [UIColor clearColor];
        
        if (view.tag == 0 || view.tag == 9) {
            view.image = [UIImage imageNamed:@"BEACHES"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
            
        } else if (view.tag == 1 || view.tag == 10) {
            view.image = [UIImage imageNamed:@"BEER"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
            
        } else if (view.tag == 2 || view.tag == 11) {
            view.image = [UIImage imageNamed:@"BLUE-HEN"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
            
        } else if (view.tag == 3 || view.tag == 12) {
            view.image = [UIImage imageNamed:@"DELAWARE"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
            
        } else if (view.tag == 4 || view.tag == 13) {
            view.image = [UIImage imageNamed:@"DOVER-RACING"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
            
        } else if (view.tag == 5 || view.tag == 14) {
            view.image = [UIImage imageNamed:@"FIRST-STATE"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
            
        } else if (view.tag == 6 || view.tag == 15) {
            view.image = [UIImage imageNamed:@"LIGHTHOUSE"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
            
        } else if (view.tag == 7 || view.tag == 16) {
            view.image = [UIImage imageNamed:@"TAX-FREE"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
        }
        else if (view.tag == 8 || view.tag == 17){
            view.image = [UIImage imageNamed:@"WILMINGTON"];
            view.image = [TDTileView drawText:@"" onImage:view.image];
        }
    }
    else {
        // This will execute when flipping view is open to close
        // We checked below by frames that is to bring glass effect when objects are
        // in closed state
        view.backgroundColor = [UIColor whiteColor];
        
        // 1st column-1 and 1st row-1
        if (view.frame.origin.x == 0 && view.frame.origin.y == 0) {
            
            view.image = [UIImage imageNamed:@"blank@2x"];
        }
        // 2nd column-1 and 1st row-2
        else if (view.frame.origin.x == 1 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 0) {
            view.image = [UIImage imageNamed:@"blank@2x"];
        }
        // 3rd column-1 and 1st row-3
        else if (view.frame.origin.x == 2 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 0) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 4th column-1 and 1st row-4
        else if (view.frame.origin.x == 3 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 0) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 5th column -1 and 1st row-5
        
        else if (view.frame.origin.x == 4 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 0) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 6th column-1 and 1st row-6
        
        else if (view.frame.origin.x == 5 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 0) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        /* ROW 2 */
        
        // 1st column-2 and 2nd row-1
        
        else if (view.frame.origin.x == 0 &&
                 view.frame.origin.y == 1 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
        }
        
        // 2nd column-2 and 2nd row-2
        else if (view.frame.origin.x == 1 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 1 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 3rd column-2 and 2nd row-3
        
        else if (view.frame.origin.x == 2 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 1 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 4th column-2 and 2nd row-4
        
        else if (view.frame.origin.x == 3 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 1 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 5th column-2 and 2nd row-5
        
        else if (view.frame.origin.x == 4 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 1 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 6th column-2 and 2nd row-6
        
        else if (view.frame.origin.x == 5 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 1 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        /* ROW 3 */
        
        // 1st column-3 and 3rd row-1
        else if (view.frame.origin.x == 0 &&
                 view.frame.origin.y == 2 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 2nd column-3 and 3rd row-2
        
        else if (view.frame.origin.x == 1 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 2 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 3rd column-3 and 3rd row-3
        else if (view.frame.origin.x == 2 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 2 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
            
        }
        // 4th column-3 and 3rd row-4
        
        else if (view.frame.origin.x == 3 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 2 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
        }
        
        // 5th column-3 and 3rd row-5
        else if (view.frame.origin.x == 4 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 2 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
        }
        
        // 6th column-3 and 3rd row-6
        
        else if (view.frame.origin.x == 5 * (self.tileWidth + spaceBetweenTiles) &&
                 view.frame.origin.y == 2 * (self.tileHeight + spaceBetweenTiles)) {
            view.image = [UIImage imageNamed:@"blank@2x"];
        }
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.8];
    if (view.tag > 99) {
        view.tag = view.tag - 100;
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                               forView:view
                                 cache:YES];
        
    } else {
        view.tag = view.tag + 100;
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                               forView:view
                                 cache:YES];
    }
    
    [UIView commitAnimations];
    
    // there is two two chance to call flip method one is by tapping and another
    // one is random flip
    // below loop will execute calling flip method by tapping
    if (isCalledByTapping) {
        
        [self performSelector:@selector(checkingPair)
                   withObject:nil
                   afterDelay:0.8];
    }
}
- (void)checkingPair {
    // pauseTimer is use to stop the tiesOpenTimer timer
    pauseTimer = YES;
    
    // We must allow to check the pair when there is only two objects in
    // tappedTilesAry if not it create conflict
    if ([tappedTilesAry count] == 2) {
        
        // Last tapped view
        UIImageView *tappedVw = [tappedTilesAry objectAtIndex:0];
        // Previous tapped view
        UIImageView *pairVw = [tappedTilesAry objectAtIndex:1];
        int tappedViewTag = tappedVw.tag;
        int pairViewTag = pairVw.tag;
        // To be a pair below are the possiblities
        // for example let assume tappedVw tag = 0 and pairVw tag = 8 (when both are
        // open)
        // OR tappedVw tag = 100 and pairVw tag = 108 (when both are closed)
        // OR tappedVw tag = 0 and pairVw tag = 108 (when one open and  anothe
        // closed)
        // OR tappedVw tag = 100 and pairVw tag = 8 (when one closed and  anothe
        // open)
        if ((tappedViewTag ==
             (pairViewTag + (numberOfColumns * numberOfRows) / 2)) ||
            (tappedViewTag ==
             (pairViewTag - (numberOfColumns * numberOfRows) / 2)) ||
            (tappedViewTag ==
             (pairViewTag + (numberOfColumns * numberOfRows) / 2) - 100) ||
            (tappedViewTag ==
             (pairViewTag - (numberOfColumns * numberOfRows) / 2) + 100)) {
                // this method will execute only when the 2 objects are successfull pair
                // this numberOfPairedTiles indeger value get increment when user paired
                // successfully
                numberOfPairedTiles++;
                [self performSelector:@selector(callNumberOfTilesPaired)
                           withObject:nil
                           afterDelay:0.5];
                [self performSelector:@selector(playPairedSound)
                           withObject:nil
                           afterDelay:0.35];
                // Once the 2 objects are successful pair then it will be added
                // pairedTilesHistory array
                [pairedTilesHistory addObject:tappedVw];
                [pairedTilesHistory addObject:pairVw];
                
                // Below loop will execute if pair objects available in random flipping
                // array
                // It must needed some time there is some paired objects dublicate may
                // available in random flipping array
                if ([tilesSubViewsDuplicateAry containsObject:tappedVw]) {
                    
                    [tilesSubViewsDuplicateAry removeObject:tappedVw];
                    
                } else if ([openedTilesAry containsObject:tappedVw]) {
                    [openedTilesAry removeObject:tappedVw];
                }
                if ([tilesSubViewsDuplicateAry containsObject:pairVw]) {
                    
                    [tilesSubViewsDuplicateAry removeObject:pairVw];
                    
                } else if ([openedTilesAry containsObject:pairVw]) {
                    [openedTilesAry removeObject:pairVw];
                }
                
                // We can stop the tiesOpenTimer when 8 objects paired
                if (numberOfPairedTiles == 8) {
                    
                    [tiesOpenTimer invalidate];
                    tiesOpenTimer = nil;
                }
                
            } else {
                
                // last4Enabled will use check whether only 4 objects available or more
                if (!last4Enabled) {
                    
                    // if more than 4 objects available mean we can add it to
                    // tilesSubViewsDuplicateAry
                    [tilesSubViewsDuplicateAry addObject:tappedVw];
                    [tilesSubViewsDuplicateAry addObject:pairVw];
                    
                } else {
                    // if less than 4 or equal objects available mean we can add it to
                    // openedTilesAry
                    
                    [openedTilesAry addObject:tappedVw];
                    [openedTilesAry addObject:pairVw];
                }
                
                [self performSelector:@selector(shakeView:)
                           withObject:tappedVw
                           afterDelay:0.35];
                [self performSelector:@selector(shakeView:)
                           withObject:pairVw
                           afterDelay:0.35];
            }
        // Once the pair checking over means we can empty tappedTilesAry
        [tappedTilesAry removeAllObjects];
    }
    // pauseTimer is use to stop the tiesOpenTimer timer
    pauseTimer = NO;
}
// This method will be called when there 2 obejects paired successfully
- (void)callNumberOfTilesPaired {
    // To increase the number of paired objects in label this delegate method will
    // be called
    [delegate numberOfTilesPaired:numberOfPairedTiles];
}

#pragma mark -
#pragma mark Play Sounds method
// If selected pair is wrong, then this method will execute and this will shake
// the pair
- (void)shakeView:(UIImageView *)viewToShake {
    
    [sound playSoundWithFile:@"Pair_wrong_1"];
    CGFloat t = 4.0;
    CGAffineTransform translateRight =
    CGAffineTransformTranslate(CGAffineTransformIdentity, t, 0.0);
    CGAffineTransform translateLeft =
    CGAffineTransformTranslate(CGAffineTransformIdentity, -t, 0.0);
    
    viewToShake.transform = translateLeft;
    
    [UIView animateWithDuration:0.02
                          delay:0.0
                        options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat
                     animations:^{
                         [UIView setAnimationRepeatCount:4.0];
                         viewToShake.transform = translateRight;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [UIView animateWithDuration:0.02
                                                   delay:0.0
                                                 options:UIViewAnimationOptionBeginFromCurrentState
                                              animations:^{
                                                  viewToShake.transform = CGAffineTransformIdentity;
                                                  [self flip:viewToShake isCalledByTapping:NO];
                                              }
                                              completion:NULL];
                         }
                     }];
}
#pragma mark -
#pragma mark Play Sounds method
// If selected pair is corrcet, then this method will execute
- (void)playPairedSound {
    [sound playSoundWithFile:@"PairCorrect"];
}
#pragma mark - Gesture recognizer method

- (void)handleTap:(UIPanGestureRecognizer *)recognizer {
    UIImageView *tappedVw = (UIImageView *)[recognizer view];
    
    numberOfTouches++;
    [delegate tileTouched:numberOfTouches];
    
    // pauseTimer is use to stop the tiesOpenTimer timer
    pauseTimer = YES;
    
    // pairedTilesHistory contains all objects which are paired.
    // We dont allow  call checkingPair if tappedVw is already paired but number
    // of moves will be get increment
    if (![pairedTilesHistory containsObject:tappedVw]) {
        
        // We don't allow to add objects in tappedTilesAry before previous pair
        // checking completion
        if ([tappedTilesAry count] < 2) {
            
            // if tilesSubViewsDuplicateAry = = 0 then there will be only 4 objects
            // remaining which all will be stored in openedTilesAry
            
            if ([tilesSubViewsDuplicateAry count] > 0) {
                
                // tappedVw.tag<= (numberOfColumns * numberOfRows means the selected
                // view is in closed state and also all the closed views will be
                // available in tilesSubViewsDuplicateAry
                if (tappedVw.tag <= (numberOfColumns * numberOfRows)) {
                    
                    // To stop the tappedVw from random flipping it will be removed from
                    // tilesSubViewsDuplicateAry once if checkingPair completed then it
                    // will be added in tilesSubViewsDuplicateAry for random flipping
                    [tilesSubViewsDuplicateAry removeObject:tappedVw];
                    [sound playSoundWithFile:@"Tiletap_button_click"];
                    // tappedTilesAry will contains last tapped 2 objects
                    [tappedTilesAry addObject:recognizer.view];
                    // if tappedVw is in closed state the first will send for flipping
                    // from there it will go for checkingPair
                    [self flip:tappedVw isCalledByTapping:YES];
                    
                } else {
                    
                    // To stop the tappedVw from random flipping it will be removed from
                    // openedTilesAry once if checkingPair completed then it will be added
                    // in openedTilesAry for random flipping
                    [openedTilesAry removeObject:tappedVw];
                    [sound playSoundWithFile:@"Tiletap_button_click"];
                    // tappedTilesAry will contains last tapped 2 objects
                    [tappedTilesAry addObject:recognizer.view];
                    // if tappedVw is in opened state then directly it will go for
                    // checkingPair, no need of flipping
                    [self checkingPair];
                }
                
            } else {
                
                // This will execute when 12 objects paired. there will be only 4
                // objects without remaiming
                
                // tappedVw.tag<= (numberOfColumns * numberOfRows means the selected
                // view is in closed state and also all the closed views will be
                // available in tilesSubViewsDuplicateAry
                if (tappedVw.tag <= (numberOfColumns * numberOfRows)) {
                    
                    // To stop the tappedVw from random flipping it will be removed from
                    // openedTilesAry once if checkingPair completed then it will be added
                    // in openedTilesAry for random flipping
                    [openedTilesAry removeObject:tappedVw];
                    [sound playSoundWithFile:@"Tiletap_button_click"];
                    // tappedTilesAry will contains last tapped 2 objects
                    [tappedTilesAry addObject:recognizer.view];
                    // if tappedVw is in closed state the first will send for flipping
                    // from there it will go for checkingPair
                    [self flip:tappedVw isCalledByTapping:YES];
                    
                } else {
                    
                    // To stop the tappedVw from random flipping it will be removed from
                    // openedTilesAry once if checkingPair completed then it will be added
                    // in openedTilesAry for random flipping
                    [openedTilesAry removeObject:tappedVw];
                    [sound playSoundWithFile:@"Tiletap_button_click"];
                    // tappedTilesAry will contains last tapped 2 objects
                    [tappedTilesAry addObject:recognizer.view];
                    // if tappedVw is in opened state then directly it will go for
                    // checkingPair, no need of flipping
                    [self checkingPair];
                }
            }
        }
    }
    
    // pauseTimer is use to stop the tiesOpenTimer timer
    pauseTimer = NO;
}
//+(UIImage*) drawText:(NSString*) text
//             inImage:(UIImage*)  image
//             atPoint:(CGPoint)   point
//{
//
//    UIFont *font = [UIFont boldSystemFontOfSize:30];
//    UIGraphicsBeginImageContext(image.size);
//    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
//    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
//    [[UIColor whiteColor] set];
//    [text drawInRect:CGRectIntegral(rect) withFont:font
//    lineBreakMode:NSLineBreakByClipping alignment:NSTextAlignmentCenter];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    return newImage;
//}

/*+ (UIImage *)drawText: (NSString *)text  onImage:(UIImage *)image  {
 
 UIGraphicsBeginImageContext(image.size);
 [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
 UIFont *font = [UIFont boldSystemFontOfSize:14];
 
 
 CGRect textFrame = (CGRect)
 {
 .size.width = image.size.width,
 .size.height = image.size.height,
 };
 
 //    CGSize textSize = [text sizeWithFont:font
 constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT)
 lineBreakMode:NSLineBreakByClipping];
 //    CGRect newTextFrame = CGRectInset(textFrame, 0, (textFrame.size.height -
 textSize.height) / 2);
 
 CGRect newTextFrame = CGRectInset(textFrame, 10, 10);
 
 [[UIColor whiteColor] set];
 
 [text drawInRect:newTextFrame withFont:font
 lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
 UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
 
 UIGraphicsEndImageContext();
 return newImage;
 
 }*/

+ (UIImage *)drawText:(NSString *)text onImage:(UIImage *)image {
    
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    UILabel *label = [[UILabel alloc]
                      initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [label setText:text];
    [label setFont:[UIFont boldSystemFontOfSize:15]];
    [label setNumberOfLines:0];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    //    [label drawTextInRect:label.frame];
    
    [label.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return newImage;
}

@end
