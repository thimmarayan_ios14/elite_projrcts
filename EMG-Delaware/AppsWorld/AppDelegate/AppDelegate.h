//
//  AppDelegate.h
//  AppsWorld
//
//  Created by Mac User on 28/04/15.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic)UINavigationController *navigationController;

@end

