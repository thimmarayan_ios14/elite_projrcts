//
//  Define.h
//  SSLC
//
//  Created by Kadirselvi Venkatesan on 30/09/14.
//
//

/*------------------------- Set RGB color ----------------------*/

#define RGBA(r,g,b,a) [UIColor colorWithRed:((r)/255.0) green:((g)/255.0) blue:((b)/255.0) alpha:(a)]

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0f]

/*------------------------- Font ----------------------------- */

#define HELVETICA_ULTRALIGHT @"HelveticaNeue-UltraLight"
#define HELVETICA_THIN @"HelveticaNeue-Thin"
#define HELVETICA_BOLD @"Helvetica-Bold"
#define HELVETICA_LIGHT @"HelveticaNeue-Light"
#define HELVETICA_MEDIUM @"HelveticaNeue-Medium"
#define HELVETICA_REGULAR @"HelveticaNeue"

/* ----------------------- Alert Title ----------------------*/

#define ALERTTITLE @"EMG-Delaware"

/*----------------------- USer Default ----------------------*/

#define USERID @"USERID"
#define MEMORYGAMESTATUS @"MEMORYGAMESTATUS"
#define SCRATCHGAMESSTATUS @"SCRATCHGAMESSTATUS"
#define SPOTDIFFGAMESTATUS @"SPOTDIFFGAMESTATUS"

/*------------------------ WERSERVICE -------------------------*/

//#define LOGIN_SERVICE @"http://esra.elitemg.com/AppsWorld/login"
//
//#define REGISTER_SERVICE @"http://esra.elitemg.com/AppsWorld/register"
//
//#define GAME_SERVICE @"http://esra.elitemg.com/AppsWorld/result"
//
//#define SCRATCHER_GETSERVICE @"http://esra.elitemg.com/AppsWorld/scw"


#define LOGIN_SERVICE @"http://esra.elitemg.com/EMG-Delaware/login"

#define REGISTER_SERVICE @"http://esra.elitemg.com/EMG-Delaware/register"

#define GAME_SERVICE @"http://esra.elitemg.com/EMG-Delaware/result"

#define SCRATCHER_GETSERVICE @"http://esra.elitemg.com/EMG-Delaware/scw"





//#define LOGIN_SERVICE @"http://esra.elitemg.com/AppWorld/public/login"
//
//#define REGISTER_SERVICE @"http://esra.elitemg.com/AppWorld/public/register"
//
//#define GAME_SERVICE @"http://esra.elitemg.com/AppWorld/public/result"
//
//#define SCRATCHER_GETSERVICE @"http://esra.elitemg.com/AppWorld/public/scw"
