//
//  SignUp.h
//  Charter Trivia
//
//  Created by Xplorant on 12/09/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "JSONModel.h"
#import "TriviaQuest.h"

@interface SignUp : JSONModel

@property (strong, nonatomic) NSString* firstname;
@property (strong, nonatomic) NSString* lastname;
@property (strong, nonatomic) NSString* phoneno;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* emailId;
@property (strong, nonatomic) NSString* besttimetocal;
@property (strong, nonatomic) NSString* result;
@property (strong, nonatomic) NSString* score;
@property (strong, nonatomic) NSArray<TriviaQuest>* trivia;

@end
