//
//  TriviaQuest.h
//  Charter Trivia
//
//  Created by Xplorant on 12/09/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "JSONModel.h"

@protocol TriviaQuest
@end

@interface TriviaQuest : JSONModel

@property (strong, nonatomic) NSString *que_id;
@property (strong, nonatomic) NSString *ans_id;

@end
