//
//  main.m
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/3/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
