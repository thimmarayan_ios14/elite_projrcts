//
//  PointViewController.m
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/11/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "PointViewController.h"
#import "TriviaQuest.h"
#import "SignUp.h"
#import "AFNetworking.h"
#import "Define.h"
#import "FXReachability.h"
#import "SignUpVc.h"

@interface PointViewController ()
{
    NSString *verify_status1;
    NSString *verify_status2;
    
    NSMutableArray *fetchSignUpAry, *archiveArray;
    NSDictionary *inputParametersDic;
    
    NSTimer *dismissTimer;
    
    int i;
}
@end

@implementation PointViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    i = 0;
    
    fetchSignUpAry = [[NSMutableArray alloc]init];
    archiveArray = [[NSMutableArray alloc]init];

   verify_status1 = [[NSUserDefaults standardUserDefaults]stringForKey:@"verify_status1"];
   verify_status2 = [[NSUserDefaults standardUserDefaults]stringForKey:@"verify_status2"];
    
//    dismissTimer= [NSTimer scheduledTimerWithTimeInterval:10.0
//                                                   target:self
//                                                 selector:@selector(dismissTimerAutomatically)
//                                                 userInfo:nil
//                                                  repeats:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    UIColor *color = [UIColor colorWithRed:(255.0f/255) green:(163.0f/255) blue:(0.0f/255)  alpha:1.0f];
    _PointLabel.textColor = color;
    _pointSec_Label.textColor = color;

    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"winPrize-bgImage"]];

    _PointLabel.text =[NSString stringWithFormat:@"%@",verify_status1];
    _pointSec_Label.text = [NSString stringWithFormat:@"%@",verify_status2];
    
    UIImage *buttonImageNormal = [UIImage imageNamed:@"NEXT-PLAYER"];
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = CGRectMake(700.0, 685.0, buttonImageNormal.size.width, buttonImageNormal.size.height);
    //[playButton setTitle:@"Play" forState:UIControlStateNormal];
    playButton.backgroundColor = [UIColor clearColor];
    playButton.userInteractionEnabled=YES;
    playButton.exclusiveTouch=YES;
    [playButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
    
    UIImage *strechableButtonImageNormal = [buttonImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [playButton setBackgroundImage:strechableButtonImageNormal forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(nextPlayerAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
  
    [self uploadUserData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [dismissTimer invalidate];
    dismissTimer = nil;
}

#pragma mark - 
#pragma mark - Upload User Data

-(void)uploadUserData
{
    NSDictionary *scoreDic = @{@"result":_PointLabel.text,@"score":_pointSec_Label.text};
    NSDictionary *userDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"userinfo"];
    NSArray *questionArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"questioninfo"];
    
    if ([FXReachability isReachable])
    {
        // Online
        
        inputParametersDic = @{@"userinfo":userDictionary, @"score":scoreDic,@"questioninfo":questionArray};
        
        [self postDataToServer];
    }
    else
    {
        // Offline
        
        // Save Dict to JSON Model
    
        JSONModelError *initError;

        NSDictionary *signUpJsonModelDict = @{@"firstname":[userDictionary valueForKey:@"firstname"],@"lastname":[userDictionary valueForKey:@"lastname"]
                           ,@"phoneno":[userDictionary valueForKey:@"phoneno"],@"city":[userDictionary valueForKey:@"city"],@"emailId":[userDictionary valueForKey:@"emailid"],@"besttimetocal":[userDictionary valueForKey:@"besttimetocal"], @"score":[scoreDic valueForKey:@"score"],@"result":[scoreDic valueForKey:@"result"],
                               @"trivia":questionArray };
    
        SignUp * userDic = [[SignUp alloc] initWithDictionary:signUpJsonModelDict error:&initError];
        NSLog(@" SignUp Log::::%@",userDic);

        if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"TRIVIADETAILS"]mutableCopy]!=nil)
        {
            // If Userdefaults already have the data, Please fetch that data first and pass the current dict with this.
        
            archiveArray = [[NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:@"TRIVIADETAILS"]] mutableCopy];
            [archiveArray addObject:signUpJsonModelDict];
        }
        else
        {
            [archiveArray addObject:signUpJsonModelDict];
        }

        NSData *signupObject = [NSKeyedArchiver archivedDataWithRootObject:archiveArray];
    
        [[NSUserDefaults standardUserDefaults] setObject:signupObject forKey:@"TRIVIADETAILS"];
    }
}

#pragma mark - Post All Data to Server

-(void)postDataToServer
{
    if([FXReachability isReachable])
    {
        // Online
        
        NSLog(@"Charter Full Info input : %@",inputParametersDic);
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputParametersDic
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        NSDictionary *finalDict = @{@"input":jsonString};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *postUrlString=[NSString stringWithFormat:@"%@", PUBLIC_URL];
        
        [manager POST:postUrlString parameters:finalDict success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSDictionary *resultDict = (NSDictionary*)responseObject;
             NSLog(@"Trivia Result : %@",resultDict);
             
             
//             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:[resultDict valueForKey:@"Message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//             [alertView show];
             
         }
              failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             NSLog(@"Error: %@", error);
             
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"There is a problem connecting to the server. Please check your internet connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alertView show];
         }];
     }
    else
    {
        // offline
    }
}
#pragma mark-
#pragma mark - Dismiss Timer 

-(void)nextPlayerAction:(UIButton *)sender
{
    UIStoryboard *board;
    if (!self.storyboard) {
        board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    } else {
        board = self.storyboard;
    }
    SignUpVc *scratchVC = [board instantiateViewControllerWithIdentifier:@"storyboardKey"];
    [self.navigationController pushViewController:scratchVC animated:YES];

}
-(void)dismissTimerAutomatically
{
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
