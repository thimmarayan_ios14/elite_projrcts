//
//  PointViewController.h
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/11/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PointViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *PointLabel;
@property (strong, nonatomic) IBOutlet UILabel *pointSec_Label;

@end
