//
//  WinPrizeViewController.m
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/4/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "WinPrizeViewController.h"
#import "QuestionViewController.h"
#import "FXReachability.h"

@interface WinPrizeViewController ()<UIAlertViewDelegate>
{
    IBOutlet UILabel *answer_Label;
    IBOutlet UILabel *winPrize_Label;
    IBOutlet UILabel *quickestTime_Label;
    IBOutlet UILabel *timer_Label;
    int secondsForGame;
    NSTimer *timerForGame, *dismissTimer;

}

@end

@implementation WinPrizeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    timer_Label.text = [NSString stringWithFormat:@"3"];
    [self timerStart];
    
        
}

-(void)viewWillAppear:(BOOL)animated
{
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"winPrize-bgImage"]];
    UIColor *color = [UIColor colorWithRed:(255.0f/255) green:(255.0f/255) blue:(255.0f/255)  alpha:1.0f];
    answer_Label.textColor = color;
    winPrize_Label.textColor = color;
    quickestTime_Label.textColor =color;
    timer_Label.textColor = color;
}

#pragma mark - Timer Method

- (void)timerStart
{
    secondsForGame = 3;
    
    timer_Label.text = [NSString stringWithFormat:@"%d",secondsForGame];
    
    timerForGame = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    
}
- (void)updateTimer
{
    secondsForGame--;
  timer_Label.text = [NSString stringWithFormat:@"%d",secondsForGame];
    
    if (secondsForGame==0)
    {
        if ([timerForGame isValid]) {
            
            [timerForGame invalidate];       // Stop Timer
            
            timerForGame = nil;
            UIStoryboard *board;
            if (!self.storyboard)
                {
                    board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                } else {
                    board = self.storyboard;
                        }
            QuestionViewController *scratchVC = [board instantiateViewControllerWithIdentifier:@"storyboardQuestionKey"];
            [self.navigationController pushViewController:scratchVC animated:YES];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
