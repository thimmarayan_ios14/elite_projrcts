//
//  SignUpVc.m
//  Charter Trivia
//
//  Created by Xplorant on 12/09/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "SignUpVc.h"
#import "WinPrizeViewController.h"
#import "PKCustomKeyboard.h"
#import "FXReachability.h"

#define ALPHA @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define NUMERIC @"1234567890"

@interface SignUpVc ()<UITextFieldDelegate,UIPopoverControllerDelegate,UIGestureRecognizerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *phoneNoLabel;
    IBOutlet UILabel *timeToCallLabel;
    IBOutlet UILabel *emailLabel;
    IBOutlet UILabel *cityLabel;
    IBOutlet UITextField *FirstName_TextField;
    IBOutlet UITextField *lastName_TextField;
    IBOutlet UITextField *email_TextField;
    IBOutlet UITextField *phoneNo_TextField;
    IBOutlet UITextField *timeCall_TextField;
    IBOutlet UITextField *city_TextField;
    IBOutlet UIButton *submitButton;
    IBOutlet UITextView *textView;
    IBOutlet UILabel *tickLabel;
    BOOL otherSelected;
    UIImageView *tickImageView;
    UIImage  *tickImage;
    BOOL selected;
    NSMutableArray *Mail_Suffix_array;
    NSMutableArray *timeCall_array;
    UIButton *done_Button;
    UIButton *cancel_Button;
    
    UIButton *timeCall_done_Button;
    UIButton *timeCall_cancel_Button;
    
    UIPickerView *sortPickerView,*timeCallPickerView;
    UIPopoverController *popoverController;
    NSString *sampleString,*timeCallString;
    NSString *joinString;
}

@end

@implementation SignUpVc
@synthesize gmailDropDown_TextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tickImageView = [[UIImageView alloc]init];
    
    Mail_Suffix_array = [[NSMutableArray alloc] initWithObjects:@"gmail.com",@"hotmail.com",@"msn.com",@"yahoo.com",@"Other", nil];
    
    timeCall_array=[[NSMutableArray alloc]initWithObjects:@"Morning", @"Afternoon", @"Evening", @"Anytime", nil];
    
    done_Button = [[UIButton alloc]init];
    cancel_Button = [[UIButton alloc]init];
    
    timeCall_done_Button=[[UIButton alloc]init];
    timeCall_cancel_Button=[[UIButton alloc]init];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bgImage"]];
    
    UIColor *color = [UIColor colorWithRed:(0.0f/255) green:(98.0f/255) blue:(155.0f/255)  alpha:1.0f];
    UIColor *placeHolderColor = [UIColor colorWithRed:(0.0f/255) green:(98.0f/255) blue:(155.0f/255)  alpha:0.5f];
    
    FirstName_TextField.textColor = color;
    FirstName_TextField.tag = 100;
    FirstName_TextField.text = @"";
    FirstName_TextField.returnKeyType = UIReturnKeyNext;
   //FirstName_TextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    FirstName_TextField.clearButtonMode = UITextFieldViewModeNever;
    FirstName_TextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    lastName_TextField.textColor = color;
    lastName_TextField.tag = 101;
    lastName_TextField.text = @"";
    lastName_TextField.clearButtonMode = UITextFieldViewModeNever;
    lastName_TextField.autocorrectionType = UITextAutocorrectionTypeNo;
    lastName_TextField.returnKeyType = UIReturnKeyNext;
    
    email_TextField.textColor = color;
    email_TextField.tag = 102;
    email_TextField.text = @"";
    email_TextField.returnKeyType = UIReturnKeyNext;
    email_TextField.clearButtonMode = UITextFieldViewModeNever;
    email_TextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    gmailDropDown_TextField.textColor = color;
    gmailDropDown_TextField.tag = 103;
    gmailDropDown_TextField.returnKeyType = UIReturnKeyNext;
    gmailDropDown_TextField.clearButtonMode = UITextFieldViewModeNever;
    gmailDropDown_TextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    phoneNo_TextField.textColor = color;
    phoneNo_TextField.tag = 104;
    phoneNo_TextField.text = @"";
    phoneNo_TextField.returnKeyType = UIReturnKeyNext;
    phoneNo_TextField.clearButtonMode = UITextFieldViewModeNever;
    phoneNo_TextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    timeCall_TextField.textColor = color;
    timeCall_TextField.tag = 105;
    timeCall_TextField.text = @"";
    timeCall_TextField.returnKeyType = UIReturnKeyNext;
    timeCall_TextField.clearButtonMode = UITextFieldViewModeNever;
    timeCall_TextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    city_TextField.textColor = color;
    city_TextField.tag = 106;
    city_TextField.text = @"";
    city_TextField.returnKeyType = UIReturnKeyDone;
    city_TextField.clearButtonMode = UITextFieldViewModeNever;
    city_TextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    FirstName_TextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"First Name" attributes:@{ NSForegroundColorAttributeName :placeHolderColor}];
    lastName_TextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Last Name" attributes:@{ NSForegroundColorAttributeName :placeHolderColor}];
    email_TextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Email" attributes:@{ NSForegroundColorAttributeName :placeHolderColor}];
     phoneNo_TextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Phone Number" attributes:@{ NSForegroundColorAttributeName :placeHolderColor}];
    gmailDropDown_TextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"" attributes:@{ NSForegroundColorAttributeName :color}];
    timeCall_TextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"" attributes:@{ NSForegroundColorAttributeName :placeHolderColor}];
    city_TextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"City Name" attributes:@{ NSForegroundColorAttributeName :placeHolderColor}];
    
    
    
    
    
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = CGRectMake(45.0, city_TextField.frame.origin.y+city_TextField.frame.size.height+67, 50, 50);
    //[playButton setTitle:@"Play" forState:UIControlStateNormal];
    playButton.backgroundColor = [UIColor clearColor];
    playButton.userInteractionEnabled=YES;
    playButton.exclusiveTouch=YES;
    [playButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
    [playButton addTarget:self action:@selector(checkAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];

    
    [submitButton setBackgroundColor:[UIColor colorWithRed:(252.0f/255) green:(176.0f/255) blue:(52.0f/255)  alpha:1.0f]];
    submitButton.layer.cornerRadius =5;
    submitButton.clipsToBounds = YES;
    tickImageView.frame = CGRectMake(0,0,20,20);
    
    [done_Button setBackgroundColor:[UIColor clearColor]];
    [done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [done_Button addTarget:self action:@selector(ClickedDoneButton:) forControlEvents:UIControlEventTouchUpInside];
    [done_Button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    
    
    [cancel_Button setBackgroundColor:[UIColor clearColor]];
    [cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel_Button addTarget:self action:@selector(ClickedCancelButton:) forControlEvents:UIControlEventTouchUpInside];
    [cancel_Button setTitleColor:[UIColor blackColor ]forState:UIControlStateNormal];
    
    //setFrame
    cancel_Button.frame =  CGRectMake(0,10,80,40);
    done_Button.frame =  CGRectMake(220,cancel_Button.frame.origin.y,CGRectGetWidth(cancel_Button.frame),CGRectGetHeight(cancel_Button.frame));
    
    
    [timeCall_done_Button setBackgroundColor:[UIColor clearColor]];
    [timeCall_done_Button setTitle:@"Done" forState:UIControlStateNormal];
    [timeCall_done_Button addTarget:self action:@selector(ClickedTimecallDoneBtn:) forControlEvents:UIControlEventTouchUpInside];
    [timeCall_done_Button setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    
    
    [timeCall_cancel_Button setBackgroundColor:[UIColor clearColor]];
    [timeCall_cancel_Button setTitle:@"Cancel" forState:UIControlStateNormal];
    [timeCall_cancel_Button addTarget:self action:@selector(ClickedTimecallCancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    [timeCall_cancel_Button setTitleColor:[UIColor blackColor ]forState:UIControlStateNormal];
    
    //setFrame
    timeCall_cancel_Button.frame =  CGRectMake(0,10,80,40);
    timeCall_done_Button.frame =  CGRectMake(220,timeCall_cancel_Button.frame.origin.y,CGRectGetWidth(timeCall_cancel_Button.frame),CGRectGetHeight(timeCall_cancel_Button.frame));
    
    
    
    
}

-(void)checkAction:(UIButton *)sender
{
//    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
//    singleTapGestureRecognizer.numberOfTapsRequired=1;
//    [tickLabel addGestureRecognizer:singleTapGestureRecognizer];
    
    
    tickImage = [UIImage imageNamed:@"tickImage"];
    tickImageView.image = tickImage;
    tickImageView.contentMode = UIViewContentModeScaleToFill;
    [tickLabel addSubview:tickImageView];
    
    if (selected)
    {
        tickImageView.hidden = YES;
        selected =NO;
    }
    else
    {
        tickImageView.hidden = NO;
        selected =YES;
    }

    

}

-(void)ClickedTimecallDoneBtn:(UIButton *)sender
{
    NSInteger row = [timeCallPickerView selectedRowInComponent:0];
    timeCallString = [timeCall_array objectAtIndex:row];
    
    timeCall_TextField.text =[NSString stringWithFormat:@"%@",timeCallString];
    [self dismissViewControllerAnimated:YES completion:NULL];
    if([timeCall_TextField.text isEqualToString:@"Other"])
    {
        [popoverController dismissPopoverAnimated:YES];
        otherSelected = YES;
        [timeCall_TextField becomeFirstResponder];
    }


}

-(void)ClickedTimecallCancelBtn:(UIButton *)sender
{
    timeCall_TextField.text =@"";
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:FXReachabilityStatusDidChangeNotification object:nil];
    
  //  [[NSNotificationCenter defaultCenter]removeObserver:self];

}

-(void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    tickImage = [UIImage imageNamed:@"tickImage"];
    tickImageView.image = tickImage;
    tickImageView.contentMode = UIViewContentModeScaleToFill;
    [tickLabel addSubview:tickImageView];
    
    if (selected)
    {
        tickImageView.hidden = YES;
        selected =NO;
    }
    else
    {
        tickImageView.hidden = NO;
        selected =YES;
    }
}

-(void)ClickedDoneButton:(UIButton *)sender
{
    
   
    NSInteger row = [sortPickerView selectedRowInComponent:0];
    sampleString  = [Mail_Suffix_array objectAtIndex:row];
    
    gmailDropDown_TextField.text =[NSString stringWithFormat:@"%@",sampleString];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    if([gmailDropDown_TextField.text isEqualToString:@"Other"])
    {
        [popoverController dismissPopoverAnimated:YES];
        otherSelected = YES;
        [gmailDropDown_TextField becomeFirstResponder];
    }
 }

-(void)ClickedCancelButton:(UIButton *)sender
{
    gmailDropDown_TextField.text =@"";
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma TextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == gmailDropDown_TextField)
    {
        if([Mail_Suffix_array objectAtIndex:4])
        {
            gmailDropDown_TextField.text = @"";
            [popoverController dismissPopoverAnimated:YES];
            otherSelected = YES;
            [gmailDropDown_TextField becomeFirstResponder];
        }
        else
        {
            [email_TextField  resignFirstResponder];
            otherSelected = NO;
        }
    }
    
    if(textField == timeCall_TextField)
    {
        if([timeCall_array objectAtIndex:4])
        {
            timeCall_TextField.text = @"";
            [popoverController dismissPopoverAnimated:YES];
            otherSelected = YES;
            [timeCall_TextField becomeFirstResponder];
        }
        else
        {
            [email_TextField  resignFirstResponder];
            otherSelected = NO;
        }
    }
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    otherSelected = NO;
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag==103)
    {
        if(otherSelected == YES)
        {
//            PKCustomKeyboard  *customKeyboard = [[PKCustomKeyboard alloc] init];
//            [customKeyboard setTextView:gmailDropDown_TextField];
            otherSelected = NO;
        }
        else
        {
            [FirstName_TextField resignFirstResponder];
            [lastName_TextField resignFirstResponder];
            [email_TextField resignFirstResponder];
            [phoneNo_TextField resignFirstResponder];
            [city_TextField resignFirstResponder];
            [timeCall_TextField resignFirstResponder];
            
            UIViewController *sortViewController = [[UIViewController alloc] init];
            UIView *popoverView = [[UIView alloc] init];
            sortViewController.view = popoverView;
            popoverView.backgroundColor = [UIColor clearColor];
            sortPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0,0)];
            [popoverView addSubview:sortPickerView];
            [popoverView addSubview:done_Button];
            [popoverView addSubview:cancel_Button];
            sortPickerView.delegate = self;
            sortPickerView.dataSource = self;
            sortPickerView.showsSelectionIndicator = YES;
            
            popoverController = [[UIPopoverController alloc] initWithContentViewController:sortViewController];
            [popoverController presentPopoverFromRect:CGRectMake(((gmailDropDown_TextField.frame.origin.x)+80),(CGRectGetMaxY(gmailDropDown_TextField.frame)+40), 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            popoverController.popoverContentSize = CGSizeMake(300,250);
            return NO;
        }
        
    }
    else if(textField.tag == 104)
    {
        
    }
    
    if(textField.tag == 105)
    {
        
        if(otherSelected == YES)
        {
            PKCustomKeyboard  *customKeyboard = [[PKCustomKeyboard alloc] init];
            [customKeyboard setTextView:timeCall_TextField];
            otherSelected = NO;
        }
        else
        {
            [FirstName_TextField resignFirstResponder];
            [lastName_TextField resignFirstResponder];
            [email_TextField resignFirstResponder];
            [phoneNo_TextField resignFirstResponder];
            [city_TextField resignFirstResponder];
            [timeCall_TextField resignFirstResponder];
            
            UIViewController *sortViewController = [[UIViewController alloc] init];
            UIView *popoverView = [[UIView alloc] init];
            sortViewController.view = popoverView;
            popoverView.backgroundColor = [UIColor clearColor];
            timeCallPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0,0)];
            [popoverView addSubview:timeCallPickerView];
            [popoverView addSubview:timeCall_done_Button];
            [popoverView addSubview:timeCall_cancel_Button];
            timeCallPickerView.delegate = self;
            timeCallPickerView.dataSource = self;
            timeCallPickerView.showsSelectionIndicator = YES;
            
            popoverController = [[UIPopoverController alloc] initWithContentViewController:sortViewController];
            [popoverController presentPopoverFromRect:CGRectMake(((timeCall_TextField.frame.origin.x)+180),(CGRectGetMaxY(timeCall_TextField.frame)+40), 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            popoverController.popoverContentSize = CGSizeMake(300,250);
            return NO;
        }
        
    }
    else if(textField.tag == 106)
    {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x,130) animated:YES];
    }
        return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    
    if(textField.tag == 104)
    {
       
    }
    else if(textField.tag == 105)
    {
      
    }
    else if(textField.tag == 106)
    {
        [scrollView setContentOffset:CGPointMake(0,-20) animated:YES];
    }
    
    else if(textField == phoneNo_TextField)
    {
    //[self phoneNumberValidation];
        if(textField == phoneNo_TextField && phoneNo_TextField.text.length>0){
            
            if(([phoneNo_TextField.text length]==14 )){
                
                [textField resignFirstResponder];
                return YES;
                
            }
            
        }

        
    }
    else if( textField == gmailDropDown_TextField)
    {
        if (gmailDropDown_TextField.text.length>0)
        {
            [self emailValidation];
        }
    }
    return YES;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == FirstName_TextField)
    {
        
        
        
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"1234567890!@#;$%^&.*(]',;')_+{}:=<>/?[~`|"] invertedSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    else if(textField == lastName_TextField)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"1234567890!@#;$%^&.*(]\'|,;')_+{}:=<>/?[~`|"] invertedSet] invertedSet];        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else if (textField == email_TextField)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_.-"] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }

    else if(textField == city_TextField)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"1234567890!@#;$%^&.*(]',;')_+{}:=<>/?[~`|"] invertedSet] invertedSet];        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else if(textField == timeCall_TextField)
    {
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"1234567890!@#;$%^&.*(]',;')_+{}:=<>/?[~`|"] invertedSet] invertedSet];        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    
    else if (textField == phoneNo_TextField)
    {
        
        
//        NSCharacterSet *unacceptedInput = nil;
//        
//        if (textField == phoneNo_TextField)
//        {
//            unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
//        } else
//        {
//            unacceptedInput = [[NSCharacterSet illegalCharacterSet] invertedSet];
//        }
//        
//        int length = [self getLength:textField.text];
//        if(length == 10)
//        {
//            if(range.length == 0)
//                return NO;
//        }
//        
//        if(length == 3)
//        {
//            string = [self formatNumber:textField.text];
//            textField.text = [NSString stringWithFormat:@"(%@) ",string];
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"%@",[string substringToIndex:3]];
//        }
//        else if(length == 6)
//        {
//            string = [self formatNumber:textField.text];
//            
//            textField.text = [NSString stringWithFormat:@"(%@) %@-",[string  substringToIndex:3],[string substringFromIndex:3]];
//            if(range.length > 0)
//                textField.text = [NSString stringWithFormat:@"(%@) %@",[string substringToIndex:3],[string substringFromIndex:3]];
//        }
//        
//        
//        return ([[string componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1);
        
        
        
        //    NSUInteger newLength = [phoneNoTxtFld.text length] + [string length] - range.length;
        //    return (newLength > 10) ? NO : YES;
        
        NSString *validRegEx =@"^[0-9]*$"; //change this regular expression as your requirement
        NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
        BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
        if (myStringMatchesRegEx)
        {
            int length = [self getLength:phoneNo_TextField.text];
            
            if(length == 10)
            {
                if(range.length == 0)
                    return NO;
            }
            
            if(length == 3)
            {
                NSString *num = [self formatNumber:phoneNo_TextField.text];
                textField.text = [NSString stringWithFormat:@"(%@) ",num];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
            }
            else if(length == 6)
            {
                NSString *num = [self formatNumber:phoneNo_TextField.text];
                textField.text = [NSString stringWithFormat:@"(%@)-%@-",[num  substringToIndex:3],[num substringFromIndex:3]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
        }
        else
        {
            NSString *mess1 = [NSString stringWithFormat:@"%@ is not a number format!",string];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:mess1
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            alert.tag = 1;
        }
    }
    return YES;
}

-(NSString*)formatNumber:(NSString*)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        
    }
    
    return mobileNumber;
}

-(int)getLength:(NSString*)mobileNumber
{
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    return length;
    
}

#pragma mark - AlertView clickedButtonAtIndex method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        if ( [phoneNo_TextField.text length] > 0)
            phoneNo_TextField.text = [phoneNo_TextField.text substringToIndex:[phoneNo_TextField.text length] - 1];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==FirstName_TextField)
    {
        [FirstName_TextField resignFirstResponder];
        [lastName_TextField becomeFirstResponder];
        
    }
    else if (textField==lastName_TextField)
    {
        [lastName_TextField resignFirstResponder];
        [email_TextField becomeFirstResponder];
        
    }
    else if (textField==email_TextField)
    {
        [email_TextField resignFirstResponder];
        [gmailDropDown_TextField becomeFirstResponder];
        
    }
    else if (textField == gmailDropDown_TextField)
    {
        [gmailDropDown_TextField resignFirstResponder];
        [phoneNo_TextField becomeFirstResponder];
    }
    else if (textField==phoneNo_TextField)
    {
        [phoneNo_TextField resignFirstResponder];
       
        [timeCall_TextField becomeFirstResponder];
    }
    else if (textField==timeCall_TextField)
    {
        [timeCall_TextField resignFirstResponder];
       
        [city_TextField becomeFirstResponder];
        
    }
    else if (textField==city_TextField)
    {
        [city_TextField resignFirstResponder];
        [scrollView setContentOffset:CGPointMake(0,-20) animated:YES];
        
    }
    
    return YES;
}

- (IBAction)ButtonTouch:(id)sender
{
        if(FirstName_TextField.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please provide a first name" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [alert show];
        }
    
        else if(lastName_TextField.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please provide a last name" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [alert show];
        }
        else if(email_TextField.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please provide an email" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [alert show];
        }
        else if(gmailDropDown_TextField.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please choose any of the email domains" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [alert show];
        }
        else if(phoneNo_TextField.text.length==0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please provide a mobile number" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [alert show];
        }
    
        else if (phoneNo_TextField.text.length<10)
        {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Charter Trivia" message:@"Please provide a valid mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
    

        else if(timeCall_TextField.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please choose the best time to call you" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [alert show];
        }
        else if(city_TextField.text.length == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please provide your city name" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [alert show];
        }
    
        else
        {
            
            NSString * string3 = [email_TextField.text stringByAppendingString:@"@"];
            joinString=[string3 stringByAppendingString:gmailDropDown_TextField.text];
            NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
            
            NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
            if ([emailTest evaluateWithObject:joinString] == NO)
            {
    
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please provide a valid email" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
                [alert show];
            }
    
            
    
            else if(!selected)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Charter Trivia" message:@"Please check the Terms and Conditions" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            else
            {
                [self storeData];
    
                UIStoryboard *board;
                if (!self.storyboard)
                {
                    board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                } else
                {
                    board = self.storyboard;
                }
                WinPrizeViewController *scratchVC = [board instantiateViewControllerWithIdentifier:@"storyboardTimeKey"];
                [self.navigationController pushViewController:scratchVC animated:YES];
            }
        }
}

-(void)emailValidation
{
    NSString * string3 = [email_TextField.text stringByAppendingString:@"@"];
    joinString=[string3 stringByAppendingString:gmailDropDown_TextField.text];
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    if ([emailTest evaluateWithObject:joinString] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Charter Trivia" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
        [alert show];
    }
}
-(void)phoneNumberValidation
{
    if (phoneNo_TextField.text.length<10)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Charter Trivia" message:@"Please Enter a Valid Mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma PickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;

{
    if (pickerView==sortPickerView)
    {
        return [Mail_Suffix_array count];
    }
    else if (pickerView==timeCallPickerView)
    {
        return [timeCall_array count];
    }
    else
    {
        return 0;
    }
    
}
-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView==sortPickerView)
    {
        return [Mail_Suffix_array objectAtIndex:row];
    }
   else if (pickerView==timeCallPickerView)
    {
        return [timeCall_array objectAtIndex:row];
    }
    else
    {
        return 0;
    }
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    if (pickerView==sortPickerView)
    {
        sampleString = [NSString stringWithFormat:@"%@",[Mail_Suffix_array objectAtIndex:row]];
    }
    else if (pickerView==timeCallPickerView)
    {
        timeCallString = [NSString stringWithFormat:@"%@",[timeCall_array objectAtIndex:row]];
    }
    else
    {
        
    }
}

-(void)storeData
{
    NSDictionary *dic = @{@"firstname":FirstName_TextField.text,@"lastname":lastName_TextField.text
                          ,@"phoneno":phoneNo_TextField.text,@"city":city_TextField.text,@"emailid":joinString, @"besttimetocal":timeCall_TextField.text};
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:@"userinfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  
}

@end
