//
//  ViewController.m
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/3/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "ViewController.h"
#import "PKCustomKeyboard.h"
#import "SignUpVc.h"

@interface ViewController ()
{
    IBOutlet UIView *tapView;
    IBOutlet UILabel *touch_Label;
    IBOutlet UILabel *tittle_Label;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-img"]];
    tittle_Label.textColor = [UIColor colorWithRed:(255.0f/255) green:(255.0f/255) blue:(255.0f/255)  alpha:1.0f];
    touch_Label.textColor = [UIColor colorWithRed:(255.0f/255) green:(255.0f/255) blue:(255.0f/255)  alpha:1.0f];
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    singleTapGestureRecognizer.numberOfTapsRequired=1;
    [tapView addGestureRecognizer:singleTapGestureRecognizer];
}

- (void)handleSingleTapGesture:(UITapGestureRecognizer*)recognizer
{
    UIStoryboard *board;
    if (!self.storyboard) {
        board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    } else {
        board = self.storyboard;
    }
    SignUpVc *scratchVC = [board instantiateViewControllerWithIdentifier:@"storyboardKey"];
    [self.navigationController pushViewController:scratchVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
