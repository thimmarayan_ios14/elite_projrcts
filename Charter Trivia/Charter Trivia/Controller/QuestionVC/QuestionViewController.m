//
//  QuestionViewController.m
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/9/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "QuestionViewController.h"
#import "FXReachability.h"
#import "PointViewController.h"
@interface QuestionViewController ()<UIAlertViewDelegate>
{
    IBOutlet UILabel *question_Label;
    IBOutlet UILabel *speed_Label;
    IBOutlet UIImageView *timer_ImageView;
    IBOutlet UILabel *timer_Label;
    IBOutlet UIButton *first_Button;
    IBOutlet UIButton *second_Button;
    IBOutlet UIButton *third_Button;
    NSMutableArray *questionArray,*questionArray1,*PickArray,*secListArray,*PlistArray;
    int secondsForGame;
    NSTimer *timerForGame;
    int idx;
    NSMutableArray *newQuestionArray;
    int remainingTime1;
    int remainingTime2;
    int remainingTime3;
    int point1;
    int point2;
    int point3;
    NSMutableArray *timeArray;
    NSMutableArray *pointsArray;
    NSMutableArray *valueArray;
    int totalTime;
    int totalPoint;
    int totalValue;
    int totalSec;
    NSString *pointAndTime;
    int value1;
    int value2;
    int value3;
    int questionCount;
}
@end

@implementation QuestionViewController

//static int n = 0;
//static int i = 1;

static int n = 0;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
   newQuestionArray = [[NSMutableArray alloc]init];
   timeArray = [[NSMutableArray alloc]init];
   pointsArray = [[NSMutableArray alloc]init];
   valueArray = [[NSMutableArray alloc]init];
   PickArray = [[NSMutableArray alloc]init];
   secListArray = [[NSMutableArray alloc]init];
   PlistArray = [[NSMutableArray alloc]init];
   questionCount = 1;
  //  isSelected = NO;
    
    /*
    int firstIndex ;
    int lastIndex = 0;
    
    for (int a = 0; a < 3; a++)
    {
        firstIndex = arc4random() % (NSUInteger)(questionArray.count);
        if (firstIndex == lastIndex)
        {
            while (firstIndex == lastIndex)
            {
                firstIndex = arc4random();
                [newQuestionArray addObject:[questionArray objectAtIndex:firstIndex]];
            }
        }
        else
        {
            [newQuestionArray addObject:[questionArray objectAtIndex:firstIndex]];
            lastIndex = firstIndex;
        }
    }
    */
    
    /*for(int i=0; i<3; i++)
    {
        NSInteger index = arc4random_uniform(37); //% (NSUInteger)(questionArray.count);
        [newQuestionArray addObject:[questionArray objectAtIndex:index]];
    }*/
    
    
//    int firstIndex, secondIndex, thirdIndex, randomNo, loop = 1;
//    
//    firstIndex = arc4random()% (NSUInteger)(questionArray.count);
//    [newQuestionArray addObject:[questionArray objectAtIndex:firstIndex]];
//    
//    secondIndex = arc4random()% (NSUInteger)(questionArray.count);
//    
//    if(firstIndex == secondIndex)
//    {
//        do {
//            randomNo = arc4random()% (NSUInteger)(questionArray.count);
//            if(firstIndex != randomNo){
//                secondIndex = randomNo;
//                [newQuestionArray addObject:[questionArray objectAtIndex:secondIndex]];
//                
//                loop = 0;
//            }
//        } while (loop);
//    }
//    else
//    {
//        [newQuestionArray addObject:[questionArray objectAtIndex:secondIndex]];
//    }
//    
//    loop = 1;
//    
//    thirdIndex = arc4random()% (NSUInteger)(questionArray.count);
//    if(firstIndex != thirdIndex && secondIndex != thirdIndex)
//    {
//        [newQuestionArray addObject:[questionArray objectAtIndex:thirdIndex]];
//    }
//    else{
//        do {
//            randomNo = arc4random()% (NSUInteger)(questionArray.count);
//            if(firstIndex != randomNo && secondIndex != randomNo){
//                thirdIndex = randomNo;
//                [newQuestionArray addObject:[questionArray objectAtIndex:thirdIndex]];
//                loop = 0;
//            }
//        } while (loop);
//    }
//
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *finalPath = [path stringByAppendingPathComponent:@"Property List.plist"];
    questionArray =[NSMutableArray arrayWithContentsOfFile:finalPath];
    for(int i=0; i<2; i++)
    {
        NSInteger index = arc4random() % (NSUInteger)(questionArray.count);
        [PlistArray addObject:[questionArray objectAtIndex:index]];
        [questionArray removeObjectAtIndex:index];
    }
    NSString *path1 = [[NSBundle mainBundle] bundlePath];
    NSString *finalPath1 = [path1 stringByAppendingPathComponent:@"SectionProperty List.plist"];
    questionArray1 =[NSMutableArray arrayWithContentsOfFile:finalPath1];
    if(questionArray1.count >0)
    {
        NSInteger index = arc4random() % (NSUInteger)(questionArray1.count);
        [secListArray addObject:[questionArray1 objectAtIndex:index]];
    }
    [PickArray addObjectsFromArray:secListArray];
    [PickArray addObjectsFromArray:PlistArray];
    for(int i=0; i<3; i++)
    {
        int randomIndex = arc4random()%[PickArray count];
        [newQuestionArray addObject:[PickArray objectAtIndex:randomIndex]];
        [PickArray removeObjectAtIndex:randomIndex];
    }
    NSString * string3 = @"Question";
    NSString *newString = [string3 stringByAppendingFormat:@" %i", questionCount];
    question_Label.text = newString;
    speed_Label.text = [[newQuestionArray objectAtIndex:0] objectForKey:@"Question"];
    [first_Button setTitle:[[newQuestionArray objectAtIndex:0] objectForKey:@"Option1"] forState:UIControlStateNormal];
    [second_Button setTitle:[[newQuestionArray objectAtIndex:0] objectForKey:@"Option2"] forState:UIControlStateNormal];
    [third_Button setTitle:[[newQuestionArray objectAtIndex:0] objectForKey:@"Option3"] forState:UIControlStateNormal];
    [first_Button setImage:[UIImage imageNamed:@"answer_image"] forState:UIControlStateNormal];
    [second_Button setImage:[UIImage imageNamed:@"answer_image"] forState:UIControlStateNormal];
    [third_Button setImage:[UIImage imageNamed:@"answer_image"] forState:UIControlStateNormal];
    [self timerStart];
   // [self stopTimer];

}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"question_bgImage"]];
    UIColor *color = [UIColor colorWithRed:(255.0f/255) green:(255.0f/255) blue:(255.0f/255)  alpha:1.0f];
    question_Label.textColor = color;
    speed_Label.textColor = color;
    timer_Label.textColor = color;
    
    [first_Button setTitleColor:color forState:UIControlStateNormal];
    [second_Button setTitleColor:color forState:UIControlStateNormal];
    [third_Button setTitleColor:color forState:UIControlStateNormal];

    first_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    second_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    third_Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    [first_Button setImageEdgeInsets:UIEdgeInsetsMake(5,0,5,0)];  // top, left, bottom, right
    [first_Button setTitleEdgeInsets: UIEdgeInsetsMake(22,50,20,0)];
    [second_Button setImageEdgeInsets:UIEdgeInsetsMake(5,0,5,0)];  // top, left, bottom, right
    [second_Button setTitleEdgeInsets: UIEdgeInsetsMake(22,50,20,0)];
    [third_Button setImageEdgeInsets:UIEdgeInsetsMake(5,0,5,0)];  // top, left, bottom, right
    [third_Button setTitleEdgeInsets: UIEdgeInsetsMake(22,50,20,0)];

    [first_Button addTarget:self action:@selector(ButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [second_Button addTarget:self action:@selector(ButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [third_Button addTarget:self action:@selector(ButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    first_Button.tag = 1;
    second_Button.tag = 2;
    third_Button.tag = 3;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [timerForGame invalidate];
    timerForGame = nil;
}

-(void)ButtonClick:(UIButton *)sender
{
    if(n < [questionArray count])
    {
        NSString *correctAnsNo = [[newQuestionArray objectAtIndex:n] objectForKey:@"CorrectAns"];
        idx = [correctAnsNo intValue];
        NSInteger tag = sender.tag;
        
        if(tag == idx)
        {
            if(tag == 1)
            {
                value1 = [timer_Label.text intValue];
                [first_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
                point1 = 100;
                [pointsArray addObject:[NSNumber numberWithInt:point1]];
                [valueArray addObject:[NSNumber numberWithInt:value1]];
            }
            else if(tag ==2)
            {
                value2 = [timer_Label.text intValue];
                [second_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
                point2 = 100;
                [pointsArray addObject:[NSNumber numberWithInt:point2]];
                [valueArray addObject:[NSNumber numberWithInt:value2]];
            }
            else
            {
                value3 = [timer_Label.text intValue];
                [third_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
                point3 = 100;
                [pointsArray addObject:[NSNumber numberWithInt:point3]];
                [valueArray addObject:[NSNumber numberWithInt:value3]];
            }
        }
        else if(tag != idx)
        {
            if (idx == 4)
            {
                // All Answers are correct.
                
                if(tag == 1)
                {
                    value1 = [timer_Label.text intValue];
                    [first_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
                    point1 = 100;
                    [pointsArray addObject:[NSNumber numberWithInt:point1]];
                    [valueArray addObject:[NSNumber numberWithInt:value1]];
                }
                else if(tag ==2)
                {
                    value2 = [timer_Label.text intValue];
                    [second_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
                    point2 = 100;
                    [pointsArray addObject:[NSNumber numberWithInt:point2]];
                    [valueArray addObject:[NSNumber numberWithInt:value2]];
                }
                else
                {
                    value3 = [timer_Label.text intValue];
                    [third_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
                    point3 = 100;
                    [pointsArray addObject:[NSNumber numberWithInt:point3]];
                    [valueArray addObject:[NSNumber numberWithInt:value3]];
                }
            }
            else
            {
                if(tag == 1)
                {
                    [first_Button setImage:[UIImage imageNamed:@"wrong_Image"] forState:UIControlStateNormal];
                    [self correctAnswerImage];
                }
                else if(tag ==2)
                {
                    [second_Button setImage:[UIImage imageNamed:@"wrong_Image"] forState:UIControlStateNormal];
                    [self correctAnswerImage];
                }
                else
                {
                    [third_Button setImage:[UIImage imageNamed:@"wrong_Image"] forState:UIControlStateNormal];
                    [self correctAnswerImage];
                }
            }
        }
        
        if (tag == 1)
        {
            value1 = [timer_Label.text intValue];
            remainingTime1 = 60 - value1;
            [timeArray addObject:[NSNumber numberWithInt:remainingTime1]];
        }
        else if (tag == 2)
        {
            value2 = [timer_Label.text intValue];
            remainingTime2 = 60 - value2;
            [timeArray addObject:[NSNumber numberWithInt:remainingTime2]];
        }
        else
        {
            value3 = [timer_Label.text intValue];
            remainingTime3 = 60 - value3;
            [timeArray addObject:[NSNumber numberWithInt:remainingTime3]];
        }
        
        n++;
    }
    else
    {
        
        NSLog(@"No of question Completed");
    }
    first_Button.userInteractionEnabled = NO;
    second_Button.userInteractionEnabled = NO;
    third_Button.userInteractionEnabled = NO;
    [self performSelector:@selector(dismissAfterDelay) withObject:nil afterDelay:1.5];
    
    [self stopTimer];
}

- (void)dismissAfterDelay
{
    if (secondsForGame == 0)
    {
        
        value3 = [timer_Label.text intValue];
        remainingTime3 = 60 - value3;
        [timeArray addObject:[NSNumber numberWithInt:remainingTime3]];
        n++;
        
    }
    
    [self stopTimer];
    
    [self timerStart];
    
    if(questionCount <[newQuestionArray count])
    {
        first_Button.userInteractionEnabled = YES;
        second_Button.userInteractionEnabled = YES;
        third_Button.userInteractionEnabled = YES;
        [first_Button setImage:[UIImage imageNamed:@"answer_image"] forState:UIControlStateNormal];
        [second_Button setImage:[UIImage imageNamed:@"answer_image"] forState:UIControlStateNormal];
        [third_Button setImage:[UIImage imageNamed:@"answer_image"] forState:UIControlStateNormal];
        
        NSString * string3 = @"Question";
        NSString *newString = [string3 stringByAppendingFormat:@" %i", questionCount+1];
        question_Label.text = newString;
        speed_Label.text = [[newQuestionArray objectAtIndex:questionCount] objectForKey:@"Question"];
        [first_Button setTitle:[[newQuestionArray objectAtIndex:questionCount] objectForKey:@"Option1"] forState:UIControlStateNormal];
        [second_Button setTitle:[[newQuestionArray objectAtIndex:questionCount] objectForKey:@"Option2"] forState:UIControlStateNormal];
        [third_Button setTitle:[[newQuestionArray objectAtIndex:questionCount] objectForKey:@"Option3"] forState:UIControlStateNormal];
        
        
        self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"question_bgImage"]];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.55];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
        [UIView commitAnimations];
        
        questionCount++;
    }
    else
    {
        [self stopTimer];
        
        totalPoint = 0;
        totalTime = 0;
        totalValue = 0;
        
        for (int i=0; i <pointsArray.count; i++)
        {
            totalPoint = [[pointsArray objectAtIndex:i] intValue]+totalPoint;
            totalValue =  [[valueArray objectAtIndex:i] intValue]+totalValue;
        }
        
        for (int i = 0; i<timeArray.count; i++)
        {
            totalTime =  [[timeArray objectAtIndex:i] intValue]+totalTime;
        }
        
        totalSec = totalPoint + totalValue;
        [self next];
        
    }
}
-(void)correctAnswerImage
{
    if(idx ==1)
    {
        [first_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
    }
    else if(idx ==2)
    {
        [second_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
    }
    else
    {
        [third_Button setImage:[UIImage imageNamed:@"correct_image"] forState:UIControlStateNormal];
    }
}
#pragma mark - Timer Method

- (void)timerStart
{
    secondsForGame = 60;
    timer_Label.text = [NSString stringWithFormat:@"%.2d",secondsForGame];
    timerForGame = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
}
- (void)updateTimer
{
//    if (isSelected == NO)
//    {
        secondsForGame--;
        timer_Label.text = [NSString stringWithFormat:@"%.2d",secondsForGame];
        
    
        if (secondsForGame==0)
        {
            [self stopTimer];
            [self dismissAfterDelay];
            //        static int  p = 0;
            //        if(p == 3)
            //        {
            //            [self next];
            //        }
            //        p++;
        }
 ///   }
    

}

-(void)next
{
//    if (isSelected == NO)
//    {
        NSString* _verifyStatus1 = [NSString stringWithFormat:@"%d",totalTime];
        NSString* _verifyStatus2 = [NSString stringWithFormat:@"%d",totalSec];
        
        [[NSUserDefaults standardUserDefaults]setObject:_verifyStatus1 forKey:@"verify_status1"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults]setObject:_verifyStatus2 forKey:@"verify_status2"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        n=n-3;
        UIStoryboard *board;
        if (!self.storyboard)
        {
            board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        } else {
            board = self.storyboard;
        }
        [self questionData];
        PointViewController *scratchVC = [board instantiateViewControllerWithIdentifier:@"storyboardPriceKey"];
        [self.navigationController pushViewController:scratchVC animated:YES];
        
       // isSelected = YES;
  //  }
    
//    NSString* _verifyStatus1 = [NSString stringWithFormat:@"%d",totalTime];
//    NSString* _verifyStatus2 = [NSString stringWithFormat:@"%d",totalSec];
//    
//    [[NSUserDefaults standardUserDefaults]setObject:_verifyStatus1 forKey:@"verify_status1"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    [[NSUserDefaults standardUserDefaults]setObject:_verifyStatus2 forKey:@"verify_status2"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    i=i-2;
//    n=n-3;
//    UIStoryboard *board;
//    if (!self.storyboard)
//    {
//        board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    } else {
//        board = self.storyboard;
//    }
//    [self questionData];
//    QuestionViewController *scratchVC = [board instantiateViewControllerWithIdentifier:@"storyboardPriceKey"];
//    [self.navigationController pushViewController:scratchVC animated:YES];
    

}
-(void)stopTimer
{
    [timerForGame invalidate];
    timerForGame = nil;
    

}

-(void)questionData
{
    NSArray *ary = @[@{@"que_id":[[newQuestionArray objectAtIndex:0] objectForKey:@"QuestionId"],@"ans_id":[[newQuestionArray objectAtIndex:0] objectForKey:@"CorrectAns"]},
                     @{@"que_id":[[newQuestionArray objectAtIndex:1] objectForKey:@"QuestionId"], @"ans_id":[[newQuestionArray objectAtIndex:1] objectForKey:@"CorrectAns"]},
                     @{@"que_id":[[newQuestionArray objectAtIndex:2] objectForKey:@"QuestionId"], @"ans_id":[[newQuestionArray objectAtIndex:2] objectForKey:@"CorrectAns"]}];
    
    [[NSUserDefaults standardUserDefaults]setObject:ary forKey:@"questioninfo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
