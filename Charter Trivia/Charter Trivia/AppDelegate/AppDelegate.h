//
//  AppDelegate.h
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/3/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end

