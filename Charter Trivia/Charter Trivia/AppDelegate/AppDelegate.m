//
//  AppDelegate.m
//  Charter Trivia
//
//  Created by Dhamodaran Sri on 9/3/15.
//  Copyright (c) 2015 Dhamodaran Sri. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "FXReachability.h"
#import "Define.h"
#import <TSMessages/TSMessageView.h>
#import "SignUp.h"
#import "AFNetworking.h"

@interface AppDelegate ()
{
    NSMutableArray *fetchSignUpAry, *archiveArray, *successAry, *failureAry;
    int i;
    NSDictionary *inputParametersDic, *eachDic;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    fetchSignUpAry = [[NSMutableArray alloc]init];
    successAry = [[NSMutableArray alloc]init];
    failureAry = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRetryAlert) name:FXReachabilityStatusDidChangeNotification object:nil];
    
    ViewController *view=[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"StoryboardId"]; //or the homeController
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:view];
    self.window.rootViewController=navController;
    [self.window makeKeyAndVisible];

    return YES;
}

#pragma mark - Retry

-(void)showRetryAlert
{
    BOOL reachable = [FXReachability isReachable];
    if (!reachable) {
        
        [TSMessage dismissActiveNotification];
        [TSMessage showNotificationInViewController:self.window.rootViewController
                                              title:NSLocalizedString(@"Network connection unavailable!", nil)
                                           subtitle:NSLocalizedString(@"Tap here to retry!", nil)
                                              image:nil
                                               type:TSMessageNotificationTypeError
                                           duration:TSMessageNotificationDurationAutomatic
                                           callback:^(void){
                                               [self networkConnectionAvailable];
                                           }
                                        buttonTitle:nil
                                     buttonCallback:nil
                                         atPosition:TSMessageNotificationPositionBottom
                               canBeDismissedByUser:NO];
        
    }
    else
    {
        [self networkConnectionAvailable];
        
        NSArray *checkAryCount = [[NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:@"TRIVIADETAILS"]] mutableCopy];
        
        if (checkAryCount.count > 0)
        {
            [self showAlert];
        }
    }
}

- (void)networkConnectionAvailable
{
    BOOL reachable = [FXReachability isReachable];
    if (!reachable) {
        //Do nothing
    } else {
        //selected=NO;
        [TSMessage dismissActiveNotification];
        [TSMessage showNotificationInViewController:self.window.rootViewController
                                              title:NSLocalizedString(@"Network connection is available now!", nil)
                                           subtitle:NSLocalizedString(@"", nil)
                                              image:nil
                                               type:TSMessageNotificationTypeSuccess
                                           duration:TSMessageNotificationDurationAutomatic
                                           callback:nil
                                        buttonTitle:nil
                                     buttonCallback:nil
                                         atPosition:TSMessageNotificationPositionBottom
                               canBeDismissedByUser:YES];
  
    }
}

#pragma mark - 
#pragma mark - Show Alert

-(void)showAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"Would you like to sync the data with server now?"
                                                       delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No" , nil];
    alertView.tag=1;
    [alertView show];
}

#pragma mark - alertview delegate Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0)
        {
            // Ok Button Tapped
            
            // Fetch from JSON Model
            
            i = 0;
            
            fetchSignUpAry = [[NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:@"TRIVIADETAILS"]] mutableCopy];
            
            [self fetchSignUpObject];
        }
        else if (buttonIndex == 1)
        {
            // Cancel Button Tapped
        }
    }
}

#pragma mark -
#pragma mark - Fetch JSON Model each dict

-(void)fetchSignUpObject
{
    if (i < fetchSignUpAry.count)
    {
        NSMutableArray *tempSignUpAry = [[NSMutableArray alloc]init];
        
        eachDic = [fetchSignUpAry objectAtIndex:i];
        [tempSignUpAry addObject:eachDic];
        
        for (SignUp *signupDetail in tempSignUpAry)
        {
            NSLog(@"SignUp :: %@", signupDetail);
            
            //            NSDictionary *userInfoDic = @{ @"firstname": signupDetail.firstname,
            //                                           @"lastname": signupDetail.lastname,
            //                                           @"phoneno": signupDetail.phoneno,
            //                                           @"city": signupDetail.city,
            //                                           @"emailid": signupDetail.emailId,
            //                                           @"besttimetocal": signupDetail.besttimetocal
            //                                           };
            //
            //            NSDictionary *scoreDic = @{ @"result": signupDetail.result,
            //                                        @"score": signupDetail.score,
            //                                      };
            
            NSDictionary *userInfoDic = @{ @"firstname": [signupDetail valueForKey:@"firstname"],
                                           @"lastname": [signupDetail valueForKey:@"lastname"],
                                           @"phoneno": [signupDetail valueForKey:@"phoneno"],
                                           @"city": [signupDetail valueForKey:@"city"],
                                           @"emailid": [signupDetail valueForKey:@"emailId"],
                                           @"besttimetocal": [signupDetail valueForKey:@"besttimetocal"]
                                           };
            
            NSDictionary *scoreDic = @{ @"result": [signupDetail valueForKey:@"result"],
                                        @"score": [signupDetail valueForKey:@"score"],
                                        };
            
            inputParametersDic = @{@"userinfo":userInfoDic, @"score":scoreDic,@"questioninfo":[signupDetail valueForKey:@"trivia"]};
            
            [self postDataToServer];
        }
    }
    else
    {
        if (successAry.count > 0)
        {
          [fetchSignUpAry removeObjectsInArray:successAry];
            
            NSData *signupObject = [NSKeyedArchiver archivedDataWithRootObject:fetchSignUpAry];
            
            [[NSUserDefaults standardUserDefaults] setObject:signupObject forKey:@"TRIVIADETAILS"];
            
            [successAry removeAllObjects];
        }
    }
}

#pragma mark - 
#pragma mark - Post All Details to Server

-(void)postDataToServer
{
    if([FXReachability isReachable])
    {
        // Online
        
        NSLog(@"Charter Full Info input : %@",inputParametersDic);
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inputParametersDic
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonString;
        if (! jsonData) {
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        NSDictionary *finalDict = @{@"input":jsonString};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *postUrlString=[NSString stringWithFormat:@"%@", PUBLIC_URL];
        
        [manager POST:postUrlString parameters:finalDict success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             // Success
             
             NSDictionary *resultDict = (NSDictionary*)responseObject;
             NSLog(@"Trivia Result : %@",resultDict);
             
             [successAry addObject:eachDic];
             
             i++;
             [self fetchSignUpObject];
             
         }
              failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             // failure
             
             NSLog(@"Error: %@", error);
             
             [failureAry addObject:eachDic];
             
             i++;
             [self fetchSignUpObject];
             
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERTTITLE message:@"There is a problem connecting to the server. Please check your internet connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alertView show];
         }];
    }
    else
    {
        // Offline
        
        if (successAry.count > 0)
        {
            [fetchSignUpAry removeObjectsInArray:successAry];
            
            NSData *signupObject = [NSKeyedArchiver archivedDataWithRootObject:fetchSignUpAry];
            
            [[NSUserDefaults standardUserDefaults] setObject:signupObject forKey:@"TRIVIADETAILS"];
            
            [successAry removeAllObjects];
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    if (successAry.count > 0)
    {
        [fetchSignUpAry removeObjectsInArray:successAry];
        
        NSData *signupObject = [NSKeyedArchiver archivedDataWithRootObject:fetchSignUpAry];
        
        [[NSUserDefaults standardUserDefaults] setObject:signupObject forKey:@"TRIVIADETAILS"];
        
        [successAry removeAllObjects];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
