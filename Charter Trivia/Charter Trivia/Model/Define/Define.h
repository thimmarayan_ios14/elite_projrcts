//
//  Define.h
//  HP
//
//  Created by Prabu on 27/08/15.
//  Copyright (c) 2015 Mobility. All rights reserved.
//

#ifndef HP_Define_h
#define HP_Define_h


#endif

#define APPDELEGATE (AppDelegate *)[[UIApplication sharedApplication]delegate]
#define K_SCREENSIZE [UIScreen mainScreen].bounds.size


#define FONT_BOLD @"Gotham Black"
#define FONT_LIGHT @"Whitney-Medium"
#define FONT_MEDIUM @"Whitney-Book"

#define  ALERTTITLE @"Charter Trivia"

//#define PUBLIC_URL @"http://192.168.5.42/trivia_app/public/userdataupload"
#define PUBLIC_URL @"http://trivia.smartletmanager.com/public/userdataupload"